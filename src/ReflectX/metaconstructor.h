#ifndef META_CONSTRUCTOR_H
#define META_CONSTRUCTOR_H
#include <string>
#include "metatype.h"
#include "variant.h"
#include <metainfo.h>

class MetaInfo;

class REFLECTX_EXPORT MetaConstructor
{
private:
	MetaConstructor(IMetaInfoBackend*,int idx);
public:
	MetaConstructor();
	bool isValid() const;

	int Index() const;

	MetaInfo EnclosingMetaInfo() const;
	std::string Signature() const;
	std::vector<MetaTypeId> GetParamTypes() const;
	std::vector<std::string> GetConstructorInfos() const;

	template<typename ...Args>
	void* Create(void* buffer, Args&&... args) const {
		if (sizeof... (args) != GetParamTypes().size()) {
			std::cout << "Param Types Not Compatable!" << std::endl;
			return NULL;
		}

		static std::vector<MetaTypeId> types_o = { (MetaType::IdFromType<Args>())... };

		if (types_o != GetParamTypes()) {
			std::cout << "Param Types Not Compatable!" << std::endl;
			return NULL;
		}

		void* param[std::max<int>(1, sizeof... (args))] = { &args... };
		return CreateImply(buffer, (const void**)param);
	}

	void* DynamicCreate(void* buffer, const VariantList& param);
	void* UnSafeCreate(void* buffer, const void** param);

private:
	void* CreateImply(void* buffer,const void** param) const;

	std::shared_ptr<IMetaInfoBackend> meta;
	int idx;

	friend class MetaInfo;
};


#endif // !META_CONSTRUCTOR_H

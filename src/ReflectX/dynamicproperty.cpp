﻿#include "metainfo.h"
#include "metaproperty.h"
#include "Reflectable.h"
#include "dynamicproperty.h"


Variant DynamicProperty::GetProperty(const char *name)
{
    do{
        if(!dynamic_cast<Reflectable*>(this)){
            break;
        }
        auto refle = dynamic_cast<Reflectable*>(this);
        MetaInfo meta = refle->GetMetaInfo();

		int idx = meta.IndexOfProperty(name);
		if (idx == -1) {
			break;
		}

        MetaProperty property = meta.GetProperty(idx);
        if(!property.isValid()){
            break;
        }

        return property.GetValue(dynamic_cast<void*>(this));

    }while(false);

    return m_propertys.at(name);
}

void DynamicProperty::SetProperty(const char *name, Variant var)
{
    do{
        if(!dynamic_cast<Reflectable*>(this)){
            break;
        }
        auto refle = dynamic_cast<Reflectable*>(this);
        MetaInfo meta = refle->GetMetaInfo();

		int idx = meta.IndexOfProperty(name);
		if (idx == -1) {
			break;
		}

		MetaProperty property = meta.GetProperty(idx);
        if(!property.isValid()){
            break;
        }
        property.SetValue(dynamic_cast<void*>(this),var);
    }while (false);

    m_propertys[name] = var;
}

﻿#ifndef ENUMRATE_H
#define ENUMRATE_H

//启用枚举的位操作
#define ENABLE_ENUM_OPERATORS(T)  \
T& \
operator |=(T& o1, const T& o2) {\
	o1 = T((int)o1 | (int)(o2));\
	return o1;\
}\
\
T \
operator |(const T& o1, const T& o2) {\
	return T((int)o1 | (int)(o2));\
}\
\
T& \
operator &=(T& o1, const T& o2) {\
	o1 = T((int)o1 & (int)(o2));\
	return o1;\
}\
\
T \
operator &(const T& o1, const T& o2) {\
	return T((int)o1 & (int)(o2));\
}\
\
T \
operator ~(const T& o1) {\
	return T(~(int)o1);\
}\
\
T& \
operator ^=(T& o1, const T& o2) {\
	o1 = T((int)o1 ^ (int)(o2));\
	return o1;\
}\
\
T \
operator ^(const T& o1, const T& o2) {\
	return T((int)o1 ^ (int)(o2));\
}\



#endif // !ENUMRATE_H

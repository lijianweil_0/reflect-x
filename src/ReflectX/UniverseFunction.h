#ifndef UNIVERSEFUNCTION_H
#define UNIVERSEFUNCTION_H

#include <functional>

#include "typetrate.h"

typedef std::function<void(void* o,const void** param, void* ret)> FunctionAny;//统一函数调用形式

enum FunctionFlag
{
	FF_None,
	FF_NormalFunction,//普通函数
	FF_FunctionalObject,//std::function,lambada表达式
	FF_MemberFunction//成员函数
};

template<typename T,typename Enable = void>
class GetFunctionType {
public:
	static constexpr FunctionFlag Flag = FF_None;
};

//普通函数
template<typename ...Args,typename R>
class GetFunctionType<R (*)(Args...),void>
{
public:
	static constexpr FunctionFlag Flag = FF_NormalFunction;
	typedef R ReturnType;
	typedef ArgList<Args...> ArgType;
	typedef typename GenSeq<sizeof...(Args)>::Type IndexList;
};

//函数对象
template<typename T>
class GetFunctionType<T,typename std::enable_if<std::is_class<T>::value,void>::type>
{
	typedef GetFunctionType<decltype(&T::operator())> FunctionOperator;
public:
	static constexpr FunctionFlag Flag = FF_FunctionalObject;
	typedef typename FunctionOperator::ReturnType ReturnType;
	typedef typename FunctionOperator::ArgType ArgType;
	typedef typename FunctionOperator::IndexList IndexList;
};

//成员函数
template<typename ...Args,typename R,typename O>
class GetFunctionType<R (O::*)(Args...),void>
{
public:
	static constexpr FunctionFlag Flag = FF_MemberFunction;
	typedef R ReturnType;
	typedef ArgList<Args...> ArgType;
	typedef typename GenSeq<sizeof...(Args)>::Type IndexList;
	typedef O ObjectType;
};

template<typename ...Args, typename R, typename O>
class GetFunctionType<R(O::*)(Args...) const, void>
{
public:
	static constexpr FunctionFlag Flag = FF_MemberFunction;
	typedef R ReturnType;
	typedef ArgList<Args...> ArgType;
	typedef typename GenSeq<sizeof...(Args)>::Type IndexList;
	typedef const O ObjectType;
};

//普通函数
template<int ... indexs, typename ...Args, typename Func>
typename std::enable_if<!std::is_same<typename GetFunctionType<Func>::ReturnType, void>::value, void>::type
InvokeFunctionImp(Func func, const void** param, void* ret, IndexesList<indexs...>, ArgList<Args...>) {
	typedef typename GetFunctionType<Func>::ReturnType R;
	R r = func(*(typename std::remove_reference<Args>::type*)(param[indexs])...);
	if (ret)
	{
		*(R*)ret = r;
	}
}

template<int ... indexs, typename ...Args, typename Func>
typename std::enable_if<std::is_same<typename GetFunctionType<Func>::ReturnType, void>::value, void>::type
InvokeFunctionImp(Func func, const void** param, void* ret, IndexesList<indexs...>, ArgList<Args...>) {
	func(*(typename std::remove_reference<Args>::type*)(param[indexs])...);
}


//成员函数
template<int ... indexs, typename ...Args, typename Func>
typename std::enable_if<!std::is_same<typename GetFunctionType<Func>::ReturnType, void>::value, void>::type
InvokeMemberFunctionImp(Func func,void* o, const void** param, void* ret, IndexesList<indexs...>, ArgList<Args...>) {
	typedef GetFunctionType<Func>::ObjectType O;
	typedef typename GetFunctionType<Func>::ReturnType R;
	R r = (((O*)o)->*func)(*(typename std::remove_reference<Args>::type*)(param[indexs])...);
	if (ret)
	{
		*(R*)ret = r;
	}
}


template<int ... indexs, typename ...Args, typename Func>
typename std::enable_if<std::is_same<typename GetFunctionType<Func>::ReturnType, void>::value, void>::type
InvokeMemberFunctionImp(Func func, void* o, const void** param, void* ret, IndexesList<indexs...>, ArgList<Args...>) {
	typedef GetFunctionType<Func>::ObjectType O;
	(((O*)o)->*func)(*(typename std::remove_reference<Args>::type*)(param[indexs])...);
}


template<typename Func>
typename std::enable_if<GetFunctionType<Func>::Flag == FF_MemberFunction, FunctionAny>::type 
WarpFunctionImp(Func func) {
	typedef GetFunctionType<Func> FuncType;
	return [=](void* o,const void** param, void* ret) {
		InvokeMemberFunctionImp(func,o,param,ret, FuncType::IndexList(), FuncType::ArgType());
	};
}

template<typename Func>
typename std::enable_if<GetFunctionType<Func>::Flag == FF_FunctionalObject || GetFunctionType<Func>::Flag == FF_NormalFunction, FunctionAny>::type 
WarpFunctionImp(Func func) {
	typedef GetFunctionType<Func> FuncType;

	return [=](void* o,const void** param, void* ret) {
		InvokeFunctionImp(func,param,ret, FuncType::IndexList(), FuncType::ArgType());
	};
}

template<typename Func>
FunctionAny WarpFunction(Func func) {//将任何形式的函数包装成统一形式
	return WarpFunctionImp(func);
}
#endif // !UNIVERSEFUNCTION_H

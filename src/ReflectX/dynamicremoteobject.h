﻿#ifndef DYNAMICREMOTEOBJECT_H
#define DYNAMICREMOTEOBJECT_H

#include "remoteobject.h"

//假设你对源对象的结构一无所知，但是又想使用远程对象的功能，就可以使用该类
struct DynamicMetaInfoData;
MetaInfo CreateDynamicMetaInfo(const DynamicMetaInfoData &data);
class REFLECTX_EXPORT DynamicRemoteObject:public RemoteObject
{
private:
    DynamicRemoteObject();

public:
    typedef DynamicRemoteObject ThisType ;
    typedef RemoteObject SuperType;
    static MetaInfo staticMetaInfo();
    MetaInfo GetMetaInfo() const override;

    ~DynamicRemoteObject();

    void SetRemoteProperty(const char* name,Variant value);//设定远程对象属性
    Variant GetRemoteProperty(const char* name);//获取远程对象属性

    GenericSignal* GetRemoteSignal(const char* signature);//获取远程对象信号
    std::shared_ptr<WorkFuture<Variant>> CallRemoteFunction(const char* name,const std::vector<Variant>& param);//调用远程对象函数
    void PostRemoteFunction(const char* name, const std::vector<Variant>& param);//调用远程对象函数，舍弃返回值，调用后立即返回

protected:
	unsigned long long DoGetTypeSignature() override;
	void UpdateValue(int idx, Variant value) override;
private:
    void Redirect( const DynamicMetaInfoData& meta);//元对象重定向
    void Redirect(const MetaInfo& meta);
    void MetaInit();

    std::shared_ptr<WorkFuture<Variant>> CallRemoteFunctionImp(int RemoteIndex,const std::vector<Variant>& param,bool isSync = false);
    void PushRemotePropertyImp(int RemoteIndex,Variant value);
    Variant GetRemotePropertyImp(int RemoteIndex) const;
    void ReceiveSignal(int RemoteIndex,const VariantList& param);
    GenericSignal* GetRemoteSignalImp(int RemoteIndex);

    int ToRemoteProperty(int index);
    int ToRemoteFunction(int index);
	int ToRemoteSignal(int index);
private:
    friend class RemoteObjectNode;

    std::unordered_map<int,int> funMap;
    std::unordered_map<int,int> propertyMap;
    std::unordered_map<int,int> signalMap;

	struct LocalProperty {
		Variant value;
		int notifer_remote;
	};

    std::unordered_map<int, LocalProperty> m_externalProperty;
    std::unordered_map<int,std::shared_ptr<GenericSignal>> m_externalSignals;

    MetaInfo m_metaInfo;//动态元对象

private:
	friend MetaInfo CreateDynamicMetaInfo(const DynamicMetaInfoData &data);
	
	template <typename T>
	friend class MetaCreator;
};



#endif // DYNAMICREMOTEOBJECT_H

﻿#ifndef STRINGUTIL_H
#define STRINGUTIL_H

#include <string>
#include <vector>

#include <ReflectX_global.h>

namespace Utils {
class REFLECTX_EXPORT StringUtil
{
public:
    static std::wstring FromLocalString(const std::string &input);
    static std::string ToLocalString(const std::wstring &input);
	static std::vector<std::string> Split(const std::string & input, const std::string& value);
	static std::string Join(const std::vector<std::string> &input, const std::string& value);
};
}


#endif // STRINGUTIL_H

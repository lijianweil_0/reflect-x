﻿#include "stringutil.h"
#include "reflectutils.h"

std::string ReflectUtils::GetSignature(const std::string & name, const std::vector<MetaTypeId>& Arguments)
{
	std::string ret;

	ret += name;
	ret += "(";
	for (int i = 0; i < Arguments.size(); i++) {
		MetaTypeId type = Arguments[i];
		ret += MetaType(type).Name();
		if (i != Arguments.size() - 1)
		{
			ret += ",";
		}
	}
	ret += ")";

	return ret;
}

bool ReflectUtils::ParseSignature(const std::string & signature, std::string & name, std::vector<MetaTypeId>& Arguments)
{
	name = {};
	Arguments = {};

	auto a = signature.find_first_of('(', 0);
	auto b = signature.find_first_of(')', 0);

	if (a == -1 || b == -1 || a>b) {
		return false;
	}

	name = signature.substr(0, a);

	int curIdx = a + 1;

	auto argsStr = signature.substr(a + 1, b - a - 1);

	if (argsStr.empty()) {
		return true;
	}

	auto args = Utils::StringUtil::Split(argsStr, ",");
	
	for (auto i : args) {
		Arguments.push_back(MetaType(i).Id());
	}

	return true;
}

﻿#ifndef  STATE_MACHINE_H
#define STATE_MACHINE_H

#include <vector>
#include <set>

#include <ReflectX_global.h>
#include <threadsafe.h>
#include <signalslot.h>
#include <metainfo.h>
#include <ReflectableObject.h>

namespace StateMachine {
	class State;
	class Translation;
	class StateMachine;

	class REFLECTX_EXPORT State
	{
	private:
		State();
		~State();

	public:
		State* CreateSubState();

	public:
		enum StateType
		{
			Single = 0,
			Parallel,
		};

		StateType GetType() const;
		void SetType(StateType type);

	public:
		std::set<State*> children();
		State* parent();
		StateMachine* TopNode();

		void SetInitState(State* state);

		Translation* AddTranslation(GenericSignal* signal,State* target);

		//当进入此状态时，设置属性
		void SetProperty(void* target,MetaInfo info,const char* property,Variant value);
		void SetProperty(Reflectable* target,const char* property,Variant value);
		void SetProperty(ReflectableObject* target, const char* property, Variant value);

		bool isActive() const;

	public:
		Signal<> EnteredState;
		Signal<> LeavedState;

	private:
		void OnTranslate(Translation* trans);
		void StateEntered();
		void StateLeaved();
		bool IsStarted();
		void SetProperty(void* target, MetaInfo info,ThreadSafe::ThreadSafeBase* safe, const char* property, Variant value);

		class StatePrivate;
	private:
		bool m_isActive;
		std::set<State*> m_children;
		State* m_parent;
		StateType type;
		StateMachine* m_top;
		std::set<Translation*> m_Translations;
		State* m_currentState;
		friend class StateMachine;
		friend class Translation;
		friend class StatePrivate;
	};

	class REFLECTX_EXPORT Translation {
	private:
		ConnectionRegistry m_registry;
		State* m_state;
		State* m_target;

		~Translation();


		friend class State;
	};

	class REFLECTX_EXPORT StateMachine:public State,public ThreadSafe::ThreadSafeBase
	{
	public:
		StateMachine();
		~StateMachine();
		void Start();
		void Stop();
		bool IsStarted() const;

	private:
		bool StartCheck();

	private:
		bool m_bIsStarted;

	};
}





#endif // ! STATE_MACHINE_H

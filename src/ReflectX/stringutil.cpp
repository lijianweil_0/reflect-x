﻿#include "stringutil.h"

std::vector<std::string> Utils::StringUtil::Split(const std::string & input,const std::string& value)
{
	std::vector<std::string> ret;
	int cur = 0;
	while (true)
	{
		int index = input.find_first_of(value.c_str(), cur);
		if (index == -1) {
			ret.push_back(input.substr(cur, input.size() - cur));
			return ret;
		}
		
		ret.push_back(input.substr(cur, index - cur));
		cur = index + value.size();
	}

	return ret;
}

std::string Utils::StringUtil::Join(const std::vector<std::string>& input, const std::string& value)
{
	std::string ret;
	for (int i = 0; i < input.size(); i++) {

		ret += input[i];

		if (i != input.size()) {
			ret += value;
		}
	}


	return ret;
}

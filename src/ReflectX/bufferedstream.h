﻿#ifndef  BUFFERED_STREAM_H
#define BUFFERED_STREAM_H

#include <metatypedef_p.h>
#include <stream.h>

//缓冲流，用于提高io效率。
//缓冲输入流，内含一个缓冲，和一个buffersize.当读取数据时，它实际读取的数据为max(buffersize,readLen)。
class REFLECTX_EXPORT BufferedInputStream :public InputStream {
public:
	BufferedInputStream(InputStream * next,int buffersize = 4096);

	int avaliable() const override;

protected:
	int doRead(unsigned char* buffer, int readLen) throw(StreamException)override;

private:
	void CheckBuffer(int readLen);
	void FillBuffer(int readLen);

private:
	ByteArray m_buffer;
	int buffersize;
};

//缓冲输出流，内含一个缓冲，和一个buffersize.当写出数据时，它会将数据缓存到buffer中，当数据超过缓冲大小时，它才会将实际的数据写出
class REFLECTX_EXPORT BufferedOutputStream :public OutputStream {
public:
	BufferedOutputStream(OutputStream* next, int buffersize = 4096);
	void write(const unsigned char* buffer, int len) throw (StreamException)override;
	void flush() override;
private:
	void CheckBuffer();
	void FlushBuffer();

private:
	ByteArray m_buffer;
	int buffersize;
};

#endif // ! BUFFERED_STREAM_H

#ifndef RX_PLUGIN_H
#define RX_PLUGIN_H

#include "initializefunction.h"
#include "metainfo.h"
#include "ReflectX_global.h"

//插件功能用于创建以某些类为集合的类库
/*
使用方法：
一创建插件
1.创建一个动态库项目
2.动态库项目里创建两个文件，分别是PluginInstance.h PluginInstance.cpp(名字你可以随意决定，只要和下文提到的一致即可)
3.PluginInstance.h中使用RX_PLUGIN_DECL声明插件接口
4.PluginInstance.cpp中使用RX_PLUGIN_BEGIN实现插件接口，以及写入插件相关信息，比如下面的
RX_PLUGIN_BEGIN(TestClass)

RX_PLUGIN_INFORMATION_BEGIN
"这是一个测试插件"
RX_PLUGIN_INFOMATION_END

RX_PLUGIN_END
5.创建一个可反射类，并在它的cpp里引入PluginInstance.h(注意要在cpp里引入，不要在.h中引入，否则可能会产生符号冲突)
6.在可反射类的cpp里用RX_PLUGIN_EXPORT_CLASS宏将类导出到插件接口中

二.使用插件
1.使用RXPluginLoader加载动态库
2.使用RXPluginLoader::GetPluginInterface方法获取插件接口
3.使用插件接口的RXPluginInterface::LoadClass方法获取类的元信息
4.你就可以用元信息创建对象以及使用对象的方法等功能
*/

struct __RXPLUGIN_DATA {
	std::string pluginName;
	std::unordered_map<std::string, MetaInfo> metas;
	std::unordered_map<std::string, std::string> attribute;
	std::string infomation;
};

//与类库的交互接口，它继承了enable_shared_from_this ,当类库被卸载时，可以感知到它是否被卸载
class REFLECTX_EXPORT RXPluginInterface:public std::enable_shared_from_this<RXPluginInterface>
{
public:
	RXPluginInterface(const __RXPLUGIN_DATA& data);
	MetaInfo LoadClass(const char* className) const;
	std::vector<std::string> ClassNames() const;
	std::string GetName() const;
	std::string GetAttribute(const char* name) const;
	std::vector<std::string> GetAttributeNames() const;
	std::string GetInfomation() const;

protected:
	__RXPLUGIN_DATA data;
};

class REFLECTX_EXPORT RXPluginImplment:public RXPluginInterface
{
public:
	RXPluginImplment(const __RXPLUGIN_DATA& data);
	void RegisterMetaInfo(const MetaInfo& meta);
};

class REFLECTX_EXPORT RXPluginLoader
{
public:
	RXPluginLoader();
	~RXPluginLoader();

	bool load(const char* lib);

	void unLoad();

	bool isLoaded() const;

	RXPluginInterface* GetPluginInterface();

private:
	struct Private;
	std::unique_ptr<Private> _P;
};

//声明一个插件接口
#define RX_PLUGIN_DECL RXPluginImplment* PluginInstance(); \
	extern "C" DECL_EXPORT RXPluginInterface* LoadPlugin(); \

//定义一个插件接口
#define RX_PLUGIN_BEGIN(name) \
\
RXPluginInterface *LoadPlugin()\
{\
	return PluginInstance();\
}\
RXPluginImplment * PluginInstance()\
{\
	static __RXPLUGIN_DATA data = []() {\
		__RXPLUGIN_DATA	data;\
		data.pluginName = #name;\


//定义插件的属性
#define RX_PLUGIN_ATTRIBUTE(name,value) data.attribute[name] = value;

//开始插件介绍
#define RX_PLUGIN_INFORMATION_BEGIN data.infomation = ""


//结束插件介绍
#define RX_PLUGIN_INFOMATION_END ;

//结束定义插件接口
#define RX_PLUGIN_END \
		return data;\
}();\
static RXPluginImplment instance(data);\
return &instance;\
}\


//将类导出到插件接口中
#define RX_PLUGIN_EXPORT_CLASS(className) \
void className##Initialize(){\
PluginInstance()->RegisterMetaInfo(className::staticMetaInfo());\
}\
INITIALIZE_FUN(className##Initialize)\

#endif // !RX_PLUGIN_H

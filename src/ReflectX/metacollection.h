﻿#ifndef METACOLLECTION_H
#define METACOLLECTION_H

#include "metatypedef_p.h"

class MetaCollectionIterator {
public:



};

class MetaCollection {
public:
	MetaCollection(MetaCollection_PPtr P);


	E_CollectionType CollectionType() const;
	std::string TypeName() const;
	MetaTypeId TypeId() const;
	MetaTypeId ValueTypeId() const;

	

private:
	MetaCollection_PPtr _P;
};

#endif // !METACOLLECTION_H

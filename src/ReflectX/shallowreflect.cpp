﻿#include "metacreator.h"
#include "shallowreflect.h"

MetaInfo ShallowReflect::staticMetaInfo = [](){
//构建元信息
MetaCreator<ShallowReflect> ret("ShallowReflect");

ret.addDefaultConstructor();//添加默认构造函数
ret.addProperty("number").addGetter(&ShallowReflect::GetNumber).addSetter(&ShallowReflect::SetNumber);
ret.addFunction("Print").setFunction(&ShallowReflect::Print);
ret.addFunction("OnClicked").setFunction(&ShallowReflect::OnClicked);

ret.addSignal("clicked").setSignal(&ShallowReflect::clicked);

return ret.finish();
}();

ShallowReflect::ShallowReflect()
{
    number = 0;
}

void ShallowReflect::SetNumber(int n)
{
    number = n;
}

int ShallowReflect::GetNumber() const
{
    return number;
}

int ShallowReflect::Print(int r)
{
    std::cout<<"Print"<<r<<std::endl;
    return r++;
}

void ShallowReflect::OnClicked(int value)
{
    std::cout<<value<<std::endl;
}

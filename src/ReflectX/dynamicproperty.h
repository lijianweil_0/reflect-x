﻿#ifndef DYNAMICPROPERTY_H
#define DYNAMICPROPERTY_H

#include <map>

#include "variant.h"

class REFLECTX_EXPORT DynamicProperty
{
public:
    virtual ~DynamicProperty(){};

    Variant GetProperty(const char* name);
    void SetProperty(const char* name, Variant var);
private:
    std::map<std::string,Variant> m_propertys;
};

#endif // DYNAMICPROPERTY_H

﻿#include <iostream>

#include "metasignal.h"
#include "metafunction.h"
#include "Reflectable.h"


MetaInfo Reflectable::staticMetaInfo() {
		static MetaInfo Instance = []() {
		typedef Reflectable ThisType;
		MetaCreator<Reflectable> meta("Reflectable");

		return meta.finish(); 
	}(); 
		return Instance; 
}
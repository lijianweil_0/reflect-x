﻿#ifndef REMOTEOBJECTHOST_H
#define REMOTEOBJECTHOST_H

#include <memory>
#include <remoteobjectnode.h>

class RemoteObject;
class RemoteObjectHostPrivate;
struct RemoteObjectHostData;
struct RemoteObjectClientData;


class REFLECTX_EXPORT RemoteObjectHost:public RemoteObjectNodeBase{
private:
    RemoteObjectHost(RemoteObjectHostPrivate* P);
public:
    RemoteObjectHost();
    ~RemoteObjectHost();

    void Listen(std::string host);
    void close() override;

    void enableRemoting(RemoteObject* obj);
    void enableRemoting(RemoteObject* obj,std::string shareName);
    void stopRemoting(RemoteObject* obj);

	RemoteObject* acquire(std::string sharedName);
private:
    void StartRemotingNotify(const std::string& shareName);
	void SendObjectList(ClientId id);
    void SendObjectReply(ClientId id,RemoteObjectHostData* obj);
    void SendMetaReply(ClientId id,RemoteObjectHostData* obj);
	void SendNewObject(ClientId id, RemoteObjectHostData* obj);
    RemoteObjectHostData* FindObjectData(RemoteObject* obj);
    void slotOnSignalInvoked(RemoteObject* source,int index,const void** param,const std::vector<MetaTypeId> types);
    RemoteObjectClientData *GetOrAcquireClientData(ClientId id);
    void slotOnPropertyChanged(RemoteObject* source,int index,const void* value,MetaTypeId type);
	void OutputValueFix(Variant& value);
	void InputValueFix(Variant& value,MetaTypeId targetType);
protected:
    void OnMessage(Message* message) override;
    void UnRegister(RemoteObject* obj) override;
private:
    RemoteObjectHostPrivate* _d();
};

#endif // REMOTEOBJECTHOST_H

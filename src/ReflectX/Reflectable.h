﻿#ifndef REFLECTABLE_H
#define REFLECTABLE_H

#include <metainfo.h>
#include <metacreator.h>

class MetaInfo;

//任何子类继承该类时必须保证该父类在第一顺位
//继承该类的子类可以获得以下特性
//1.从实例获取到元对象
//2.获得一个用于将槽函数规范化执行的ConnectionRegistry


class REFLECTX_EXPORT Reflectable{
public:
	static constexpr char* ClassName = "Reflectable";
	typedef Reflectable ThisType; 
	static MetaInfo staticMetaInfo();
    virtual MetaInfo GetMetaInfo() const { return staticMetaInfo(); };
};

DECL_VARIANT_TYPE(Reflectable*)


//每个继承自Reflectable的类都必须有此宏，如果类具有命名空间，则宏内必须填带有命名空间的类名
#define REFLECTABLE(This,Super) public: \
static constexpr char* ClassName = #This;\
typedef This ThisType ;\
typedef Super SuperType;\
static MetaInfo staticMetaInfo();\
MetaInfo GetMetaInfo() const override{return staticMetaInfo();};\


#define BeginMetaData(This)\
MetaInfo This::staticMetaInfo(){\
static MetaInfo Instance = [](){\
typedef This ThisType;\
MetaCreator<This> meta(This::ClassName,This::SuperType::staticMetaInfo());\


#define EndMetaData \
return meta.finish();\
}();\
return Instance;\
}\


#define META_DefaultConstructor meta.addDefaultConstructor();\

#define META_BEGIN_CONSTRUCTOR(...) meta.addConstructor().setConstructor<__VA_ARGS__>()

#define META_END_CONSTRUCTOR  .finish();

#define META_CopyConstructor meta.addCopyConstructor();\

#define META_BEGIN_FUNCTION(Name) meta.addFunction(#Name).setFunction(&ThisType::Name)

//该宏用于应对重载函数情况
#define META_BEGIN_FUNCTION_X(Name,...) meta.addFunction(#Name).setFunction(OverLoad<__VA_ARGS__>::of(&ThisType::Name))

#define META_END_FUNCTION  .finish();

#define META_BEGIN_PROPERTY(Name) meta.addProperty(#Name)

#define META_ADD_GETTER(Getter) .addGetter(&ThisType::Getter)

#define META_ADD_SETTER(Setter) .addSetter(&ThisType::Setter)

#define META_ADD_NOTIFIER(Notifier) .addNotifier(#Notifier)

#define META_END_PROPERTY .finish();

#define META_BEGIN_SIGNAL(Name) meta.addSignal(#Name).setSignal(&ThisType::Name)

#define META_END_SIGNAL .finish();

#define META_CLASS_INFO(Content) meta.addClassInfo(#Content);

#define META_MEMBER_INFO(Content) .addInfo(#Content)

//简单定义属性
#define DECL_SIMPE_PROPERTY(type,name) \
public:\
void Set##name(type value){\
	if(m_##name == value)return;\
	m_##name = value;\
	name##Changed(m_##name);\
}\
type Get##name() const{\
	return m_##name;\
}\
Signal<type> name##Changed;\
private:\
type m_##name;\

//简单定义属性的元数据
#define META_SIMPE_PROPERTY_BEGIN(name) \
META_BEGIN_SIGNAL(name##Changed)\
META_MEMBER_INFO(RO_NOTIFIER)\
META_END_SIGNAL;\
\
META_BEGIN_PROPERTY(name)\
META_ADD_GETTER(Get##name)\
META_ADD_SETTER(Set##name)\
META_ADD_NOTIFIER(name##Changed)\
META_MEMBER_INFO(RO_PROPERTY)\


#define META_SIMPE_PROPERTY_END \
META_END_PROPERTY;

#endif // REFLECTABLE_H

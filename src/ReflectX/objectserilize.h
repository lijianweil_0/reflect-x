﻿#ifndef OBJECT_SERILIZE_H
#define OBJECT_SERILIZE_H

#include "ReflectX_global.h"

#include <datastream.h>
#include <metatype.h>
#include <metainfo.h>
#include <Reflectable.h>

class Reflectable;

//自定义序列化反序列化器
class IObjectSerilizer {
public:
	//使用已构造好的对象进行序列化
	virtual void Serilize(void* target, DataOutputStream& stream) = 0;
	//对已构造好的对象进行反序列化
	virtual void UnSerilize(void* target,DataInputStream& stream) = 0;
};

/*
//默认序列化规则：
1.仅序列化属性
2.被序列化的属性必须可读写
3.如果类型有某个属性是你不想被序列化的，为该属性的Info中添加“DisableSerilize”

*/

class REFLECTX_EXPORT ObejctSerilize
{
public:
	//将对象序列化
	static ByteArray Serilize(void* target, const MetaInfo& meta);
	static ByteArray SerilizeObj(Reflectable* target);

	
	struct UnSerilizeResult
	{
		bool isSuccess;
		MetaInfo meta;
		void* target;
	};

	//将对象反序列化,如果target为NULL则创建默认对象，meta为与target对应的元对象，target为空时，meta无效
	static UnSerilizeResult UnSerilize(const ByteArray& s, void* target = NULL, MetaInfo meta = {});
	static Reflectable* UnSerilizeObj(const ByteArray& s, Reflectable* target = NULL);


	static void RegisterSerilizer(const MetaInfo& meta, IObjectSerilizer* serilizer = NULL);
	template<typename T>
	static typename std::enable_if<std::is_base_of<Reflectable, T>::value,void>::type RegisterSerilizer(IObjectSerilizer* serilizer = NULL) {
		RegisterSerilizer(T::staticMetaInfo(), serilizer);
	}
};

#endif
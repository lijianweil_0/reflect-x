﻿#include "filestream.h"

FileInputStream::FileInputStream(const char * filePath, std::ios::openmode mode)
{
	m_stream.open(filePath, mode);
}

int FileInputStream::doRead(unsigned char * buffer, int readLen) throw(StreamException)
{
	if (!m_stream) {
		throw StreamException("文件打开失败");
	}

	return m_stream.readsome((char*)buffer, readLen);
}

int FileInputStream::avaliable() const
{
	auto cur = m_stream.tellg();
	m_stream.seekg(0, std::ios::end);
	auto len = m_stream.tellg() - cur;
	m_stream.seekg(cur);

	return len;
}

FileOutputStream::FileOutputStream(const char * filePath, std::ios::openmode mode)
{
	m_stream.open(filePath, mode);
}

void FileOutputStream::write(const unsigned char * buffer, int len) throw(StreamException)
{
	if (!m_stream) {
		throw StreamException("文件打开失败");
	}

	m_stream.write((char*)buffer,len);
}

﻿cmake_minimum_required(VERSION 3.5)

project(ReflectX VERSION 0.1 LANGUAGES CXX)

set(CMAKE_INCLUDE_CURRENT_DIR ON)
set(CMAKE_CXX_STANDARD 11)
set(CMAKE_CXX_STANDARD_REQUIRED ON)

set(ProgramName ReflectX)

add_library(${ProgramName} SHARED
  ReflectX_global.h
  datastream.cpp
  datastream.h
  dynamicproperty.cpp
  dynamicproperty.h
  dynamicremoteobject.cpp
  dynamicremoteobject.h
  eventloop.cpp
  eventloop.h
  messagechannel.cpp
  messagechannel.h
  metacreator.cpp
  metacreator.h
  metafunction.cpp
  metafunction.h
  metainfo.cpp
  metainfo.h
  metaconstructor.h
  metaconstructor.cpp
  metaproperty.cpp
  metaproperty.h
  metasignal.cpp
  metasignal.h
  metatest.cpp
  metatest.h
  metatype.cpp
  metatype.h
  objecttrace.cpp
  objecttrace.h
  Reflectable.cpp
  Reflectable.h
  remoteobject.cpp
  remoteobject.h
  remoteobject_p.cpp
  remoteobject_p.h
  remoteobjecthost.cpp
  remoteobjecthost.h
  remoteobjectnode.cpp
  remoteobjectnode.h
  rotest.cpp
  rotest.h
  shallowreflect.cpp
  shallowreflect.h
  signalslot.cpp
  signalslot.h
  stringutil.cpp
  stringutil.h
  tcpsocket.cpp
  tcpsocket.h
  typetrate.cpp
  typetrate.h
  variant.cpp
  variant.h
  workcondition.cpp
  workcondition.h
  workservice.cpp
  workservice.h
  shareddata.h
  shareddata.cpp
  dynamicmetacreator.h
  dynamicmetacreator.cpp
  reflectutils.h
  reflectutils.cpp
  Timer.cpp
  Timer.h
  objectserilize.cpp
  objectserilize.h
  statemachine.h
  statemachine.cpp
  threadsafe.cpp
  threadsafe.h
  ReflectableObject.cpp
  ReflectableObject.h
  ObjectPool.cpp
  ObjectPool.h
  classfactory.cpp
  classfactory.h
  COWTypes.cpp
  COWTypes.h
  metamap.cpp
  metamap.h
  metacollection.cpp
  metacollection.h
  metatypedef_p.h
  textstream.cpp
  textstream.h
  stream.cpp
  stream.h
  filestream.h
  filestream.cpp
  bytearraystream.cpp
  bytearraystream.h
  bufferedstream.cpp
  bufferedstream.h
  threadpool.cpp
  threadpool.h
  enumrate.h
  rxplugin.h
  rxplugin.cpp
  initializefunction.h
  RXTypes.h
  RXTypes.cpp
  UniverseFunction.cpp
  UniverseFunction.h
)

if(WIN32)
    set(LIB_SOCKET wsock32)
endif()

if(UNIX)
	set(LIB_PTHREAD -pthread)
endif()
target_include_directories(${ProgramName}
PUBLIC ${CMAKE_CURRENT_SOURCE_DIR}/../InteUtils
)
target_link_libraries(${ProgramName} ${LIB_SOCKET} ${LIB_PTHREAD} InteUtils)
set_target_properties(${PROJECT_NAME} PROPERTIES RUNTIME_OUTPUT_DIRECTORY ${CMAKE_LIBRARY_OUTPUT_DIRECTORY})
target_compile_definitions(ReflectX PRIVATE REFLECTX_LIBRARY)

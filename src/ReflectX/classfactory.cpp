﻿#include "classfactory.h"

ClassFactory::CreateResult ClassFactory::Create(const char * className)
{
	CreateResult ret{};

	if (metas.count(className)) {
		ret.info = metas[className];
		ret.obj = ret.info.Create();
	}

	return ret;
}

void ClassFactory::RegisterClass(MetaInfo info)
{
	metas[info.ClassName()] = info;
}

void ClassFactory::UnRegisterClass(const char * className)
{
	metas.erase(className);
}

MetaInfo ClassFactory::FindClass(const char * className)
{
	if (metas.count(className)) {
		return metas[className];
	}
	return MetaInfo();
}

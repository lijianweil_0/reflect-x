﻿#ifndef METATEST_H
#define METATEST_H

#include "Reflectable.h"
#include "metainfo.h"

//深反射示例

//深反射的基类需要REFLECTABLE_BASE宏以及BeginMetaDataBase宏并且继承Reflectable接口
//继承自深反射基类的类需要将REFLECTABLE_BASE换成REFLECTABLE，以及将BeginMetaDataBase换成BeginMetaData，无需重复继承Reflectable接口

class REFLECTX_EXPORT MetaTest:public Reflectable
{
public:
    REFLECTABLE(MetaTest, Reflectable)
    MetaTest();

    void SetNumber(int n);
    int GetNumber()const;
    int Print(int r);

    void OnClicked(int value);
    Signal<int> clicked;

private:
    int number;
};

#endif // METATEST_H

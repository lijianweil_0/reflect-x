﻿#ifndef REFLECTUTILS_H
#define REFLECTUTILS_H

#include <ReflectX_global.h>

#include <string>
#include <vector>

#include <metatype.h>

class REFLECTX_EXPORT ReflectUtils
{
public:
    static std::string GetSignature(const std::string & name,const std::vector<MetaTypeId>& Arguments);
	static bool ParseSignature(const std::string & signature,std::string &name, std::vector<MetaTypeId>& Arguments);
};

#endif // REFLECTUTILS_H

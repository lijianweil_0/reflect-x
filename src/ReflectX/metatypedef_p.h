﻿#ifndef METATYPEDEF_H
#define METATYPEDEF_H

#include <memory>

#include <map>
#include <unordered_map>
#include <string>
#include <vector>
#include <set>
#include <unordered_set>
#include <list>
#include <stdint.h>
#include <functional>
#include <RXTypes.h>



class MetaMap;
class MetaCollection;
typedef unsigned int MetaTypeId;


template<typename T>
class IsMap
{
public:
	static constexpr bool value = false;
	static constexpr char name[] = "";
	typedef void keyType;
	typedef void valueType;
};

//概念定义 用于定义自己的映射表类型
#define DECL_MAP_TYPE(type) \
template<typename K, typename V>\
class IsMap<type<K, V>>\
{\
public:\
	static constexpr bool value = true;\
	static constexpr char name[] = #type;\
	typedef K keyType;\
	typedef V valueType;\
};\

//MapTypeDecl
DECL_MAP_TYPE(std::map);
DECL_MAP_TYPE(std::unordered_map);



template<typename T>
class IsList
{
public:
	static constexpr bool value = false;
	static constexpr char name[] = "";
	typedef void valueType;
};


//定义线性表类型
#define DECL_List_Type(type) \
template<typename T>\
class IsList<type<T>>\
{\
public:\
	static constexpr bool value = true;\
	static constexpr char name[] = #type;\
    typedef T valueType;\
}; \

DECL_List_Type(std::vector)
DECL_List_Type(std::set)
DECL_List_Type(std::unordered_set)
DECL_List_Type(std::list)


template<typename T>
class IsContainer {
public:
	static constexpr bool value = IsList<T>::value || IsMap<T>::value;
};


template <typename T>
struct __TypeNameDecl {
	static const bool vaild = false;
	static constexpr char* Name = "";
};

#define DECL_VARIANT_TYPE(typeName) \
template <>\
struct __TypeNameDecl<typeName>{\
    static const bool vaild = true;\
    static constexpr char* Name = #typeName;\
};\




enum E_CollectionType {
	EC_None,
	EC_ArrayList,//连续线性列表
	EC_LinkedList,//链式线性列表
	EC_Set,//无序，非重复集合
};


template<typename T>
class GetCollectionType
{
public:
	typedef void ValueType;
	static constexpr char baseName[] = "";
	static constexpr E_CollectionType value = EC_None;
};

//概念定义 用于定义自己的线性列表类型
#define DECL_VECTOR_TYPE(className,type) \
template<typename T>\
class GetCollectionType<className<T>>\
{\
public:\
	typedef T ValueType;\
	static constexpr char baseName[] = #className;\
	static constexpr E_CollectionType value = type;\
};\

struct MetaCollection_P
{
	E_CollectionType collectionType;

	MetaTypeId typeId;
	MetaTypeId ValueType;
	std::string BaseTypeName;

	std::function<void(int, void*)> Gettter;//仅对线性表有效
	std::function<void(int, const void*)> Setter;

	std::function<bool(void*, const void*)> Contains;

	typedef void* Iterator;


};

typedef std::shared_ptr<MetaCollection_P> MetaCollection_PPtr;

//VectorTypeDecl
DECL_VECTOR_TYPE(std::vector, EC_ArrayList);



//BasicTypeDecl
DECL_VARIANT_TYPE(bool);
DECL_VARIANT_TYPE(char);
DECL_VARIANT_TYPE(signed char);
DECL_VARIANT_TYPE(unsigned char);
DECL_VARIANT_TYPE(int);
DECL_VARIANT_TYPE(unsigned int);
DECL_VARIANT_TYPE(short);
DECL_VARIANT_TYPE(unsigned short);
DECL_VARIANT_TYPE(long);
DECL_VARIANT_TYPE(unsigned long);
DECL_VARIANT_TYPE(long long);
DECL_VARIANT_TYPE(unsigned long long);
DECL_VARIANT_TYPE(float);
DECL_VARIANT_TYPE(double);
DECL_VARIANT_TYPE(std::string);
DECL_VARIANT_TYPE(std::wstring);
DECL_VARIANT_TYPE(ByteArray);
DECL_VARIANT_TYPE(Size2F);
DECL_VARIANT_TYPE(Size2I);
DECL_VARIANT_TYPE(Size3F);
DECL_VARIANT_TYPE(Size3I);
DECL_VARIANT_TYPE(Point2F);
DECL_VARIANT_TYPE(Point2I);
DECL_VARIANT_TYPE(Point3F);
DECL_VARIANT_TYPE(Point3I);
DECL_VARIANT_TYPE(Rect2F);
DECL_VARIANT_TYPE(Rect2I);
DECL_VARIANT_TYPE(Rect3F);
DECL_VARIANT_TYPE(Rect3I);
DECL_VARIANT_TYPE(ColorF);
DECL_VARIANT_TYPE(ColorI);
DECL_VARIANT_TYPE(Image);
DECL_VARIANT_TYPE(Variant);
DECL_VARIANT_TYPE(VariantList);
DECL_VARIANT_TYPE(VariantWMap);
DECL_VARIANT_TYPE(VariantMap);



#endif

﻿#ifndef SHAREDDATA_H
#define SHAREDDATA_H
#include <memory>

template <typename T>
class SharedDataPointer
{
public:
	typedef SharedDataPointer<T> ThisType;
public:
	SharedDataPointer() {
		
	}
	SharedDataPointer(T* d) {
		data.reset(d);
	}
	SharedDataPointer(const ThisType& other):data(other.data) {

	}
	~SharedDataPointer() {

	}

	ThisType& operator = (const ThisType& other) {
		data = other.data;
		return *this;
	}

	bool operator == (const ThisType& other)const {
		return data == other.data;
	}

	const T& operator* () const {
		return *data.get();
	}

	T& operator*() {
		if (data.use_count() > 1) {
			std::shared_ptr<T> temp(new T(*data.get()));
			data = temp;
		}
		else if (data.get() == NULL)
		{
			data.reset(new T());
		}
		return *data.get();
	}

	T* operator->() const {
		return data.get();
	}

	T* operator->() {
		if (data.use_count() > 1) {
			std::shared_ptr<T> temp(new T(*data.get()));
			data = temp;
		}
		else if (data.get() == NULL)
		{
			data.reset(new T());
		}
		return data.get();
	}

	bool IsEmpty() const
	{
		return data.get() != NULL;
	}

private:
	std::shared_ptr<T> data;
};

#endif // SHAREDDATA_H

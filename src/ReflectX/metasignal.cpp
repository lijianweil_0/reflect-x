﻿#include "metainfo.h"
#include "metasignal.h"
#include "reflectutils.h"

MetaSignal::MetaSignal(IMetaInfoBackend* meta, int idx)
{
    this->meta = meta? meta->shared_from_this():NULL;
    this->idx = idx;

	localMeta = IMetaInfoBackend::ToLocalSignal(idx, meta, localIdx);
}

MetaSignal::MetaSignal()
{
    idx = 0;
	localIdx = 0;
	localMeta = NULL;
}

int MetaSignal::Index() const
{
    return idx;
}

bool MetaSignal::isValid() const
{
	return (bool)meta;
}

std::string MetaSignal::Name() const
{
    if(!isValid()){
        return "";
    }

	auto &&info = LocalMeta()->GetSignalInfo(LocalIndex());
    return info.Name;
}

std::string MetaSignal::GetSignature() const
{
    if(!isValid()){
        return "";
    }

	auto &&info = LocalMeta()->GetSignalInfo(LocalIndex());
	return ReflectUtils::GetSignature(info.Name, info.ArgTypes);
}

std::vector<MetaTypeId> MetaSignal::GetParamTypes() const
{
    if(!isValid()){
        return std::vector<MetaTypeId>();
    }

	auto &&info = LocalMeta()->GetSignalInfo(LocalIndex());
	return info.ArgTypes;
}

GenericSignal *MetaSignal::GetSignal(void *o) const
{
    if(!isValid()){
        return NULL;
    }

	return LocalMeta()->GetSignal(o, LocalIndex());
}

std::vector<std::string> MetaSignal::GetSignalInfo() const
{
     if(!isValid()){
        return std::vector<std::string>();
    }

	return LocalMeta()->GetSignalInfo(LocalIndex()).Infos;
}

int MetaSignal::LocalIndex() const
{
	return localIdx;
}

IMetaInfoBackend * MetaSignal::LocalMeta() const
{
	return localMeta;
}

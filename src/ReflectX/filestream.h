﻿#ifndef FILESTREAM_H
#define FILESTREAM_H

#include <stream.h>
#include <filestream.h>
#include <fstream>

//缓冲流，用于提高io效率。
//缓冲输入流，内含一个缓冲，和一个buffersize.当读取数据时，它实际读取的数据为max(buffersize,readLen)。
class REFLECTX_EXPORT FileInputStream :public InputStream {
public:
	FileInputStream(const char* filePath, std::ios::openmode);

	
	int avaliable() const override;

protected:
	int doRead(unsigned char* buffer, int readLen) throw(StreamException)override;
private:
	mutable std::ifstream m_stream;
};

//缓冲输出流，内含一个缓冲，和一个buffersize.当写出数据时，它会将数据缓存到buffer中，当数据超过缓冲大小时，它才会将实际的数据写出
class REFLECTX_EXPORT FileOutputStream :public OutputStream {
public:
	FileOutputStream(const char* filePath, std::ios::openmode);
	void write(const unsigned char* buffer, int len) throw (StreamException)override;
private:
	std::ofstream m_stream;
};


#endif
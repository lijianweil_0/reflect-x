﻿#ifndef STREAM_H
#define STREAM_H

#include <exception>
#include <stdexcept>
#include <string>

//#include <signalslot.h>
#include <ReflectX_global.h>


class REFLECTX_EXPORT StreamException :public std::logic_error
{
public:
	StreamException(const char* msg = "");
};

class REFLECTX_EXPORT StreamDisconnectedException :public StreamException
{
public:
	StreamDisconnectedException(const char* msg = "");
};

class REFLECTX_EXPORT InputStream
{
public:
	InputStream();
	InputStream(InputStream* next);
	int read() throw(StreamException);
	int read(unsigned char* buffer,int readLen);
	//流内还有多少字节可以直接读取而不会造成堵塞
	virtual int avaliable() const;
	size_t byteReaded() const;//从流被创建开始，已经读取了多少字节
public:
	//Signal<> readyRead;
protected:
	 InputStream* GetNext() const;
	 virtual int doRead(unsigned char* buffer, int readLen) = 0;
private:
	InputStream* next;
	size_t ReadedNum;
};


class REFLECTX_EXPORT OutputStream {
public:
	OutputStream();
	OutputStream(OutputStream* next);

	virtual void write(int val) throw (StreamException);
	virtual void write(const unsigned char* buffer, int len) throw (StreamException) = 0;

	virtual void flush();
protected:
	OutputStream* GetNext();
private:
	OutputStream* next;

};


#endif // !STREAM_H

﻿#ifndef WORKSERVICE_H
#define WORKSERVICE_H
#include <memory>
#include <functional>
#include <vector>

#include <ReflectX_global.h>

class REFLECTX_EXPORT WorkService
{
public:
    typedef std::function<void()> EventType;
    typedef unsigned long long EventId;
public:

    WorkService();
    ~WorkService();

    EventId Post(const EventType& o);

    void Cancel(EventId event);//取消一个还在队列中的事件

    void ProcessOne();//处理一个事件，无事件则返回
    void ProcessAll();//处理所有事件,无事件则返回
    void Run();//持续一个一个的处理事件，事件为空则等待

    void close();//关闭事件循环并使所有处于等待状态中的线程唤醒，当该函数执行完毕时，所有线程均已完成工作并退出循环
private:
    EventType WaitForOne();
    EventType PullOne();
    std::vector<EventType> PullAll();

private:
struct Private;
 std::unique_ptr<Private> _P;
};

#endif // WORKSERVICE_H

﻿#ifndef DYNAMICMETACREATOR_H
#define DYNAMICMETACREATOR_H

#include <ReflectX_global.h>
#include <metainfo.h>
#include <signalslot.h>
#include <metacreator.h>

class REFLECTX_EXPORT DynamicMetaCreator
{
public:
    DynamicMetaCreator(int size,std::function<void(void*)> destructor,const char* className,const MetaInfo& Super, std::function<Reflectable*(void*)> caster = {});
	DynamicMetaCreator(int size, std::function<void(void*)> destructor, const char* className, std::function<Reflectable*(void*)> caster = {});

    void addDefaultConstructor(std::function<void(void* buffer)> fun);
    void addCopyConstructor(std::function<void(void* buffer,const void* other)> fun);

public:
    class PropertyHelper{
    public:
        PropertyHelper(__StandardMetaInfoHelper::ClassData* meta,const char* name,MetaTypeId type);

        PropertyHelper& addGetter(const FunctionAny& getter);
        PropertyHelper& addSetter(const FunctionAny& setter);
        PropertyHelper& addNotifier(const char* name);
        PropertyHelper& addInfo(const char*m);
        void finish();
    private:
		__StandardMetaInfoHelper::PropertyInfo info;
        __StandardMetaInfoHelper::ClassData* meta;
        friend class DynamicMetaCreator;
    };

    PropertyHelper addProperty(const char* name,MetaTypeId type);

    class FunctionHelper{
    public:
        FunctionHelper(__StandardMetaInfoHelper::ClassData* meta,const char* name,MetaTypeId ReturnType,const std::vector<MetaTypeId>& argTypes);

        FunctionHelper& setFunction(FunctionAny fun);
        FunctionHelper& addInfo(const char* m);
        void finish();
    private:
		__StandardMetaInfoHelper::FunctionInfo info;
		__StandardMetaInfoHelper::ClassData* meta;
        friend class DynamicMetaCreator;
    };

    FunctionHelper addFunction(const char* name,MetaTypeId ReturnType,const std::vector<MetaTypeId>& argTypes);

    class SignalHelper
    {
    public:
        SignalHelper(__StandardMetaInfoHelper::ClassData* meta,const char* name,const std::vector<MetaTypeId>& argTypes);

        SignalHelper& setSignal(std::function<GenericSignal*(void*)>);
        SignalHelper& addInfo(const char* e);

        void finish();
    private:
        __StandardMetaInfoHelper::SignalInfo info;
		__StandardMetaInfoHelper::ClassData* meta;
        friend class DynamicMetaCreator;
    };

    SignalHelper addSignal(const char* name,const std::vector<MetaTypeId>& argTypes);

    void addClassInfo(const char* content);

    MetaInfo finish();
private:
    __StandardMetaInfoHelper::ClassData result;
	IMetaInfoBackend* super;
};

#endif // DYNAMICMETACREATOR_H

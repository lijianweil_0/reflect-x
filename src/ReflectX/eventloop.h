﻿#ifndef EVENTLOOP_H
#define EVENTLOOP_H
#include <memory>
#include <mutex>
#include <thread>
#include <set>
#include <vector>
#include <functional>
#include <tuple>

#include <ReflectX_global.h>

namespace ThreadSafe
{
	class ThreadSafeBase;
	class Event;
}

class EventRegister;
class EventLoopManager;


class REFLECTX_EXPORT EventLoop
{
public:
    typedef std::thread::id ThreadId;

public:
    EventLoop();
    ~EventLoop();

    int exec();
    void exit(int code = 0);//立即关闭事件循环
public:
	static void PostEvent(ThreadSafe::ThreadSafeBase* receiver, ThreadSafe::Event* event);
	static void CancelEvent(ThreadSafe::Event* event);

private:
	static void Register(ThreadSafe::ThreadSafeBase* obj);
	static void MoveThread(ThreadSafe::ThreadSafeBase* obj, ThreadId NewThread);
	static ThreadId GetThread(ThreadSafe::ThreadSafeBase* obj);
	static void UnRegister(ThreadSafe::ThreadSafeBase* obj);
	static EventLoop* CurrentLoop();//获取当前线程最顶部的事件循环
private:
    struct Private;
    std::unique_ptr<Private> _P;
	friend class ThreadSafe::ThreadSafeBase;
};

#endif // EVENTLOOP_H

﻿#ifndef BYTEARRAYSTREAM_H
#define BYTEARRAYSTREAM_H

#include <metatypedef_p.h>
#include <stream.h>
#include <mutex>
#include <memory>
#include <condition_variable>

class REFLECTX_EXPORT ByteArrayBuffer {
public:
	ByteArrayBuffer();
	ByteArrayBuffer(const unsigned char* buffer, int bufferLen);

	~ByteArrayBuffer();

	int Avaliable() const;//缓冲中剩余可读的数据

	void Close();//关闭缓冲，此时再读取数据会抛异常
	void Finish();//结束缓冲，向缓冲内写入结束符

	ByteArray GetContent() const;//获取缓冲内部的数据

	void Reset();//重置缓冲状态，清空缓存内容

	//读取指定长度字节,如遇到结束符会仅读取剩余可读数据，如没有结束符，但剩余可读数据小于要读取的量，则阻塞直到读取到指定长度数据，若缓冲已关闭，则会抛异常
	int Read(unsigned char* buffer, int readLen) throw(StreamException);
	//写入指定长度字节，如果缓冲已结束或关闭，则抛出异常
	bool Write(const unsigned char* buffer, int len) throw (StreamException);
private:
	ByteArray buffer;
	bool isClosed;
	bool isFinished;
	mutable std::mutex  m_mutex;
	mutable std::condition_variable m_con;
};

typedef std::shared_ptr<ByteArrayBuffer> ByteArrayBufferPtr;

//用于读取一段缓存中的内容

class REFLECTX_EXPORT BytearrayInputStream :public InputStream{
public:
	BytearrayInputStream();
	BytearrayInputStream(const unsigned char* buffer,int bufferLen);//使用已知数据构造输入流
	BytearrayInputStream(ByteArrayBufferPtr buffer);//使用已知缓冲构造输入流。当输入流和输出流共享缓冲时，它们可以看做被连接在一起的管道
	
	int avaliable() const override;

	ByteArrayBufferPtr GetBuffer();//获取内部缓冲

	void close();//关闭流,所有正在读取数据的线程会解除阻塞
protected:
	int doRead(unsigned char* buffer, int readLen) throw(StreamException)override;

private:
	ByteArrayBufferPtr buffer;
};

class REFLECTX_EXPORT BytearrayOutputStream :public OutputStream {
public:
	BytearrayOutputStream();
	BytearrayOutputStream(ByteArrayBufferPtr buffer);
	void close();//关闭流,所有正在读取数据的线程会解除阻塞
	ByteArrayBufferPtr GetBuffer();//获取内部缓冲
	ByteArray GetByteArray() const;
	void write(const unsigned char* buffer, int len) throw (StreamException) override;
private:
	ByteArrayBufferPtr buffer;
};

#endif // !BYTEARRAYSTREAM_H

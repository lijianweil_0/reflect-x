﻿#ifndef METAPROPERTY_H
#define METAPROPERTY_H
#include <string>

#include "metatype.h"
#include "variant.h"
#include <metainfo.h>
class MetaInfo;
class MetaSignal;

class REFLECTX_EXPORT MetaProperty
{
private:
    MetaProperty(IMetaInfoBackend* meta ,int idx);
public:
    MetaProperty();

	MetaInfo EnclosingMetaInfo() const;

    int Index() const;
    bool isValid() const;
    std::string Name() const;

    bool isStatic() const;
    bool isReadable() const;
    bool isWritable() const;
    bool hasNotifier() const;

    MetaTypeId Type() const;

    Variant GetValue(const void* o) const;
    void SetValue(void* o,Variant value) const;
    MetaSignal GetNotifier() const;

    std::vector<std::string> GetPropertyInfo() const;

private:
	IMetaInfoBackend* LocalMeta() const;
	int LocalIndex() const;
private:
	std::shared_ptr<IMetaInfoBackend> meta;
    int idx;

	IMetaInfoBackend* localMeta;
	int localIdx;
    friend class MetaInfo;
};

#endif // METAPROPERTY_H

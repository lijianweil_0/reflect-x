﻿//#ifndef TEXTSTREAM_H
//#define TEXTSTREAM_H
//
//#include <string>
//
//#include <stream.h>
//
//#include <ReflectX_global.h>
//
//class REFLECTX_EXPORT TextInputStream :public InputStream {
//public:
//	TextInputStream(InputStream* next,const char* codec = "locale");
//
//	virtual int doRead(wchar_t* buffer,int readLen);//读取字符串
//
//	virtual int doRead(unsigned char * buffer, int readLen) override;//该函数被禁用了，只能使用其它函数
//
//	virtual std::wstring Read(int len);//读取指定长度
//	virtual std::wstring ReadUntil(wchar_t* mark);//读取，直到遇到指定字符串
//	virtual std::wstring ReadLine();//读取一行
//public:
//	bool FillUp();
//	bool Fetch();
//
//private:
//	struct Private;
//	std::unique_ptr<Private> _P;
//};
//
//
//
//#endif // ! TEXTSTREAM_H

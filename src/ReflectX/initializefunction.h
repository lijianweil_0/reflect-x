#ifndef  INITIALIAZE_FUNCTION_H
#define INITIALIAZE_FUNCTION_H

#define INITIALIZE_FUN(name) \
class name##Initializer\
{\
public:\
	name##Initializer() {\
	name();\
	}\
}name##ins;\

#define DESTRUCT_FUN(name) \
class name##Destructor\
{\
public:\
	~name##Destructor() {\
	name();\
	}\
}name##des;\

#endif // ! INITIALIAZE_FUNCTION_H

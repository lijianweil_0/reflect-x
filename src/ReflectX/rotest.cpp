﻿#include "rotest.h"

BeginMetaData(ROTest)

META_DefaultConstructor

//注册远程属性
RO_BEGIN_META_PROPERTY(Number)
RO_END_META_PROPERTY

//注册远程函数
RO_BEGIN_META_FUNCTION(Print)
RO_END_META_FUNCTION

EndMetaData

ROTest::ROTest()
{
    Number = 0;
}


int ROTest::GetNumberImp() const{
    return Number;
}

void ROTest::SetNumberImp(int value){
    if(value != Number){
        Number = value;
        NumberChanged(Number);
    }
}

int ROTest::PrintImp(int number){
    return 0;
}

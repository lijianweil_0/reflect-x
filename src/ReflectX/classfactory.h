﻿#ifndef CLASS_FACTORY_H
#define CLASS_FACTORY_H

#include <map>
#include "metainfo.h"

class ClassFactory
{
public:
	struct CreateResult
	{
		void* obj;
		MetaInfo info;
	};

	CreateResult Create(const char* className);

	template<typename T>
	void RegisterClass() {
		RegisterClass(T::staticMetaInfo());
	}

	void RegisterClass(MetaInfo info);
	void UnRegisterClass(const char* className);

	MetaInfo FindClass(const char* className);

private:
	std::map<std::string, MetaInfo> metas;
};



#endif // !CLASS_FACTORY_H

﻿#ifndef ROTEST_H
#define ROTEST_H

#include "remoteobject.h"

class REFLECTX_EXPORT ROTest:public RemoteObject
{
public:
    REFLECTABLE(ROTest,RemoteObject)
    ROTest();

    //声明属性
    RO_DECL_PROPERTY(int,Number);

    //声明函数
    RO_DECL_FUNCTION_1_R(Print,int,int,number);

private:
    int Number;

};

#endif // ROTEST_H

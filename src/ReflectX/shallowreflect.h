﻿#ifndef SHALLOWREFLECT_H
#define SHALLOWREFLECT_H
#include "metainfo.h"

//浅反射示例
class REFLECTX_EXPORT ShallowReflect
{
public:
    static MetaInfo staticMetaInfo;
    ShallowReflect();
    void SetNumber(int n);
    int GetNumber()const;
    int Print(int r);

    void OnClicked(int value);
    Signal<int> clicked;
private:
    int number;
};

#endif // SHALLOWREFLECT_H

#ifndef RXTYPES_H
#define RXTYPES_H

#include <vector>
#include <map>

#include <IUSize.h>
#include <IURect.h>
#include <IUImage.h>
#include <IUColor.h>
#include <IUTypes.h>
#include <IUPoint.h>

#include <shareddata.h>

#include <ReflectX_global.h>

class InputStream;
class OutputStream;
class DataInputStream;
class DataOutputStream;
//定义了一些RX库内增加的一些数据类型

//字节数组，用于存储二进制信息
typedef IUByteArray ByteArray;

//Size 用于表示大小信息
typedef IUSize2F Size2F;
typedef IUSize2I Size2I;
typedef IUSize3F Size3F;
typedef IUSize3I Size3I;

template<typename T,int dim>
DataOutputStream& operator << (DataOutputStream& s, const IUSize<T,dim>& r) {
	for (const auto &i : r)
	{
		s << i;
	}
	return s;
}

template<typename T,int dim>
DataInputStream& operator >> (DataInputStream& s, IUSize<T,dim>& r) {
	for (auto &i : r)
	{
		s >> i;
	}
	
	return s;
}

//Point 用于表示一个点
typedef IUPoint2F Point2F;
typedef IUPoint2I Point2I;
typedef IUPoint3F Point3F;
typedef IUPoint3I Point3I;

template<typename T, int dim>
DataOutputStream& operator << (DataOutputStream& s, const IUPoint<T, dim>& r) {
	for (const auto &i : r)
	{
		s << i;
	}
	return s;
}

template<typename T, int dim>
DataInputStream& operator >> (DataInputStream& s, IUPoint<T, dim>& r) {
	for (auto &i : r)
	{
		s >> i;
	}

	return s;
}


//Rect 用于表示一个矩形框

typedef IURect2F Rect2F;
typedef IURect2I Rect2I;
typedef IURect3F Rect3F;
typedef IURect3I Rect3I;

template<typename T, int dim>
DataOutputStream& operator << (DataOutputStream& s, const IURect<T,dim>& r) {

	s << r.GetPoint();
	s << r.GetSize();
	return s;
}

template<typename T, int dim>
DataInputStream& operator >> (DataInputStream& s, IURect<T, dim>& r) {

	auto point = r.GetPoint();
	auto size = r.GetSize();

	s >> point;
	s >> size;
	r.SetPoint(point);
	r.SetSize(size);

	return s;
}

//Image用于表示一个二维矩阵数据
typedef IUImage Image;
REFLECTX_EXPORT DataOutputStream& operator << (DataOutputStream& s, const Image& r);
REFLECTX_EXPORT DataInputStream& operator >> (DataInputStream& s, Image& r);

//Color用于表示颜色数据
typedef IUColorI ColorI;//整型颜色数据,取值0-255
typedef IUColorF ColorF;//浮点型颜色数据，取值0-1.0

template<typename T>
DataOutputStream& operator << (DataOutputStream& s, const IUColorT<T>& r) {

	s << r.GetR();
	s << r.GetG();
	s << r.GetB();
	s << r.GetA();

	return s;
}

template<typename T>
DataInputStream& operator >> (DataInputStream& s, IUColorT<T>& o){

	T r, g, b, a;
	s >> r >> g >> b >> a;

	o = IUColorT<T>(r, g, b, a);

	return s;
}


//Variant用于存储任意在元系统中存储的类型的数据
class Variant;
typedef std::vector<Variant> VariantList;
typedef std::map<std::wstring, Variant> VariantWMap;
typedef std::map<std::string, Variant> VariantMap;

#endif // !RXTYPES_H

﻿#include "metatest.h"

//在这里写该类的元信息
BeginMetaData(MetaTest)

META_DefaultConstructor

META_BEGIN_PROPERTY(Number)
META_ADD_GETTER(GetNumber)
META_ADD_SETTER(SetNumber)
META_END_PROPERTY

META_BEGIN_FUNCTION(Print)META_END_FUNCTION
META_BEGIN_FUNCTION(OnClicked)META_END_FUNCTION

META_BEGIN_SIGNAL(clicked)META_END_SIGNAL

EndMetaData

MetaTest::MetaTest()
{
    number = 1;
}

void MetaTest::SetNumber(int n)
{
    number = n;
}

int MetaTest::GetNumber() const
{
    return number;
}

int MetaTest::Print(int r)
{
    std::cout<<"Print"<<r<<std::endl;
    return r++;
}

void MetaTest::OnClicked(int value)
{
    std::cout<<value<<std::endl;
}

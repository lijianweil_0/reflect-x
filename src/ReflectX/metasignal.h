﻿#ifndef METASIGNAL_H
#define METASIGNAL_H
#include <string>
#include "metatype.h"
#include "variant.h"
#include <signalslot.h>
#include <metainfo.h>
class MetaInfo;
class REFLECTX_EXPORT MetaSignal
{
private:
    MetaSignal(IMetaInfoBackend* meta,int idx);
public:
    MetaSignal();

    int Index() const;
    bool isValid() const;

    std::string Name() const;
    std::string GetSignature() const;
    std::vector<MetaTypeId> GetParamTypes() const;

    GenericSignal* GetSignal(void* o) const;
    std::vector<std::string> GetSignalInfo() const;
private:
	int LocalIndex() const;
	IMetaInfoBackend* LocalMeta() const;
private:
    std::shared_ptr<IMetaInfoBackend> meta;
    int idx;

	IMetaInfoBackend* localMeta;
	int localIdx;

    friend class MetaInfo;
};

#endif // METASIGNAL_H

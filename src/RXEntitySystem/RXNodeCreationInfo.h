#ifndef RXNODECREATIONINFO_H
#define RXNODECREATIONINFO_H

#include <metatype.h>

#include "RXEntitySystem_global.h"
#include "RXECSDef.h"

//节点创建信息，用于标识一个节点创建时的唯一信息
class RXWorld;

class RXEntitySystem_EXPORT RXNodeCreationInfo
{
public:
	RXWorld* world;
	RXECSDef::NodeId header;
	RXECSDef::NodeId nodeid;
};

DECL_VARIANT_TYPE(RXNodeCreationInfo*);



#endif // !RXNODECREATIONINFO_H]

﻿#ifndef RXECSDEF_H
#define RXECSDEF_H

#include <metainfo.h>
#include <threadpool.h>
#include <variant.h>
#include <map>
#include <unordered_set>

namespace RXECSDef
{
	//实体创建逻辑
	enum CreationPolicy
	{
		CP_Private,//私有创建，仅在本端创建实体，其它主机不可见
		CP_Public,//公有创建，仅服务端可使用该逻辑，服务端创建后，其它客户端也会跟着创建.
	};

	typedef uint32_t NodeId;
	typedef std::vector<NodeId> NodeIdList;


	struct NodeCreationInfo
	{
		RXECSDef::NodeId id;
		RXECSDef::NodeId parentNodeId;
		bool isRoot;
		MetaInfo meta;

		std::map<int, Variant> init_property;
	};
	typedef std::shared_ptr<NodeCreationInfo> NodeCreationInfoPtr;

	struct NodePropertyChangeEvent {
		RXECSDef::NodeId id;
		int propIndex;
		Variant value;
	};
	typedef std::shared_ptr<NodePropertyChangeEvent> NodePropertyChangeEventPtr;

	typedef std::string RXAspectID;
	typedef std::vector<RXAspectID> RXAspectIDList;

	typedef std::string RXAspectServiceID;
	typedef std::vector<RXAspectServiceID> RXAspectServiceIDList;

	typedef ThreadJob AspectJob;
	typedef std::shared_ptr<AspectJob> AspectJobPtr;
	typedef std::vector<AspectJobPtr> AspectJobList;
};

#define DECL_ASPECT_NAME(x) \
namespace Aspects \
{\
	namespace AspectNames {\
		const static RXECSDef::RXAspectID x = #x; \
	} \
}\

#define DECL_ASPECT_SERVICE(x) \
namespace Aspects \
{ \
	namespace AspectService { \
		const static RXECSDef::RXAspectServiceID x = #x; \
	} \
} \



#endif // !RXECSDEF_H

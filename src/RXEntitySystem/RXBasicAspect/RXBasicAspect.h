#ifndef RX_BASICASPECT_H
#define RX_BASICASPECT_H

#include "RXEntitySystem_global.h"
#include "RXShadowWorld/RXAspect.h"
#include "RXBasicAspect.h"
#include "RXBasicAspectDef.h"
/*
基本切面功能用于向其它切面功能提供每个前端节点的基本信息，树状图等服务

*/

class BasicShadowNode;
class RXBasicAspectService;
class RXEntitySystem_EXPORT RXBasicAspect :public RXAspect
{
public:
	RXBasicAspect();
	~RXBasicAspect();


	RXECSDef::AspectJobList CreateAspectJob(std::chrono::system_clock::duration delta) override;
	
private:
	enum DirtyMark
	{
		None,
		Hierarchy,//树状结构乱了
		Enable,//可用性变了
	};
	void MarkDirty(DirtyMark mark);
	void AspectBegin() override;
	void CalculateEnableTree(RXECSDef::NodeId root,bool parentEnabled);
private:
	ShadowNodeFactoryBase* nodes;
	RXECSDef::NodeId rootId;
	RXBasicAspectService* service;
	
	class RXBasicAspectServiceImp;
	friend class RXBasicAspectServiceImp;
	friend class BasicShadowNode;
	
	DirtyMark m_eDirty;
};




#endif // !RX_BASICASPECT_H

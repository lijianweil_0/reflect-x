#include <RXNode.h>

#include "RXBasicAspect.h"
#include "RXBasicAspectShadowNode.h"

BasicShadowNode::BasicShadowNode(RXBasicAspect* aspect)
{
	this->aspect = aspect;
}

BasicShadowNode::~BasicShadowNode()
{
	aspect->MarkDirty(RXBasicAspect::Hierarchy);
	aspect->MarkDirty(RXBasicAspect::Enable);
}

void BasicShadowNode::InitNode(const RXECSDef::NodeCreationInfo & initInfo)
{
	RXShadowNode::InitNode(initInfo);

	meta = initInfo.meta;
	parent = initInfo.parentNodeId;
	static int NameIndex = RXNode::staticMetaInfo().IndexOfProperty("Name");
	Name = initInfo.init_property.at(NameIndex).Value<std::wstring>();

	static int EnableIndex = RXNode::staticMetaInfo().IndexOfProperty("Enabled");
	isEnabled = initInfo.init_property.at(EnableIndex).Value<bool>();

	if (initInfo.isRoot) {
		aspect->rootId = initInfo.isRoot;
	}
	aspect->MarkDirty(RXBasicAspect::Hierarchy);
	aspect->MarkDirty(RXBasicAspect::Enable);
}

void BasicShadowNode::HandlePropertyChange(const RXECSDef::NodePropertyChangeEvent & change)
{
	static int NameIndex = RXNode::staticMetaInfo().IndexOfProperty("Name");
	static int ParentIndex = RXNode::staticMetaInfo().IndexOfProperty("Parent");
	static int EnableIndex = RXNode::staticMetaInfo().IndexOfProperty("Enabled");
	if (change.propIndex == NameIndex) {
		Name = change.value.Value<std::wstring>();
	}
	else if (change.propIndex == ParentIndex) {
		parent = change.value.Value<RXECSDef::NodeId>();
		aspect->MarkDirty(RXBasicAspect::Hierarchy);
	}
	else if (change.propIndex == EnableIndex) {
		isEnabled = change.value.Value<bool>();
		aspect->MarkDirty(RXBasicAspect::Enable);
	}
}

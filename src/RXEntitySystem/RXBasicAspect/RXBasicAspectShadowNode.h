#ifndef  RXBASIC_ASPECT_SHADOW_NODE_H
#define RXBASIC_ASPECT_SHADOW_NODE_H

#include "RXShadowWorld/RXShadowNode.h"

class RXBasicAspect;

class BasicShadowNode :public RXShadowNode
{
public:
	BasicShadowNode(RXBasicAspect* aspect);
	~BasicShadowNode();

protected:
	void InitNode(const RXECSDef::NodeCreationInfo& initInfo) override;//后端节点初始化

	void HandlePropertyChange(const RXECSDef::NodePropertyChangeEvent& change) override;//处理前端节点属性变化
private:
	MetaInfo meta;//元对象
	std::wstring Name;//名称
	bool isEnabled;//局部可用
	bool isGlobalEnabled;//全局可用
	RXECSDef::NodeId parent;//父节点
	RXBasicAspect* aspect;
	RXECSDef::NodeIdList children;//子节点
	RXECSDef::NodeId rootComp;//根组件
	friend class RXBasicAspect;
};
#endif // ! RXBASIC_ASPECT_SHADOW_NODE_H

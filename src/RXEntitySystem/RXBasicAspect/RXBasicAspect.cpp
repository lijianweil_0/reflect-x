#include "RXEntity.h"
#include "RXNode.h"
#include "RXComponent.h"
#include "RXBasicAspectService.h"
#include "RXBasicAspect.h"
#include "RXBasicAspectDef.h"
#include "RXBasicAspectShadowNode.h"

class BasicAspectShadowNodeFactory :public ShadowNodeFactoryBase
{
public:
	BasicAspectShadowNodeFactory(RXBasicAspect* aspect) {
		this->aspect = aspect;
	}
	BasicShadowNode* CreateNode() {
		return new BasicShadowNode(aspect);
	}

private:
	RXBasicAspect * aspect;
};

class RXBasicAspect::RXBasicAspectServiceImp :public RXBasicAspectService {
public:
	RXBasicAspectServiceImp(RXBasicAspect* aspect) {
	}

	 RXECSDef::NodeId GetRootNode() override{
		 return aspect->rootId;
	 }

	 const MetaInfo* GetNodeMeta(RXECSDef::NodeId id) override{
		 auto node = static_cast<BasicShadowNode*>(aspect->nodes->GetNode(id));

		 if (node) {
			 return &node->meta;
		 }

		 return NULL;
	 }

	 RXECSDef::NodeId GetNodeParent(RXECSDef::NodeId id) override{
		 auto node = static_cast<BasicShadowNode*>(aspect->nodes->GetNode(id));

		 if (node) {
			 return node->parent;
		 }

		 return NULL;
	 }

	 const RXECSDef::NodeIdList* GetNodeChildren(RXECSDef::NodeId id) override{
		 auto node = static_cast<BasicShadowNode*>(aspect->nodes->GetNode(id));

		 if (node) {
			 return &node->children;
		 }

		 return NULL;
	 }

	 const std::wstring* GetNodeName(RXECSDef::NodeId id) override{
		 auto node = static_cast<BasicShadowNode*>(aspect->nodes->GetNode(id));

		 if (node) {
			 return &node->Name;
		 }

		 return NULL;
	 }

	 const bool* IsNodeEnabled(RXECSDef::NodeId id) override{
		 auto node = static_cast<BasicShadowNode*>(aspect->nodes->GetNode(id));

		 if (node) {
			 return &node->isEnabled;
		 }

		 return NULL;
	 }

	 const bool* IsNodeGlobalEnabled(RXECSDef::NodeId id) override {
		 auto node = static_cast<BasicShadowNode*>(aspect->nodes->GetNode(id));

		 if (node) {
			 return &node->isGlobalEnabled;
		 }

		 return NULL;
	 }

	 RXECSDef::NodeId GetRootComponent(RXECSDef::NodeId id) override
	 {
		 auto node = static_cast<BasicShadowNode*>(aspect->nodes->GetNode(id));

		 if (node) {
			 return node->rootComp;
		 }

		 return 0;
	 }

	 bool GlobalEnableChanged() override
	 {
		 return dirtyMark & Enable;
	 }

	 bool HierarchyChanged() override {
		 return dirtyMark & Hierarchy;
	 }

public:
	RXBasicAspect* aspect;
	DirtyMark dirtyMark;

	friend class RXBasicAspect;
};

RXBasicAspect::RXBasicAspect()
{
	SetAspectInfo(RXAspectInfo(Aspects::AspectNames::BasicAspect, {}));
}

RXBasicAspect::~RXBasicAspect()
{
}

RXECSDef::AspectJobList RXBasicAspect::CreateAspectJob(std::chrono::system_clock::duration delta)
{
	static_cast<RXBasicAspectServiceImp*>(service)->dirtyMark = m_eDirty;

	RXECSDef::AspectJobList ret;

	RXECSDef::AspectJobPtr HierarchyJob;

	if (m_eDirty & Hierarchy) {//当节点树状图关系变化时。进行节点关系重构
		RXECSDef::AspectJobList JobRely = {};
		HierarchyJob = RXECSDef::AspectJob::CreateJob([=]() {
			nodes->Foreach([=](RXShadowNode* node) {
				auto n = static_cast<BasicShadowNode*>(node);
				n->children.clear();
				n->rootComp = 0;
				return true;
			});

			nodes->Foreach([=](RXShadowNode* node) {
				auto n = static_cast<BasicShadowNode*>(node);
				auto parentId = n->parent;
				auto parentNode = nodes->GetNode(parentId);
				auto parent = static_cast<BasicShadowNode*>(parentNode);

				static auto entityMeta = RXEntity::staticMetaInfo();
				static auto compMeta = RXComponent::staticMetaInfo();

				if (parent) {//计算实体节点的根组件属性
					parent->children.push_back(node->GetNodeId());

					if (entityMeta.IsBaseOf(parent->meta) && compMeta.IsBaseOf(n->meta))
					{
						parent->rootComp = n->GetNodeId();
					}
				}
					
				return true;
			});
			}, JobRely);
		ret.push_back(HierarchyJob);
	}

	RXECSDef::AspectJobPtr EnableJob;

	if (m_eDirty & Enable) {//当节点树状图关系变化时。进行节点可用性重构
		RXECSDef::AspectJobList JobRely = {};
		if (HierarchyJob) JobRely.push_back(HierarchyJob);
		EnableJob = RXECSDef::AspectJob::CreateJob([=]() {
			CalculateEnableTree(rootId,true);
			}, JobRely);
		ret.push_back(EnableJob);
	}

	m_eDirty = None;
	return ret;
}

void RXBasicAspect::MarkDirty(DirtyMark mark)
{
	m_eDirty = DirtyMark(m_eDirty | mark);
}

void RXBasicAspect::AspectBegin()
{
	this->nodes = new BasicAspectShadowNodeFactory(this);
	RegisterShadowFactory<RXNode>(nodes);

	service = new RXBasicAspectServiceImp(this);
	RegisterAspectService(Aspects::AspectService::BasicAspectService, service);

	RegisterNodeClass<RXNode>();
	RegisterNodeClass<RXEntity>();
	RegisterNodeClass<RXComponent>();
}

void RXBasicAspect::CalculateEnableTree(RXECSDef::NodeId root, bool parentEnabled)
{
	auto node = static_cast<BasicShadowNode*>(nodes->GetNode(root));

	if (!node)
	{
		return;
	}

	node->isGlobalEnabled = parentEnabled & node->isEnabled;

	for (const auto &i : node->children)
	{
		CalculateEnableTree(i, node->isGlobalEnabled);
	}
}

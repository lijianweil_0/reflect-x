#ifndef RX_BASIC_ASPECT_SERVICE_H
#define RX_BASIC_ASPECT_SERVICE_H

#include "RXShadowWorld/RXAspect.h"

class RXBasicAspectService:public RXAspectService
{
public:
	virtual RXECSDef::NodeId GetRootNode() = 0;

	virtual const MetaInfo* GetNodeMeta(RXECSDef::NodeId id) = 0;

	virtual RXECSDef::NodeId GetNodeParent(RXECSDef::NodeId id) = 0;

	virtual RXECSDef::NodeId GetRootComponent(RXECSDef::NodeId id) = 0;//获取某节点的根组件节点（输入节点必须是实体节点)

	virtual const RXECSDef::NodeIdList* GetNodeChildren(RXECSDef::NodeId id) = 0;

	virtual const std::wstring* GetNodeName(RXECSDef::NodeId id) = 0;

	virtual const bool* IsNodeEnabled(RXECSDef::NodeId id) = 0;

	virtual const bool* IsNodeGlobalEnabled(RXECSDef::NodeId id) = 0;

	virtual bool GlobalEnableChanged() = 0;//全局可用性改变了

	virtual bool HierarchyChanged() = 0;//树状关系改变了
};

#endif // !RX_BASIC_ASPECT_SERVICE_H

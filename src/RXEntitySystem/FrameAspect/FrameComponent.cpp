#include "FrameAspect.h"
#include "FrameComponent.h"

BeginMetaData(FrameComponent)

META_BEGIN_CONSTRUCTOR(RXNodeCreationInfo*)
META_END_CONSTRUCTOR

META_BEGIN_SIGNAL(OnFrame)
META_END_SIGNAL

EndMetaData


FrameComponent::FrameComponent(RXNodeCreationInfo* info):SuperType(info)
{
	m_bIsActive = false;
}

FrameComponent::~FrameComponent()
{
}

void FrameComponent::HandleEvent(ThreadSafe::Event * e)
{
	auto frame_event = dynamic_cast<FrameAspect::FrameEvent*>(e);
	if (frame_event)
	{
		OnFrame(frame_event->time_delta);
		return;
	}

	SuperType::HandleEvent(e);
}

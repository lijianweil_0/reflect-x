#ifndef FRAME_ASPECT_TYPES_H
#define FRAME_ASPECT_TYPES_H

#include <memory>
#include <vector>

#include <metatype.h>
#include "RXEntitySystem_global.h"

class CurveFragment;
RXEntitySystem_EXPORT DataInputStream& operator >> (DataInputStream& s, CurveFragment& c);
RXEntitySystem_EXPORT DataOutputStream& operator << (DataOutputStream& s, const CurveFragment& c);

class RXEntitySystem_EXPORT CurveFragment
{
public:
	enum Type:char
	{
		None,
		Straght,//直线
		Curve,//曲线
		QuadCurve//二阶曲线
	};

	bool operator == (const CurveFragment& other) const;
	bool operator != (const CurveFragment& other) const;

	void SetStraght(Point2F begin,Point2F end);
	void SetCurve(Point2F begin,Point2F con,Point2F end);
	void SetQuadCurve(Point2F begin, Point2F con1, Point2F con2, Point2F end);

	Type GetType() const;
	Point2F GetPoint(int index) const;
private:
	friend DataInputStream& operator >> (DataInputStream& s, CurveFragment& c);
	friend DataOutputStream& operator << (DataOutputStream& s, const CurveFragment& c);

	Type type = None;
	static constexpr int point_size = 4;
	Point2F p[point_size];
};

typedef std::vector<CurveFragment> CurveFragmentList;


class CurveFunctionCreator;
class CurveFunction;
RXEntitySystem_EXPORT DataInputStream& operator >> (DataInputStream& s, CurveFunction& c);
RXEntitySystem_EXPORT DataOutputStream& operator << (DataOutputStream& s, const CurveFunction& c);
class RXEntitySystem_EXPORT CurveFunction
{
public:
	float operator()(float x,bool *valid = NULL) const;//根据x值获取y值,valid表示输入值是否在定义域内
	bool operator == (const CurveFunction& other) const;
	bool operator != (const CurveFunction& other) const;
	const CurveFragmentList& GetCurve() const;
private:
	CurveFragmentList curve;
	friend DataInputStream& operator >> (DataInputStream& s, CurveFunction& c);
	friend DataOutputStream& operator << (DataOutputStream& s, const CurveFunction& c);
	friend class CurveFunctionCreator;
};
typedef std::vector<CurveFunction> CurveFunctionList;

//曲线函数的创建器
class CurveFunctionCreator
{
public:
	CurveFunctionCreator& Begin(float x,float y);//设置起始x点并开始进行曲线绘制
	CurveFunctionCreator& LineTo(float dx,float y);//绘制直线,dx为x轴前进的值,只能为正数
	CurveFunctionCreator& CurveTo(float dx, float y,float cx,float cy);//绘制曲线
	CurveFunctionCreator& QuadCurveTo(float dx, float y, float c1x, float c1y,float c2x,float c2y);//绘制二阶曲线

	const CurveFunction& End();//结束绘制并返回曲线函数
private:
	CurveFunction result;
	Point2F current;
};

DECL_VARIANT_TYPE(CurveFragment)
DECL_VARIANT_TYPE(CurveFragmentList)
DECL_VARIANT_TYPE(CurveFunction)
DECL_VARIANT_TYPE(CurveFunctionList)
#endif // !FRAME_ASPECT_TYPES_H

#ifndef FRAME_SHADOW_NODE_H
#define FRAME_SHADOW_NODE_H

#include "RXShadowWorld/RXShadowNode.h"

class FrameShadowNode:public RXShadowNode
{
public:
	FrameShadowNode();
	~FrameShadowNode();

protected:
	void InitNode(const RXECSDef::NodeCreationInfo& initInfo) override;//后端节点初始化

	void HandlePropertyChange(const RXECSDef::NodePropertyChangeEvent& change) override;//处理前端节点属性变化
private:
	bool isActive;
};

#endif // !FRAME_SHADOW_NODE_H

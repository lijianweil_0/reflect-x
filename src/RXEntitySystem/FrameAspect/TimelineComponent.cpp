#include "TimelineComponent.h"

BeginMetaData(TimelineComponent)

META_BEGIN_CONSTRUCTOR(RXNodeCreationInfo*)
META_END_CONSTRUCTOR

META_SIMPE_PROPERTY_BEGIN(Duration)
META_SIMPE_PROPERTY_END
META_SIMPE_PROPERTY_BEGIN(CurrentProcess)
META_SIMPE_PROPERTY_END
META_SIMPE_PROPERTY_BEGIN(Curve)
META_SIMPE_PROPERTY_END
META_SIMPE_PROPERTY_BEGIN(Loop)
META_SIMPE_PROPERTY_END
META_SIMPE_PROPERTY_BEGIN(Reverse)
META_SIMPE_PROPERTY_END

META_BEGIN_FUNCTION(Start)
META_END_FUNCTION

META_BEGIN_FUNCTION(StartFromBegin)
META_END_FUNCTION

META_BEGIN_FUNCTION(Pause)
META_END_FUNCTION

EndMetaData


TimelineComponent::TimelineComponent(RXNodeCreationInfo* info) :SuperType(info)
{
}

TimelineComponent::~TimelineComponent()
{
}

bool TimelineComponent::IsActive() const
{
	return m_bIsActive;
}

void TimelineComponent::Start()
{
	m_bIsActive = true;
}

void TimelineComponent::StartFromBegin()
{
	m_bIsActive = true;

	if (m_Reverse)
	{
		m_CurrentProcess = 1.0;
	}
	else {
		m_CurrentProcess = 0.0;
	}
}

void TimelineComponent::Pause()
{
	m_bIsActive = false;
}

void TimelineComponent::slotOnFrame(double delta)
{
	if (m_bIsActive)
	{
		return;
	}

	if (m_Duration <= 0)
	{
		return;
	}

	IncreseProcess(delta);
	
	std::vector<double> ret;

	for (const auto c : m_Curve)
	{
		float value = c(m_CurrentProcess);
		ret.push_back(value);
	}

	Update(ret);
}

void TimelineComponent::IncreseProcess(double delta)
{
	if (m_Reverse)
	{
		m_CurrentProcess -= delta / m_Duration;//将经过的事件换算成当前进度

		if (m_CurrentProcess < 0)
		{
			if (m_Loop)
			{
				m_CurrentProcess += int(m_CurrentProcess) + 1;
			}
			else {
				m_CurrentProcess = 0;
				m_bIsActive = false;
			}
		}
	}
	else {
		m_CurrentProcess += delta / m_Duration;//将经过的事件换算成当前进度

		if (m_CurrentProcess > 1.0)
		{
			if (m_Loop)
			{
				m_CurrentProcess -= int(m_CurrentProcess);
			}
			else {
				m_CurrentProcess = 1.0;
				m_bIsActive = false;
			}
		}
	}
}

#ifndef FRAME_COMPONENT_H
#define FRAME_COMPONENT_H

#include "RXEntitySystem_global.h"
#include "RXComponent.h"

class RXEntitySystem_EXPORT FrameComponent:public RXComponent
{
	REFLECTABLE(FrameComponent, RXComponent)
public:
	FrameComponent(RXNodeCreationInfo*);
	~FrameComponent();

protected:
	void HandleEvent(ThreadSafe::Event* e) override;

public:
	Signal<double> OnFrame;//帧事件，每帧向外部发出帧信号，参数为与上一帧的事件差，单位为毫秒

private:
	bool m_bIsActive;

};


#endif // !FRAME_NODE_H

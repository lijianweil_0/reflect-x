#ifndef TIMELINE_COMPONENT_H
#define TIMELINE_COMPONENT_H

/*
时间轴组件是一种用于制造数据动画的组件，通过设置时间轴曲线，动画长短，他就会在每帧进行数据更新
*/

#include "RXEntitySystem_global.h"
#include "FrameComponent.h"
#include "FrameAspectTypes.h"

class RXEntitySystem_EXPORT TimelineComponent :public FrameComponent
{
	REFLECTABLE(TimelineComponent, FrameComponent)
public:
	TimelineComponent(RXNodeCreationInfo*);
	~TimelineComponent();


	DECL_SIMPE_PROPERTY(double, Duration);//时间长度,单位为秒 > 0
	DECL_SIMPE_PROPERTY(double, CurrentProcess);//当前所在的时间位置，取值0-1.0
	DECL_SIMPE_PROPERTY(CurveFunctionList, Curve);//时间轴动画组
	DECL_SIMPE_PROPERTY(bool, Loop);//是否循环播放
	DECL_SIMPE_PROPERTY(bool, Reverse);//是否反向播放

	bool IsActive() const;//是否运行中
	void Start();//从当前时间点启动
	void StartFromBegin();//从开始的时间点播放(如果IsReverse为true，则为从结尾的地方逆向播放
	void Pause();//暂停

protected:
	void slotOnFrame(double);
public:
	Signal<std::vector<double>> Update;//更新事件，
private:
	void IncreseProcess(double val);
private:
	bool m_bIsActive;
	
};


#endif // !TIMELINE_COMPONENT_H

#include "FrameComponent.h"
#include "FrameShadowNode.h"

FrameShadowNode::FrameShadowNode()
{
}

FrameShadowNode::~FrameShadowNode()
{
}

void FrameShadowNode::InitNode(const RXECSDef::NodeCreationInfo & initInfo)
{
	RXShadowNode::InitNode(initInfo);
	static int isActiveIndex = FrameComponent::staticMetaInfo().IndexOfProperty("Active");
	this->isActive = initInfo.init_property.at(isActiveIndex).Value<bool>();
}

void FrameShadowNode::HandlePropertyChange(const RXECSDef::NodePropertyChangeEvent & change)
{
	static int isActiveIndex = FrameComponent::staticMetaInfo().IndexOfProperty("Active");
	if (change.id == isActiveIndex) {
		isActive = change.value.Value<bool>();
	}
}

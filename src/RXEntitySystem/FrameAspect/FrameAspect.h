#ifndef  FRAME_ASPECT_H
#define FRAME_ASPECT_H

#include "RXEntitySystem_global.h"
#include "RXShadowWorld/RXAspect.h"
#include "RXECSDef.h"

class RXBasicAspectService;
//帧切面功能，能够向前端发送帧信息
class RXEntitySystem_EXPORT FrameAspect:public RXAspect
{
public:
	FrameAspect();
	~FrameAspect();


	RXECSDef::AspectJobList CreateAspectJob(std::chrono::system_clock::duration delta) override;

protected:
	void AspectBegin() override;

private:
	ShadowNodeFactoryBase* frame_nodes;
	RXBasicAspectService* basic_service;
private:
	class FrameAspcetJob;
	class FrameEvent:public ThreadSafe::Event
	{
	public:
		FrameEvent(double time_delta);
	private:
		double time_delta;
		friend class FrameComponent;
		friend class FrameAspcetJob;
	};
	friend class FrameComponent;
};


#endif // ! FRAME_ASPECT_H

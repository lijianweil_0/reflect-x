﻿#ifndef RXCOMPONENT_H
#define RXCOMPONENT_H

#include "RXNode.h"

class RXEntitySystem_EXPORT RXComponent:public RXNode
{
	REFLECTABLE(RXComponent,RXNode)
public:
	RXComponent(RXNodeCreationInfo*);
};


#endif // !RXCOMPONENT_H

#ifndef RXASSET_H
#define RXASSET_H

#include "RXEntitySystem_global.h"
#include <RXNode.h>

/*
Asset(资产)是用于给一些组件或实体复用的结构(比如材质，网格)，它的结构组成上没有什么限制
*/

class RXEntitySystem_EXPORT RXAsset :public RXNode
{
public:
	REFLECTABLE(RXAsset, RXNode)
	RXAsset(RXNodeCreationInfo*);
	~RXAsset();
};

#endif // ! RXASSET_H

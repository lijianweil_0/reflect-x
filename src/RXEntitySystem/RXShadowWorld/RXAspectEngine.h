﻿#ifndef RXAspectEngine_H
#define RXAspectEngine_H

#include <memory>


#include "RXECSDef.h"
#include "RXEntitySystem_global.h"

class RXAspect;
class RXWorld;
class RXNode;
class RXAspectService;
/*

*/

class RXEntitySystem_EXPORT RXAspectEngine
{
public:
	RXAspectEngine(const std::vector<RXAspect*>& aspects);
	~RXAspectEngine();

	RXWorld* GetWorld();

	void SetMaxFPS(int FPS);//设置切片引擎在一秒内最多计算的次数
	int GetMaxFPS() const;

private:
	void slotOnNewNodeSpawned_Front(RXNode* node,bool Root);
	void slotOnNewNodeSpawned_Shadow(RXECSDef::NodeCreationInfoPtr info);

	void slotOnPropertyChanged_Front(RXECSDef::NodeId node,int propertyIndex,Variant value);
	void slotOnPropertyChanged_Shadow(RXECSDef::NodePropertyChangeEventPtr);

	void slotOnNodeDestoried_Front(RXECSDef::NodeId node);
	void slotOnNodeDestoried_Shadow(RXECSDef::NodeId id);

	void slotOnNodeEvent_Front(RXECSDef::NodeId node, ThreadSafe::Event* event);
	void slotOnNodeEvent_Shadow(RXECSDef::NodeId id, ThreadSafe::Event* event);

private:
	void BeginFrame();
	void PostFrame();
	void HandleFrame();
	void OnFrameFinished();
	void InitAspect();
private:
	void CollectCreationInfo(RXNode* root, RXECSDef::NodeCreationInfoPtr ret,bool isRoot);
	void PostToFrontNode(RXECSDef::NodeId nodeId,ThreadSafe::Event* event);
	RXAspectService* AcquireAspectService(const RXECSDef::RXAspectServiceID& name);
	void RegisterNodeType(MetaInfo meta);
	void BuildCacheData();
private:
	struct Private;
	std::unique_ptr<Private> _P;
	friend class RXAspect;
	class WorldListener;
	friend class WorldListener;

};



#endif // !RXAspectEngine

﻿#ifndef RXSHADOWNODE_H
#define RXSHADOWNODE_H

#include <RXEntitySystem_global.h>

#include <threadsafe.h>
#include "RXECSDef.h"

class RXEntitySystem_EXPORT RXShadowNode
{
public:
	virtual ~RXShadowNode();
	virtual void InitNode(const RXECSDef::NodeCreationInfo& initInfo);//后端节点初始化

	virtual void HandlePropertyChange(const RXECSDef::NodePropertyChangeEvent& change);//处理前端节点属性变化

	virtual void HandleFrontNodeEvent(ThreadSafe::Event* e);//处理由前端节点发来的事件

	RXECSDef::NodeId GetNodeId() const;
private:
	RXECSDef::NodeId NodeId;

};

#endif // !RXSHADOWNODE_H

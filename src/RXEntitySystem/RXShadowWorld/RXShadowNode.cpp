﻿#include "RXShadowNode.h"

RXShadowNode::~RXShadowNode()
{
}

void RXShadowNode::InitNode(const RXECSDef::NodeCreationInfo & initInfo)
{
	this->NodeId = initInfo.id;
}

void RXShadowNode::HandleFrontNodeEvent(ThreadSafe::Event * e)
{
}

RXECSDef::NodeId RXShadowNode::GetNodeId() const
{
	return NodeId;
}

void RXShadowNode::HandlePropertyChange(const RXECSDef::NodePropertyChangeEvent& change)
{
}

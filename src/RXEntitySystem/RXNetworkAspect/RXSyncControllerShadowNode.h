#ifndef RXSYNCCONTROLLERSHADOWNODE_H
#define RXSYNCCONTROLLERSHADOWNODE_H

#include "RXShadowWorld/RXShadowNode.h"
#include "RXSyncControllerComponent.h"

#include <messagechannel.h>

class RXNetworkAspect;

class RXSyncStartEvent;
class RXSyncEndEvent;

class RXSyncControllerShadowNode :public RXShadowNode
{
public:
	RXSyncControllerShadowNode(RXNetworkAspect* aspect);
	~RXSyncControllerShadowNode();

protected:
	void InitNode(const RXECSDef::NodeCreationInfo& initInfo) override;//后端节点初始化

	void HandlePropertyChange(const RXECSDef::NodePropertyChangeEvent& change) override;//处理前端节点属性变化
	void HandleFrontNodeEvent(ThreadSafe::Event* e) override;

private:
	void HandleStartEvent(RXSyncStartEvent* e);
	void HandleEndEvent(RXSyncEndEvent* e);

private:
	RXNetworkAspect* aspect;

	RXSyncControllerComponent::SyncType m_SyncMode;
	RXSyncControllerComponent::State m_State;

	friend class RXNetworkAspect;
};
#endif // ! RXBASIC_ASPECT_SHADOW_NODE_H

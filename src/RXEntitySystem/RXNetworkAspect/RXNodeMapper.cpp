#include "RXNodeMapper.h"

RemoteNodeMapper::RemoteNodeMapper()
{
}

RemoteNodeMapper::~RemoteNodeMapper()
{
}

void RemoteNodeMapper::AddEntry(RXECSDef::NodeId Remote, RXECSDef::NodeId Local, MapType type)
{
	MappDataPtr data = std::make_shared<MappData>();

	data->RemoteId = Remote;
	data->LocalId = Local;
	data->m_type = type;

}

void RemoteNodeMapper::RemoteByRemoteID(RXECSDef::NodeId Remote)
{
	if (RemoteMap.count(Remote))
	{
		auto data = RemoteMap.at(Remote);

		LocalMap.erase(data->LocalId);
		RemoteMap.erase(data->RemoteId);
	}
}

void RemoteNodeMapper::RemoteByLocalID(RXECSDef::NodeId Local)
{
	if (LocalMap.count(Local))
	{
		auto data = LocalMap.at(Local);
		LocalMap.erase(data->LocalId);
		RemoteMap.erase(data->RemoteId);
	}
}

void RemoteNodeMapper::Clear()
{
	LocalMap.clear();
	RemoteMap.clear();
}

RXECSDef::NodeId RemoteNodeMapper::GetLocalFromRemote(RXECSDef::NodeId Remote)
{
	if (RemoteMap.count(Remote))
	{
		return RemoteMap.at(Remote)->LocalId;
	}

	return 0;
}

RXECSDef::NodeId RemoteNodeMapper::GetRemoteFromLocal(RXECSDef::NodeId Local)
{
	if (LocalMap.count(Local))
	{
		return LocalMap.at(Local)->RemoteId;
	}

	return 0;
}

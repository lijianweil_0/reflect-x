#include "RXNetworkAspect.h"
#include "RXSyncControllerShadowNode.h"
#include "RXNetworkEvent_P.h"

RXSyncControllerShadowNode::RXSyncControllerShadowNode(RXNetworkAspect * aspect)
{
	this->aspect = aspect;
}

RXSyncControllerShadowNode::~RXSyncControllerShadowNode()
{

}

void RXSyncControllerShadowNode::InitNode(const RXECSDef::NodeCreationInfo & initInfo)
{
	RXShadowNode::InitNode(initInfo);
}

void RXSyncControllerShadowNode::HandlePropertyChange(const RXECSDef::NodePropertyChangeEvent & change)
{
	RXShadowNode::HandlePropertyChange(change);
}

void RXSyncControllerShadowNode::HandleFrontNodeEvent(ThreadSafe::Event * e)
{
	RXShadowNode::HandleFrontNodeEvent(e);

	if (dynamic_cast<RXSyncStartEvent*>(e))
	{
		HandleStartEvent(dynamic_cast<RXSyncStartEvent*>(e));
	}
	else if (dynamic_cast<RXSyncEndEvent*>(e))
	{
		HandleEndEvent(dynamic_cast<RXSyncEndEvent*>(e));
	}
}

void RXSyncControllerShadowNode::HandleStartEvent(RXSyncStartEvent * e)
{
	aspect->Start(e->syncType, e->ip, e->port);
}

void RXSyncControllerShadowNode::HandleEndEvent(RXSyncEndEvent * e)
{

}

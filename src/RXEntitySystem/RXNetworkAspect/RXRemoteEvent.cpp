#include "RXRemoteEvent.h"

RXRemoteEvent::RXRemoteEvent(const char topic, VariantMap param, EventFlag flag):ThreadSafe::Event(Other)
{
	this->topic = topic;
	this->param = param;
	this->flag = flag;
}

RXRemoteEvent::~RXRemoteEvent()
{
}

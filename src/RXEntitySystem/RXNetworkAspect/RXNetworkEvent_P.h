#ifndef RXNETWORKEVENT_P_H
#define RXNETWORKEVENT_P_H

#include <threadsafe.h>

#include "RXSyncComponent.h"
#include "RXSyncControllerComponent.h"

class RXSyncStartEvent:public ThreadSafe::Event
{
public:
	RXSyncStartEvent();

	RXSyncControllerComponent::SyncType syncType;
	std::string ip;
	int port;
};

class RXSyncEndEvent :public ThreadSafe::Event
{
public:
	RXSyncEndEvent();

};


#endif // !RXNETWORKEVENT_P_H

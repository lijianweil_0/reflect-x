﻿#ifndef RXNETWORKASPECT_H
#define RXNETWORKASPECT_H

#include "RXEntitySystem_global.h"
#include "RXShadowWorld/RXAspect.h"
#include "RXSyncControllerComponent.h"

class RXBasicAspectService;
class RXSyncControllerShadowNode;
class RXSyncShadowNode;
class RXSyncNodeListener;

class RXEntitySystem_EXPORT RXNetworkAspect :public RXAspect
{
public:
	RXNetworkAspect();
	~RXNetworkAspect();

	RXECSDef::AspectJobList CreateAspectJob(std::chrono::system_clock::duration delta) override;

private:
	enum DirtyMark
	{
		None,
		SyncController,
		SynvComp,
	};
	void MarkDirty(DirtyMark mark);
	void AspectBegin() override;
	void Start(RXSyncControllerComponent::SyncType type,std::string ip,int port);
	void Stop();
private:

	struct Private;
	std::unique_ptr<Private> _P;

	friend class RXSyncControllerShadowNode;
	friend class RXSyncShadowNode;
	friend class RXSyncNodeListener;
};

#include "RXNetworkDef.h"

#endif // !RXNETWORKASPECT_H

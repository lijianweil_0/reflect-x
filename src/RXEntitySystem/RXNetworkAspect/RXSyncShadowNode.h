#ifndef RXSYNCSHADOWNODE_H
#define RXSYNCSHADOWNODE_H

#include "RXShadowWorld/RXShadowNode.h"

class RXNetworkAspect;

class RXSyncShadowNode :public RXShadowNode
{
public:
	RXSyncShadowNode(RXNetworkAspect* aspect);
	~RXSyncShadowNode();

protected:
	void InitNode(const RXECSDef::NodeCreationInfo& initInfo) override;//后端节点初始化

	void HandlePropertyChange(const RXECSDef::NodePropertyChangeEvent& change) override;//处理前端节点属性变化
private:
	RXNetworkAspect* aspect;
	friend class RXNetworkAspect;
};
#endif // ! RXBASIC_ASPECT_SHADOW_NODE_H

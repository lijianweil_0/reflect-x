﻿
#include "RXSyncControllerShadowNode.h"
#include "RXSyncShadowNode.h"
#include "RXSyncNodeListener.h"

#include "RXSyncComponent.h"
#include "RXSyncControllerComponent.h"

#include "RXNetworkAspect.h"
#include "RXNetworkDef.h"

#include "RXBasicAspect/RXBasicAspectDef.h"
#include "RXBasicAspect/RXBasicAspectService.h"

#include "RXNodeMapper.h"

template<typename T>
class GenericFactory :public ShadowNodeFactoryBase
{
public:
	GenericFactory(RXNetworkAspect* aspect)
	{
		this->aspect = aspect;
	}

	RXShadowNode* CreateNode() override {
		return new T(aspect);
	}

private:

	RXNetworkAspect* aspect;
};

typedef GenericFactory<RXSyncControllerShadowNode> RXSyncControllerFactory;
typedef GenericFactory<RXSyncShadowNode> RXSyncShadowFactory;
typedef GenericFactory<RXSyncNodeListener> RXSyncNodeListenerFactory;

struct RXNetworkAspect::Private
{
	ShadowNodeFactoryBase* m_SyncController;
	ShadowNodeFactoryBase* m_SynvComp;
	ShadowNodeFactoryBase* m_ListenerNodes;
	RXBasicAspectService* basic_service;
	DirtyMark m_eDirty;

	RemoteNodeMapper m_NodeMapper;
	std::unique_ptr<Channel> m_channel;//与客户端进行交流

};

RXNetworkAspect::RXNetworkAspect():_P(new Private)
{
	SetAspectInfo(RXAspectInfo(Aspects::AspectNames::NetworkAspect, { Aspects::AspectNames::BasicAspect }));
}

RXNetworkAspect::~RXNetworkAspect()
{
}

RXECSDef::AspectJobList RXNetworkAspect::CreateAspectJob(std::chrono::system_clock::duration delta)
{
	if (_P->m_SyncController->GetSize() > 1)
	{
		std::cerr << "警告：一个World下只能存在一个RXSyncControllerComponent" << std::endl;
		assert(false);
	}

	RXECSDef::AspectJobList ret;

	





	return ret;
}

void RXNetworkAspect::MarkDirty(DirtyMark mark)
{
	_P->m_eDirty = DirtyMark(_P->m_eDirty | mark);
}

void RXNetworkAspect::AspectBegin()
{
	_P->m_SyncController = new RXSyncControllerFactory(this);
	RegisterShadowFactory<RXSyncControllerComponent>(_P->m_SyncController);
	_P->m_SynvComp = new RXSyncShadowFactory(this);
	RegisterShadowFactory<RXSyncComponent>(_P->m_SynvComp);
	_P->m_ListenerNodes = new RXSyncNodeListenerFactory(this);
	RegisterShadowFactory<RXNode>(_P->m_ListenerNodes);

	RegisterNodeClass<RXSyncComponent>();
	RegisterNodeClass<RXSyncControllerComponent>();

	_P->basic_service = dynamic_cast<RXBasicAspectService*>(AcquireAspectService(Aspects::AspectService::BasicAspectService));
}

void RXNetworkAspect::Start(RXSyncControllerComponent::SyncType type, std::string ip, int port)
{
	RXSyncControllerShadowNode* node = NULL;
	_P->m_SyncController->Foreach([&](RXShadowNode* n) ->bool {
		node = static_cast<RXSyncControllerShadowNode*>(node);
		return true;
	});

	if (!node)
	{
		return;
	}

	if (node->m_SyncMode == RXSyncControllerComponent::SyncType::ST_Offline)
	{
		_P->m_channel.reset(new Channel());
		if (type == RXSyncControllerComponent::ST_Service)
		{
			_P->m_channel->Bind(port);
		}
		else if (type == RXSyncControllerComponent::ST_CLient)
		{
			_P->m_channel->ConnectTo(ip,port);
		}
		else if (type == RXSyncControllerComponent::ST_ListenService)
		{
			_P->m_channel->Bind(port);
		}

		node->m_SyncMode = type;
	}
}

void RXNetworkAspect::Stop()
{
	RXSyncControllerShadowNode* node = NULL;
	_P->m_SyncController->Foreach([&](RXShadowNode* n) ->bool {
		node = static_cast<RXSyncControllerShadowNode*>(node);
		return true;
	});

	if (!node)
	{
		return;
	}
	_P->m_channel.reset();
	node->m_SyncMode = RXSyncControllerComponent::SyncType::ST_Offline;
	node->m_State = RXSyncControllerComponent::State::S_Offline;
}

#ifndef RXSYNCNODELISTENER_H
#define RXSYNCNODELISTENER_H

#include "RXShadowWorld/RXShadowNode.h"

class RXNetworkAspect;

class RXSyncNodeListener :public RXShadowNode
{
public:
	RXSyncNodeListener(RXNetworkAspect* aspect);
	~RXSyncNodeListener();

protected:
	void InitNode(const RXECSDef::NodeCreationInfo& initInfo) override;//后端节点初始化

	void HandlePropertyChange(const RXECSDef::NodePropertyChangeEvent& change) override;//处理前端节点属性变化
	void HandleFrontNodeEvent(ThreadSafe::Event* e) override;
private:
	RXNetworkAspect* aspect;
	MetaInfo meta;
	std::map<int, Variant> propMap;
	friend class RXNetworkAspect;
};
#endif // ! RXBASIC_ASPECT_SHADOW_NODE_H

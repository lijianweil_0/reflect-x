#ifndef RXSYNCCOMPONENT_H
#define RXSYNCCOMPONENT_H

#include "RXRemoteEvent.h"
#include "RXComponent.h"

//同步组件，用于在服务端和客户端之间进行网络同步
/*
使用方法：
将该组件挂载到实体下即可，
注意事项：
1.组件只为对应的实体和组件服务，其子实体不会服务,若要为子实体提供同步服务，则为子实体安装同步组件。一个实体只能被一个同步组件服务，否则程序崩溃
2.一般的“值”类型属性会在同步时原封不动的拷贝到目标对象，而“引用”类型 如指针 NodeId，以及它们的容器，都会被转换成目标机器上的对应节点的指针
这种转换规则如下:
当对象A和B都是在服务器创建的，则客户端会跟随创建对象A~和B~，并将它们关联起来，当A中有指针指向B，则该指针在客户端会被转换成指向B~的指针
当对象A是在服务器创建的，而B是在客户端和服务器都创建的并且没有同步组件（比如实体和它的资产），此时要求没有同步组件的节点必须具备全局唯一且不重复的命名，则引用才能得到正确转换
3.在客户端创建实体并挂载同步组件是没有意义的，它不会影响服务器端或任何其它客户端。只有服务端创建实体并挂在同步组件，然后客户端因同步而创建相同的实体并挂在同步组件，此时这个同步组件才是有意义的。
这个同步组件可以用于服务端通讯和设置同步选项，一些选项是服务端的，而一些选项是客户端的，而另一些是都可以的。
4.对一个已经挂载了同步组件的实体，若在程序运行中动态创建一个组件，并将该组件挂在到实体下，则客户端对应的实体（从服务端挂在开始）也会创建对应的组件并产生关联
若之后再将该组件挂在到另一个不具备同步组件的实体上，则客户端的该组件也会尝试挂载，然后停止对该组件的属性同步，若前面说的是个具备同步组件的实体，则该组件的同步由新的同步组件接管
5.
*/

class RXEntitySystem_EXPORT RXSyncComponent :public RXComponent
{
public:
	enum PropertySyncFlag:char//属性同步选项
	{
		DisablePropertySync = 0,//禁用属性同步
		EnableEntityPropertySync = 1,//实体属性同步
		EnableComponentPropertySync = 2,//组件属性同步
		AllEnable = EnableEntityPropertySync | EnableComponentPropertySync
	};

	enum Role:char//角色
	{
		Role_None = 0,
		Role_Source,//源
		Role_Simulated,//未被占领的远程对象
		Role_Autonomis,//已被占领的远程对象
	};

public:
	REFLECTABLE(RXSyncComponent, RXComponent);

	RXSyncComponent(RXNodeCreationInfo*);
	~RXSyncComponent();	

	//属性同步选项，RS端均可使用，具体效果取交集
	DECL_SIMPE_PROPERTY(PropertySyncFlag, PropertySync);

	Role GetLocalRole() const;
	Role GetRemoteRole() const;

private:
	Role m_localRole;
	Role m_remoteRole;
};

DECL_VARIANT_TYPE(RXSyncComponent::PropertySyncFlag);

#endif // !RXSYNCCOMPONENT_H

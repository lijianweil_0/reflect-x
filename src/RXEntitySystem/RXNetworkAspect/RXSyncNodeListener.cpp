#include "RXSyncNodeListener.h"

RXSyncNodeListener::RXSyncNodeListener(RXNetworkAspect * aspect)
{
	this->aspect = aspect;
}

RXSyncNodeListener::~RXSyncNodeListener()
{
}

void RXSyncNodeListener::InitNode(const RXECSDef::NodeCreationInfo & initInfo)
{
	RXShadowNode::InitNode(initInfo);

	this->meta = initInfo.meta;

	this->propMap = initInfo.init_property;

}

void RXSyncNodeListener::HandlePropertyChange(const RXECSDef::NodePropertyChangeEvent & change)
{
	RXShadowNode::HandlePropertyChange(change);

	propMap[change.propIndex] = change.value;
}

void RXSyncNodeListener::HandleFrontNodeEvent(ThreadSafe::Event * e)
{
	RXShadowNode::HandleFrontNodeEvent(e);
}

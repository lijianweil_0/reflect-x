﻿#ifndef RXSYNCCONTROLLERCOMPONENT_H
#define RXSYNCCONTROLLERCOMPONENT_H

#include "RXComponent.h"

//同步系统控制组件，用于和整个同步系统做交互。想要进行网络同步，整个World中有且仅能有一个该组件
class RXEntitySystem_EXPORT RXSyncControllerComponent :public RXComponent
{
public:
	enum SyncType:char//同步方式
	{
		ST_Offline = 0,//离线
		ST_Service,//服务器模式
		ST_ListenService,//监听服务器模式
		ST_CLient,//客户端模式
	};

	enum State:char//当前状态
	{
		S_Offline,//服务器（客户端）未连接
		S_Listening,//服务器正在工作
		S_Connecting,//客户端正在连接
		S_Connected,//客户端正在工作
	};

public:
	REFLECTABLE(RXSyncControllerComponent, RXComponent);

	RXSyncControllerComponent(RXNodeCreationInfo*);
	~RXSyncControllerComponent();

	//在指定ip端口上开启服务端
	void StartAsServer(const char* ip,int port);

	//在指定ip端口上开启监听服务
	void StartAsListenServer(const char* ip,int port);

	//以客户端的方式启动
	void StartAsClient(const char* hostIp,int port);

	void EndSync();

	SyncType GetSyncType() const;
	State GetState() const;
private:
	SyncType m_syncType;
	State m_state;
};

DECL_VARIANT_TYPE(RXSyncControllerComponent::SyncType);

#endif // !RXSYNCCONTROLLERCOMPONENT_H

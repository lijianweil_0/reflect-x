#ifndef RXNODEMAPPER_H
#define RXNODEMAPPER_H

#include <RXECSDef.h>

//负责远端与本地节点映射的类,只有客户端模式下该类有意义
class RemoteNodeMapper {
public:
	enum MapType//映射类型
	{
		Default,//默认类型，指服务器实例化一个节点，客户端跟着实例化这个节点，两者id做映射
		Name//按名称类型，指服务器的一个节点指向了一个非共享节点，客户端通过名称找到本地的这个对应的节点进行映射
	};
	struct MappData
	{
		RXECSDef::NodeId RemoteId;
		RXECSDef::NodeId LocalId;
		MapType m_type;


	};

	typedef std::shared_ptr<MappData> MappDataPtr;

	RemoteNodeMapper();
	~RemoteNodeMapper();

	void AddEntry(RXECSDef::NodeId Remote, RXECSDef::NodeId Local, MapType type);
	void RemoteByRemoteID(RXECSDef::NodeId Remote);

	void RemoteByLocalID(RXECSDef::NodeId Local);

	void Clear();

	RXECSDef::NodeId GetLocalFromRemote(RXECSDef::NodeId Remote);

	RXECSDef::NodeId GetRemoteFromLocal(RXECSDef::NodeId Local);

private:
	std::map<RXECSDef::NodeId, MappDataPtr> RemoteMap;
	std::map<RXECSDef::NodeId, MappDataPtr> LocalMap;
};

#endif // !RXNODEMAPPER_H

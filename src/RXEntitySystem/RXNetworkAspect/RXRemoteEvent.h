#ifndef RXREMOTE_EVENT_H
#define RXREMOTE_EVENT_H

#include <threadsafe.h>
#include <variant.h>

#include "RXEntitySystem_global.h"

/*
远程事件用于服务器/客户端通讯使用，在原节点上对后端节点抛出的事件会出现在目标节点的事件处理器中
*/
class RXEntitySystem_EXPORT RXRemoteEvent :public ThreadSafe::Event {
public:
	enum EventFlag
	{
		UnCertain,//无保证事件，在抛出后不保证接收方会接收到该事件
		Certain,//有保证事件，在抛出后保证接收方会接收到事件(但如果接收方因网络问题一直接收不到数据包，则会一直占用资源
	};

	RXRemoteEvent(const char topic,VariantMap param,EventFlag flag = UnCertain);
	~RXRemoteEvent();

private:
	std::string topic;//事件标题
	VariantMap param;//事件参数
	EventFlag flag;

};

#endif // !RXREMOTE_EVENT_H

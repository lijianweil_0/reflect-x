#include "RXSyncComponent.h"

BeginMetaData(RXSyncComponent)

META_BEGIN_CONSTRUCTOR(RXNodeCreationInfo*)
META_END_CONSTRUCTOR

META_SIMPE_PROPERTY_BEGIN(PropertySync)
META_SIMPE_PROPERTY_END

EndMetaData

RXSyncComponent::RXSyncComponent(RXNodeCreationInfo * info):SuperType(info)
{
	SetPropertySync(AllEnable);
}

RXSyncComponent::~RXSyncComponent()
{

}

RXSyncComponent::Role RXSyncComponent::GetLocalRole() const
{
	return m_localRole;
}

RXSyncComponent::Role RXSyncComponent::GetRemoteRole() const
{
	return m_remoteRole;
}

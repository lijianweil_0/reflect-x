#include "RXSyncShadowNode.h"

RXSyncShadowNode::RXSyncShadowNode(RXNetworkAspect * aspect)
{
	this->aspect = aspect;
}

RXSyncShadowNode::~RXSyncShadowNode()
{
}

void RXSyncShadowNode::InitNode(const RXECSDef::NodeCreationInfo & initInfo)
{
}

void RXSyncShadowNode::HandlePropertyChange(const RXECSDef::NodePropertyChangeEvent & change)
{
}

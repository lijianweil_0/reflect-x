﻿#include "RXSyncControllerComponent.h"

BeginMetaData(RXSyncControllerComponent)

META_BEGIN_CONSTRUCTOR(RXNodeCreationInfo*)
META_END_CONSTRUCTOR


EndMetaData

RXSyncControllerComponent::RXSyncControllerComponent(RXNodeCreationInfo * info) :SuperType(info)
{
	m_syncType = ST_Offline;
	m_state = S_Offline;
}

RXSyncControllerComponent::~RXSyncControllerComponent()
{
}

void RXSyncControllerComponent::StartAsServer(const char * ip, int port)
{
}

void RXSyncControllerComponent::StartAsListenServer(const char * ip, int port)
{
}

void RXSyncControllerComponent::StartAsClient(const char * hostIp, int port)
{
}

void RXSyncControllerComponent::EndSync()
{
}

RXSyncControllerComponent::SyncType RXSyncControllerComponent::GetSyncType() const
{
	return SyncType();
}

RXSyncControllerComponent::State RXSyncControllerComponent::GetState() const
{
	return m_state;
}

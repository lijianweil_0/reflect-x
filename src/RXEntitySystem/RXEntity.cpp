﻿#include "RXComponent.h"
#include "RXEntity.h"

BeginMetaData(RXEntity)

META_BEGIN_CONSTRUCTOR(RXNodeCreationInfo*)
META_END_CONSTRUCTOR

META_BEGIN_PROPERTY(RootComponent)
META_ADD_SETTER(SetRootComponent)
META_ADD_GETTER(GetRootComponent)
META_END_PROPERTY


EndMetaData

RXEntity::RXEntity(RXNodeCreationInfo* info):SuperType(info)
{
}

RXEntity::~RXEntity()
{
}

void RXEntity::SetRootComponent(RXComponent * component)
{
	auto currentRoot = FindChild<RXComponent>(std::wstring(), false);
	if (currentRoot) {
		currentRoot->SetParent(NULL);
		currentRoot->DeleteLater();
	}
	
	component->SetParent(this);
}

RXComponent * RXEntity::GetRootComponent() const
{
	return FindChild<RXComponent>(std::wstring(),false);
}

﻿#ifndef RXENTITY_H
#define RXENTITY_H
#include "RXEntitySystem_global.h"


#include <RXNode.h>

class RXComponent;

class RXEntitySystem_EXPORT RXEntity :public RXNode
{
public:
	REFLECTABLE(RXEntity, RXNode)
	RXEntity(RXNodeCreationInfo*);
	~RXEntity();

	//一个实体最多只能有一个根组件，根组件必须是实体的子对象。默认情况下，第一个添加到实体中的组件就是根组件.在三维架构中，根组件的位置会被认为是实体的位置
	void SetRootComponent(RXComponent* component);
	RXComponent* GetRootComponent() const;
};


#endif // !RXENTITY_H

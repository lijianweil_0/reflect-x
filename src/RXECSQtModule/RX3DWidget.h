#ifndef RX3DWIDGET_H
#define RX3DWIDGET_H

#include <QOpenGLWidget>
#include <QOpenGLExtraFunctions>

#include <RX3DRender/RX3DOutputInterface.h>
#include "RXECSQtModule_global.h"

#define OutputBufferCount 2

struct OutputBuffer {
	GLuint m_textureBuffer;
	GLuint m_depthBuffer;
};

class RXECSQtModule_EXPORT RX3DWidget:public QOpenGLWidget,public RX3DOutputInterface,public QOpenGLExtraFunctions
{
	Q_OBJECT
public:
	RX3DWidget(QWidget* parent = NULL);
	~RX3DWidget();

protected:
	void initializeGL() override;
	void paintGL() override;
	void resizeGL(int w, int h) override;
	
private slots:
	void finished();
private:
	void RegisterPainter(RX3DPainterInterface * painter) override;
	void UnRegisterPainter(RX3DPainterInterface * painter) override;
	void Update() override;
	void MakeCurrent() override;
	void DoneCurrent() override;
	int GetWidth() override;
	int GetHeight() override;
	void PostEvent(RX3DPainterInterface* painter, RX3DGLEvent* event) override;
	void HandleEvent();
private:
	struct Private;
	std::unique_ptr<Private> _P;
};

#endif // !RX3DWIDGET_H

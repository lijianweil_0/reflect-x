#include <QMutex>
#include <QOpenGLTexture>
#include <QSet>
#include <QEventLoop>
#include <QOffScreenSurface>
#include <glm/glm.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <QThread>
#include <QOpenglContext>
#include <QFile>
#include <QOpenGLTexture>
#include <QOpenglFrameBufferObject>
#include "RX3DWidget.h"

struct EventPacket
{
	RX3DGLEvent* event;
	RX3DPainterInterface* painter;
};

struct RX3DWidget::Private
{
	QMutex m_mutex;
	QSet<RX3DPainterInterface*> painters;
	QList<EventPacket> eventBuffer;//为了优化事件处理效率，将事件放在一个缓冲内，一次执行
};

RX3DWidget::RX3DWidget(QWidget * parent):QOpenGLWidget(parent),_P(new Private)
{
}

RX3DWidget::~RX3DWidget()
{
	finished();
}

void RX3DWidget::initializeGL()
{
	
}

void RX3DWidget::paintGL()
{
	QMutexLocker locker(&_P->m_mutex);
	for (auto i : _P->painters)
	{
		i->paintGL(this);
	}
}

void RX3DWidget::resizeGL(int w, int h)
{
	QMutexLocker lock(&_P->m_mutex);
	for (auto i : _P->painters)
	{
		i->onResize(this);
	}
}

void RX3DWidget::RegisterPainter(RX3DPainterInterface * painter)
{
	QMutexLocker lock(&_P->m_mutex);
	_P->painters.insert(painter);
	metaObject()->invokeMethod(this, [=]() {
		if (_P->painters.contains(painter))
		{
			painter->glInit();
		}
		},Qt::QueuedConnection);
}

void RX3DWidget::UnRegisterPainter(RX3DPainterInterface * painter)
{
	QMutexLocker lock(&_P->m_mutex);
	_P->painters.remove(painter);
	for (auto it = _P->eventBuffer.begin(); it != _P->eventBuffer.end();)//注销painter的同时释放掉对应的事件
	{
		if (it->painter == painter)
		{
			delete it->painter;
			it = _P->eventBuffer.erase(it);
		}
		else {
			it++;
		}
	}
}

int RX3DWidget::GetWidth()
{
	return width();
}

int RX3DWidget::GetHeight()
{
	return height();
}

void RX3DWidget::PostEvent(RX3DPainterInterface * painter, RX3DGLEvent * event)
{
	QMutexLocker lock(&_P->m_mutex);
	_P->eventBuffer.push_back({event,painter});

	metaObject()->invokeMethod(this, [=]() {
		HandleEvent();
		},Qt::QueuedConnection);
}

void RX3DWidget::HandleEvent()
{
	QMutexLocker lock(&_P->m_mutex);
	if (_P->eventBuffer.isEmpty())
	{
		return;
	}
	makeCurrent();
	for (const auto & i : _P->eventBuffer)
	{
		i.painter->onEvent(this,i.event);
		delete i.event;
	}
	doneCurrent();
}

void RX3DWidget::Update()
{
	update();
}

void RX3DWidget::MakeCurrent()
{
	makeCurrent();
}

void RX3DWidget::DoneCurrent()
{
	doneCurrent();
}

void RX3DWidget::finished()
{
	QMutexLocker locker(&_P->m_mutex);

	MakeCurrent();

	for (auto i : _P->painters) {
		i->onDispose(this);//让painter完成最后的销毁行为
		delete i;
	}

	DoneCurrent();
}
#include "IUByteArray.h"


IUByteArray & operator+=(IUByteArray & o1, const IUByteArray & o2)
{
	o1.insert(o1.begin(), o2.begin(), o2.end());
	return o1;
}

IUByteArray operator+(const IUByteArray & o1, const IUByteArray & o2)
{
	IUByteArray ret;
	ret.insert(ret.begin(), o1.begin(), o1.end());
	ret.insert(ret.begin(), o2.begin(), o2.end());
	return ret;
}

#ifndef IUCOMPRESS_H
#define IUCOMPRESS_H

#include "InteUtils_global.h"

#include "IUTypes.h"

class InteUtils_EXPORT IUCompress
{
public:
	//压缩数据，level代表压缩等级，取值0-9,0代表不压缩，但最快，9代表最小压缩,但最慢
	enum CompressError
	{
		CE_OK,
		CE_LEVEL_ERROR,//Level值输入不正确
		CE_MEM_ERROR//内存不足
	};
	static IUByteArray Compress(const IUByteArray& input,int level = 5, CompressError* err = NULL);
	//解压数据
	enum UnCompressError
	{
		UE_OK,
		UE_DATA_ERROR,//输入数据已损坏
		UE_MEM_ERROR//内存不足
	};
	static IUByteArray Uncompress(const IUByteArray& input, UnCompressError* err = NULL);

};
#endif // !IUCOMPRESS_H

#ifndef IUIMAGE_H
#define IUIMAGE_H

#include <memory>

#include "InteUtils_global.h"
#include "IUTypes.h"
#include "IUSize.h"

struct ImageData;

class InteUtils_EXPORT IUImage
{
public:
	typedef char* Scalar;
	typedef char* Tuple;

	enum ScalarType:char
	{
		ST_None,
		ST_INT8,
		ST_INT16,
		ST_INT32,
		ST_INT64,
		ST_UINT8,
		ST_UINT16,
		ST_UINT32,
		ST_UINT64,
		ST_FLOAT,
		ST_DOUBLE
	};

	enum DefaultType//预设图形类型
	{
		DT_None,
		DT_RGB,
		DT_RGBA
	};

	IUImage();
	IUImage(const IUImage& o);
	IUImage(DefaultType type, IUSize2I size);
	IUImage(ScalarType scalarType, unsigned int tupleSize, IUSize2I size);
	~IUImage();

	bool operator == (const IUImage& o) const;
	IUImage& operator = (const IUImage& o);
	ScalarType GetScalarType() const;
	int GetScalarSize() const;
	uint8_t GetTupleSize() const;
	IUSize2I GetSize() const;
	bool IsNull() const;
	char* GetData();//获取可修改指针，内部数据会进行写时拷贝
	const char* GetData() const;//获取不可修改指针，内部数据保持引用
	int GetDataSize() const;
	Tuple GetTuple(int x, int y);

	enum SaveFormate
	{
		SF_Default,
		SF_Png,
		SF_Bmp,
		SF_Tga,
		SF_Jpg,
		SF_Hdr
	};

	static IUImage FromFile(const char* file);
	static IUImage FromBuffer(const char* buffer,int len);

	static IUByteArray ToBuffer(const IUImage& image, SaveFormate formate);
	static bool ToFile(const IUImage& image,const char* file, SaveFormate formate = SF_Default);

private:
	static IUImage FromBufferIMP(const unsigned char* buffer,int x,int y,int comp);
	static IUByteArray ToBufferIMP(const IUImage& image, SaveFormate formate);
	void Initialize(ScalarType scalarType, unsigned int tupleType, IUSize2I size);
	static int TypeSize(IUImage::ScalarType type);
private:
	std::shared_ptr<ImageData> data;
};


#endif // !IUIMAGE_H

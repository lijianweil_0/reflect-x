#include <iostream>
#include <zlib.h>
#include "IUCompress.h"

IUByteArray IUCompress::Compress(const IUByteArray & input, int level, CompressError* err)
{
	
	int bufferSize = input.size() + 16;//输出缓冲区比输入数据多16字节的缓冲
	do
	{
		IUByteArray ret;
		ret.resize(bufferSize, 0);
		uLongf outputLen = ret.size();
		int zr = compress(ret.data(), &outputLen, input.data(), input.size());
		if (zr == Z_OK)
		{
			ret.resize(outputLen);
			if(err)
			{ 
				*err = CE_OK;
			}
			return ret;
		}
		else if (zr == Z_MEM_ERROR)
		{
			if (err)
			{
				*err = CE_MEM_ERROR;
			}
			std::cerr << "IUCompress Error:Out Of Memory" << std::endl;
			return {};
		}
		else if (zr == Z_BUF_ERROR)
		{
			bufferSize *= 1.2;
			continue;
		}
		else if (zr == Z_STREAM_ERROR)
		{
			if (err)
			{
				*err = CE_LEVEL_ERROR;
			}
			std::cerr << "IUCompress Error:Invalid Level Value" << std::endl;
			return {};
		}
	} while (true);

	return {};
}

IUByteArray IUCompress::Uncompress(const IUByteArray & input, UnCompressError* err)
{
	int bufferSize = input.size() * 4;

	while (true)
	{
		IUByteArray ret;
		ret.resize(bufferSize, 0);
		uLongf outputLen = ret.size();
		int zr = uncompress(ret.data(), &outputLen, input.data(), input.size());
		
		if (zr == Z_OK)
		{
			ret.resize(outputLen);
			if (err)
			{
				*err = UE_OK;
			}
			return ret;
		}
		else if (zr == Z_MEM_ERROR)
		{
			if (err)
			{
				*err = UE_MEM_ERROR;
			}
			std::cerr << "IUUncompress Error:Out Of Memory" << std::endl;
			return {};
		}
		else if (zr == Z_BUF_ERROR)
		{
			bufferSize *= 1.2;
			continue;
		}
		else if (zr == Z_DATA_ERROR)
		{
			if (err)
			{
				*err = UE_DATA_ERROR;
			}
			std::cerr << "IUCompress Error:Input Data Is Broken" << std::endl;
			return {};
		}
	}

	return {};
}

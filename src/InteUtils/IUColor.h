#ifndef IUCOLOR_H
#define IUCOLOR_H
#include <stdint.h>
#include <type_traits>
#include <limits>

template<typename T,typename Enable = void>
class IUColorT;

template<typename T>
class IUColorT<T,typename std::enable_if<(std::is_integral<T>::value && !std::is_signed<T>::value) || std::_Is_floating_point<T>::value,void>::type>
{
public:
	IUColorT(){
		data[0] = 0;
		data[1] = 0;
		data[2] = 0;
		data[3] = 0;
	}
	IUColorT(T r,T g,T b,T a) {
		data[0] = r;
		data[1] = g;
		data[2] = b;
		data[3] = a;
	}
	IUColorT(T data[4]) {
		memcpy(this->data, data, sizeof(data));
	}
	~IUColorT() {

	}
	IUColorT(const IUColorT& o)
	{
		memcpy(data, o.data, sizeof(data));
	}

	bool operator == (const IUColorT& o) const
	{
		return memcmp(data, o.data, sizeof(data)) == 0;
	}

	IUColorT& operator = (const IUColorT& o)
	{
		memcpy(data, o.data, sizeof(data));
		return *this;
	}

	template<typename T>
	typename std::enable_if<std::is_integral<T>::value,IUColorT<T>>::type 
		CastTo()//颜色类型转换
	{
		static constexpr auto max = std::numeric_limits<T>::max();
		
		return IUColorT<T>((double)data[0]/max,(double)data[1] / max, (double)data[2] / max, (double)data[3] / max);
	}

	template<typename T>
	typename std::enable_if<std::is_floating_point<T>::value, IUColorT<T>>::type
		CastTo()//颜色类型转换
	{
		return IUColorT<T>(data[0], data[1], data[2], data[3]);
	}

	T* GetData() {
		return data;
	}

	const T* GetData() const {
		return data;
	}

public:
	T GetR() const
	{
		return data[0];
	}

	void SetR(T v)
	{
		data[0] = v;
	}

	T GetG() const
	{
		return data[1];
	}

	void SetG(T v)
	{
		data[1] = v;
	}

	T GetB() const
	{
		return data[2];
	}

	void SetB(T v)
	{
		data[2] = v;
	}

	T GetA() const
	{
		return data[3];
	}

	void SetA(T v)
	{
		data[3] = v;
	}

private:
	T data[4];
};

typedef IUColorT<uint8_t> IUColorI;
typedef IUColorT<float> IUColorF;

#endif // !IUCOLOR_H

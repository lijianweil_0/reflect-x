#ifndef IUSTRING_H
#define IUSTRING_H

#include <string>
#include <memory>

#include "IUByteArray.h"
#include "InteUtils_global.h"

typedef std::wstring IUString;


class InteUtils_EXPORT IUStringUtils
{
public:
	class StringCodec
	{
	public:
		virtual ~StringCodec() {};

		virtual IUByteArray Encode(const IUString& str,bool *ok = NULL) = 0;
		virtual IUString Decode(const IUByteArray& bta,bool *ok = NULL) = 0;
	};
	typedef std::shared_ptr<StringCodec> StringCodecPtr;

	static StringCodecPtr CreateCodec(const char* codecName);
	

	static IUByteArray Encode(const IUString& str,const char* codecName = "local");
	static IUString Decode(const IUByteArray& bta,const char* codecName = "local");

};

#endif // !IUSTRING_H

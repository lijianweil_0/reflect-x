#include <IUTypes.h>
#include <iostream>
#include <fstream>
#include "IUImage.h"

#define STB_IMAGE_IMPLEMENTATION	// include之前必须定义
#include <stb_image.h>

#define STB_IMAGE_WRITE_IMPLEMENTATION	// include之前必须定义
#include <stb_image_write.h>

struct ImageData
{
	IUImage::ScalarType type;
	uint8_t tupleSize;
	IUSize2I size;
	IUByteArray data;
};

IUImage::IUImage()
{
}

IUImage::IUImage(const IUImage & o) :data(o.data)
{
}

IUImage::IUImage(IUImage::DefaultType type, IUSize2I size)
{
	switch (type)
	{
	case IUImage::DT_None:
		break;
	case IUImage::DT_RGB:
		Initialize(ScalarType::ST_UINT8, 3, size);
		break;
	case IUImage::DT_RGBA:
		Initialize(ScalarType::ST_UINT8, 4, size);
		break;
	default:
		break;
	}
}

IUImage::IUImage(ScalarType scalarType, unsigned int tupleType, IUSize2I size)
{
	Initialize(scalarType, tupleType, size);
}

IUImage::~IUImage()
{
}

bool IUImage::operator==(const IUImage & o) const
{
	if (data == o.data)
	{
		return true;
	}

	if (!data || !o.data)
	{
		return false;
	}

	if (data->size != o.data->size)
	{
		return false;
	}

	if (data->tupleSize != o.data->tupleSize)
	{
		return false;
	}

	if (data->type != o.data->type)
	{
		return false;
	}

	if (data->data != o.data->data)
	{
		return false;
	}

	return true;
}

IUImage & IUImage::operator=(const IUImage & o)
{
	data = o.data;

	return *this;
}

IUImage::ScalarType IUImage::GetScalarType() const
{
	if (!data)
	{
		return ST_None;
	}

	return data->type;
}

int IUImage::GetScalarSize() const
{
	if (!data)
	{
		return 0;
	}
	return TypeSize(data->type);
}

uint8_t IUImage::GetTupleSize() const
{
	if (!data)
	{
		return 0;
	}
	return data->tupleSize;
}

IUSize2I IUImage::GetSize() const
{
	if (!data)
	{
		return IUSize2I();
	}

	return data->size;
}

bool IUImage::IsNull() const
{
	return !data;
}

char* IUImage::GetData()
{
	if (data)
	{
		if (data.use_count() > 1)
		{
			data = std::make_shared<ImageData>(*data);
		}
		return (char*)data->data.data();
	}

	return NULL;
}

const char* IUImage::GetData() const
{
	if (data)
	{
		return (const char*)data->data.data();
	}

	return {};
}

int IUImage::GetDataSize() const
{
	if (data)
	{
		return data->data.size();
	}

	return 0;
}

IUImage::Tuple IUImage::GetTuple(int x, int y)
{
	if (data) {

		if (x >= data->size[0] || x < 0 || y >= data->size[1] || y < 0)
		{
			return NULL;
		}

		int t_index = x + y * data->size[0];

		int v_index = t_index * data->tupleSize * TypeSize(data->type);

		return (IUImage::Tuple)(data->data.data() + v_index);
	}

	return NULL;
}

IUImage IUImage::FromFile(const char * file)
{
	int x, y, n;
	unsigned char *data = stbi_load(file, &x, &y, &n, 0);

	if (!data)
	{
		return {};
	}

	return FromBufferIMP(data, x, y, n);
}

IUImage IUImage::FromBuffer(const char * buffer,int len)
{
	int x, y, n;
	unsigned char *data = stbi_load_from_memory((unsigned char* const)buffer, len, &x, &y, &n, 0);
	if (!data)
	{
		return {};
	}
	return FromBufferIMP(data,x,y,n);
}

IUByteArray IUImage::ToBuffer(const IUImage & image, SaveFormate formate)
{
	return ToBufferIMP(image, formate);
}

bool IUImage::ToFile(const IUImage & image, const char * file, SaveFormate formate)
{
	auto buffer = ToBufferIMP(image, formate);

	if (buffer.empty())
	{
		return false;
	}

	std::fstream f;
	f.open(file, std::ios::out | std::ios::binary | std::ios::trunc);
	if (!f)
	{
		return false;
	}
	
	f.write((const char*)buffer.data(), buffer.size());

	return true;
}

IUImage IUImage::FromBufferIMP(const unsigned char * buffer, int x, int y, int comp)
{
	int totalSize = x * y * comp;

	IUImage image(IUImage::ST_UINT8, comp, IUSize2I(x,y));

	memcpy(image.GetData(),buffer,totalSize);

	stbi_image_free((void*)buffer);

	return image;
}

void WriteHandler(void *context, void *data, int size)
{
	auto ret = static_cast<IUByteArray*>(context);
	
	if (data)
	{
		*ret = { (const unsigned char*)data,(const unsigned char*)data + size };
	}
}

IUByteArray IUImage::ToBufferIMP(const IUImage & image, SaveFormate formate)
{
	IUByteArray ret;
	int len;
	auto size = image.GetSize();

	if (formate == IUImage::SF_Png ||
		formate == IUImage::SF_Bmp ||
		formate == IUImage::SF_Tga ||
		formate == IUImage::SF_Jpg)
	{
		if (image.GetScalarType() != ST_UINT8)
		{
			std::cerr << "无法保存图片，目标格式需要ST_UINT8" << std::endl;
			return {};
		}
	}

	if (formate == IUImage::SF_Hdr)
	{
		if (image.GetScalarType() != ST_FLOAT)
		{
			std::cerr << "无法保存图片，目标格式需要ST_FLOAT" << std::endl;
			return {};
		}
	}
	switch (formate)
	{
	case IUImage::SF_Default:
	case IUImage::SF_Png:
		stbi_write_png_to_func(&WriteHandler, &ret, size[0], size[1], image.GetTupleSize(), image.GetData(), 0);
		break;
	case IUImage::SF_Bmp:
		stbi_write_bmp_to_func(&WriteHandler, &ret, size[0], size[1], image.GetTupleSize(), image.GetData());
		break;
	case IUImage::SF_Tga:
		stbi_write_tga_to_func(&WriteHandler, &ret, size[0], size[1], image.GetTupleSize(), image.GetData());
		break;
	case IUImage::SF_Jpg:
		stbi_write_jpg_to_func(&WriteHandler, &ret, size[0], size[1], image.GetTupleSize(), image.GetData(),0);
		break;
	case IUImage::SF_Hdr:
		stbi_write_hdr_to_func(&WriteHandler, &ret, size[0], size[1], image.GetTupleSize(), (const float*)image.GetData());
		break;
	default:
		break;
	}
	return ret;
}

void IUImage::Initialize(ScalarType scalarType, unsigned int tupleType, IUSize2I size)
{
	if (!data)
	{
		data = std::make_shared<ImageData>();
	}

	data->size = size;
	data->tupleSize = tupleType;
	data->type = scalarType;

	int totalsize = size[0] * size[1] * tupleType * TypeSize(scalarType);
	data->data.resize(totalsize, 0);
}

int IUImage::TypeSize(IUImage::ScalarType type)
{
	switch (type)
	{
	case IUImage::ST_None:
		return 0;
		break;
	case IUImage::ST_INT8:
		return sizeof(int8_t);
		break;
	case IUImage::ST_INT16:
		return sizeof(int16_t);
		break;
	case IUImage::ST_INT32:
		return sizeof(int32_t);
		break;
	case IUImage::ST_INT64:
		return sizeof(int64_t);
		break;
	case IUImage::ST_UINT8:
		return sizeof(uint8_t);
		break;
	case IUImage::ST_UINT16:
		return sizeof(uint16_t);
		break;
	case IUImage::ST_UINT32:
		return sizeof(uint32_t);
		break;
	case IUImage::ST_UINT64:
		return sizeof(uint64_t);
		break;
	case IUImage::ST_FLOAT:
		return sizeof(float);
		break;
	case IUImage::ST_DOUBLE:
		return sizeof(double);
		break;
	default:
		return 0;
		break;
	}

	return 0;
}
#ifndef IUPOINT_H
#define IUPOINT_H

#include "IUVector.h"

template<typename T, int dim>
class IUPoint :public IUVectorBase<T, dim>
{
public:
	IUPoint() = default;
	template<typename ...Arg>
	IUPoint(Arg...arg) :IUVectorBase(arg...) {}

	void SetX(T v)
	{
		static_assert(dim >= 1, "dimension must >= 1");
		(*this)[0] = v;
	}

	T GetX() const
	{
		static_assert(dim >= 1, "dimension must >= 1");
		return (*this)[0];
	}

	void SetY(T v)
	{
		static_assert(dim >= 2, "dimension must >= 2");
		(*this)[1] = v;
	}

	T GetY() const
	{
		static_assert(dim >= 2, "dimension must >= 2");
		return (*this)[1];
	}

	void SetZ(T v)
	{
		static_assert(dim >= 3, "dimension must >= 3");
		(*this)[2] = v;
	}

	T GetZ() const
	{
		static_assert(dim >= 3, "dimension must >= 3");
		return (*this)[2];
	}
};



typedef IUPoint<float,2> IUPoint2F;
typedef IUPoint<int, 2> IUPoint2I;
typedef IUPoint<float, 3> IUPoint3F;
typedef IUPoint<int, 3> IUPoint3I;

#endif // !IUPOINT_H

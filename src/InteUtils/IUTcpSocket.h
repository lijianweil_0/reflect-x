#ifndef IUTcpSocket_H
#define IUTcpSocket_H
#include <memory>
#include "InteUtils_global.h"
#include "IUTypes.h"

class IUSocketEngine;

class IUTcpSocket;

class InteUtils_EXPORT IUTcpServer
{
public:
	IUTcpServer();
	virtual ~IUTcpServer();

	bool Bind(const char* ip,unsigned short port);
protected:
	virtual IUTcpSocket* CreateTcpSocketRaw(uint32_t socketId);
	virtual void OnNewConnection(IUTcpSocket* socketId);
private:
	uint32_t socketId;
	friend class IUSocketEngine;
};

class InteUtils_EXPORT IUTcpSocket
{
protected:
	IUTcpSocket(uint32_t socketId);

public:
	IUTcpSocket();
	virtual ~IUTcpSocket();
	bool connect(const char* ip,uint16_t port,const char* localIp = "0.0.0.0",uint16_t localPort = 0);

	void Write(const char* content,uint32_t length);
protected:
	virtual void OnMessage(const char* content,uint32_t length);
	virtual void OnConnected();
private:
	uint32_t socketId;
	friend class IUSocketEngine;
	friend class IUTcpServer;
};
#endif // !IUTcpSocket_H

#include <uv.h>
#include <thread>
#include <mutex>
#include <unordered_map>

#include "IUTcpSocket.h"

struct SocketServerData
{
	std::string ip;
	int16_t port;
	uv_tcp_t server;
	IUTcpServer* frontEnd;
	bool isValid = true;//代表该套接字是否在析构中
};

typedef std::shared_ptr<SocketServerData> SocketServerDataPtr;

struct SocketData
{
	std::string localip;
	int16_t localport;
	std::string peerip;
	int16_t peerport;

	uv_tcp_t socket;
	IUTcpSocket* frontEnd;
	bool isValid = true;
};
typedef std::shared_ptr<SocketData> SocketDataPtr;

class IUSocketEngine
{
public:
	typedef uint32_t SocketId;
public:
	IUSocketEngine();
	~IUSocketEngine();
	static IUSocketEngine* Instance();
	SocketId CreateSocket(const char* target,int port, const char* localIp, int localport, IUTcpSocket*);
	void DestorySocket(SocketId id);
	void SocketWrite(SocketId id,const char* content,int length);

	SocketId CreateServer(const char* ip,int port, IUTcpServer* socket);
	void DestoryServer(SocketId id);

private:
	SocketId AllocID();
	static void OnNewConnection_CB(uv_stream_t* stream,int status);
	void OnNewConnection(uv_stream_t* stream, int status);

	static void OnConnected_CB(uv_connect_t* req, int status);
	void OnConnected(uv_connect_t* req, int status);

	static void OnAllocMemory_CB(uv_handle_t* handle, size_t suggested_size, uv_buf_t* buf);
	static void OnNewMessage_CB(uv_stream_t* stream, ssize_t nread, const uv_buf_t* buf);
	void OnNewMessage(uv_stream_t* stream, ssize_t nread, const uv_buf_t* buf);

	static void OnServerClosed_CB(uv_handle_t *handle);
	void OnServerClosed(uv_handle_t *handle);

	static void OnSocketClosed_CB(uv_handle_t *handle);
	void OnSocketClosed(uv_handle_t *handle);

private:
	uv_loop_t g_mainloop;
	std::recursive_mutex g_mutex;
	std::unique_ptr<std::thread> loop_thread;

	std::unordered_map<SocketId, SocketServerDataPtr> serverData;
	std::unordered_map<SocketId, SocketDataPtr> socketData;
	std::unordered_map<uv_tcp_t*, SocketId> serverMap;
	std::unordered_map<uv_tcp_t*, SocketId> socketMap;
	SocketId MaxId;
};

IUTcpServer::IUTcpServer():socketId(0)
{
}

IUTcpServer::~IUTcpServer()
{
	if (socketId)
	{
		IUSocketEngine::Instance()->DestoryServer(socketId);
	}
}

bool IUTcpServer::Bind(const char * ip, unsigned short port)
{
	if (!socketId)
	{
		auto id = IUSocketEngine::Instance()->CreateServer(ip, port, this);
		if (id)
		{
			socketId = id;
			
		}
		else {
			return false;
		}
	}

	return true;
}

void IUTcpServer::OnNewConnection(IUTcpSocket * socketId)
{
}

IUTcpSocket * IUTcpServer::CreateTcpSocketRaw(uint32_t socketId)
{
	return new IUTcpSocket(socketId);
}

IUTcpSocket::IUTcpSocket(uint32_t socketId):socketId(socketId)
{

}

IUTcpSocket::IUTcpSocket()
{

}

IUTcpSocket::~IUTcpSocket()
{
	if (socketId)
	{
		IUSocketEngine::Instance()->DestorySocket(socketId);
	}
}

bool IUTcpSocket::connect(const char * ip, uint16_t port, const char* localIp, uint16_t localPort)
{
	auto tempId = IUSocketEngine::Instance()->CreateSocket(ip,port,localIp,localPort,this);

	if (tempId)
	{
		socketId = tempId;
	}
	else {
		return false;
	}

	return true;
}

void IUTcpSocket::Write(const char * content, uint32_t length)
{
	IUSocketEngine::Instance()->SocketWrite(socketId, content, length);
}

void IUTcpSocket::OnMessage(const char * content, uint32_t length)
{
	
}

void IUTcpSocket::OnConnected()
{
}

IUSocketEngine::IUSocketEngine()
{
	MaxId = 1;
	uv_loop_init(&g_mainloop);
	loop_thread.reset(new std::thread([=]() {
		uv_run(&g_mainloop, UV_RUN_DEFAULT);
		uv_loop_close(&g_mainloop);
	}));
}

IUSocketEngine::~IUSocketEngine()
{
	uv_stop(&g_mainloop);
	loop_thread->join();
	loop_thread.reset();
}

IUSocketEngine * IUSocketEngine::Instance()
{
	static IUSocketEngine engine;
	return &engine;
}

IUSocketEngine::SocketId IUSocketEngine::CreateSocket(const char * target, int port,const char* localIp,int localport, IUTcpSocket* socket)
{
	std::lock_guard<std::recursive_mutex> lock(g_mutex);

	auto data = std::make_shared<SocketData>();
	data->frontEnd = socket;
	uv_tcp_init(&g_mainloop, &data->socket);
	struct sockaddr_in client_addr;
	try
	{
		if (port == 0)
		{
			uv_ip4_addr(localIp, localport, &client_addr);
			bool flag = uv_tcp_bind(&data->socket, (const struct sockaddr *) &client_addr, 0);
			if (!flag) {
				throw int(0);
			}
		}
		else {
			bool flag;
			do
			{
				int tempPort = rand() % 30000;
				uv_ip4_addr(localIp, tempPort, &client_addr);
				flag = uv_tcp_bind(&data->socket, (const struct sockaddr *) &client_addr, 0);
			} while (!flag);

		}
	}
	catch (int except)
	{
		uv_close((uv_handle_t*)&data->socket, NULL);
		return 0;
	}

	struct sockaddr_in server_addr;
	uv_ip4_addr(target, port, &server_addr);

	uv_tcp_connect(NULL, &data->socket, (const struct sockaddr*) &server_addr, &IUSocketEngine::OnConnected_CB);
	SocketId id = AllocID();

	data->localip = localIp;
	data->localport = client_addr.sin_port;
	data->peerip = target;
	data->peerport = port;

	socketMap[&data->socket] = id;
	socketData[id] = data;
	return id;
}

void IUSocketEngine::DestorySocket(SocketId id)
{
	std::lock_guard<std::recursive_mutex> lock(g_mutex);
	if (!socketData.count(id)) {
		return;
	}
	
	auto data = socketData[id];
	data->isValid = false;
	uv_close((uv_handle_t*)&data->socket,NULL);
}

void IUSocketEngine::SocketWrite(SocketId id,const char * content, int length)
{
	std::lock_guard<std::recursive_mutex> lock(g_mutex);

	if (!socketData.count(id))
	{
		return;
	}

	auto data = socketData[id];
	uv_buf_t uvBuf = uv_buf_init((char*)content, length);
	uv_write(NULL, (uv_stream_t*)&data->socket,&uvBuf, 1, NULL);
}

IUSocketEngine::SocketId IUSocketEngine::CreateServer(const char * ip, int port, IUTcpServer* socket)
{
	SocketServerDataPtr data = std::make_shared<SocketServerData>();
	uv_tcp_init(&g_mainloop,&data->server);
	struct sockaddr_in addr;
	try
	{
		if (port == 0)
		{
			uv_ip4_addr(ip, port, &addr);
			bool flag = uv_tcp_bind(&data->server, (const struct sockaddr *) &addr, 0);
			if (!flag) {
				throw int(0);
			}
		}
		else {
			bool flag;
			do
			{
				int tempPort = rand() % 30000;
				uv_ip4_addr(ip, tempPort, &addr);
				flag = uv_tcp_bind(&data->server, (const struct sockaddr *) &addr, 0);
			} while (!flag);

		}
	}
	catch (int except)
	{
		uv_close((uv_handle_t*)&data->server, NULL);
		return 0;
	}

	data->frontEnd = socket;
	data->ip = ip;
	data->port = addr.sin_port;

	std::lock_guard<std::recursive_mutex> lock(g_mutex);

	SocketId id = AllocID();
	serverData[id] = data;
	serverMap[&data->server] = id;
	
	uv_listen((uv_stream_t*)&data->server, 128, &IUSocketEngine::OnNewConnection_CB);

	return id;
}

void IUSocketEngine::DestoryServer(SocketId id)
{
	std::lock_guard<std::recursive_mutex> lock(g_mutex);
	if (!serverData.count(id)) {
		return;
	}

	serverData[id]->isValid = false;

	uv_close((uv_handle_t*)&serverData[id]->server, &IUSocketEngine::OnServerClosed_CB);
}

IUSocketEngine::SocketId IUSocketEngine::AllocID()
{
	SocketId ret = MaxId;
	MaxId++;
	return ret;
}

void IUSocketEngine::OnNewConnection_CB(uv_stream_t * stream, int status)
{
	IUSocketEngine::Instance()->OnNewConnection(stream,status);
}

void IUSocketEngine::OnNewConnection(uv_stream_t * stream, int status)
{
	SocketDataPtr data = std::make_shared<SocketData>();
	uv_tcp_init(&g_mainloop, &data->socket);

	{
		std::lock_guard<std::recursive_mutex> lock(g_mutex);

		if (!serverMap.count((uv_tcp_t*)stream))
		{
			return;
		}

		SocketId serverId = serverMap[(uv_tcp_t*)stream];
		auto server_data = serverData[serverId];
		if (!server_data->isValid)
		{
			return;
		}

		data->localip = server_data->ip;
		data->localport = server_data->port;
		if (uv_accept(stream, (uv_stream_t*)&data->socket) != 0) {
			return;
		}

		SocketId id = AllocID();
		data->frontEnd = server_data->frontEnd->CreateTcpSocketRaw(id);
		socketMap[&data->socket] = id;
		socketData[id] = data;
		uv_read_start((uv_stream_t*)&data->socket, &IUSocketEngine::OnAllocMemory_CB, &IUSocketEngine::OnNewMessage_CB);

		server_data->frontEnd->OnNewConnection(data->frontEnd);
	}
}

void IUSocketEngine::OnConnected_CB(uv_connect_t * req, int status)
{
	IUSocketEngine::Instance()->OnConnected(req, status);
}

void IUSocketEngine::OnConnected(uv_connect_t * req, int status)
{
	uv_tcp_t * socket = (uv_tcp_t *)req->handle;

	std::lock_guard<std::recursive_mutex> lock(g_mutex);
	if (socketMap.count(socket))
	{
		auto id = socketMap[socket];
		auto data = socketData[id];
		if (!data->isValid)
		{
			return;
		}

		uv_read_start((uv_stream_t*)&data->socket, &IUSocketEngine::OnAllocMemory_CB, &IUSocketEngine::OnNewMessage_CB);

		data->frontEnd->OnConnected();
	}
}

void IUSocketEngine::OnAllocMemory_CB(uv_handle_t * handle, size_t suggested_size, uv_buf_t * buf)
{
	buf->len = suggested_size;
	buf->base = static_cast<char *>(malloc(suggested_size));
}

void IUSocketEngine::OnNewMessage_CB(uv_stream_t * stream, ssize_t nread, const uv_buf_t * buf)
{
	IUSocketEngine::Instance()->OnNewMessage(stream, nread, buf);
}

void IUSocketEngine::OnNewMessage(uv_stream_t * stream, ssize_t nread, const uv_buf_t * buf)
{
	std::lock_guard<std::recursive_mutex> lock(g_mutex);

	do
	{
		if (socketMap.count((uv_tcp_t*)stream))
		{
			break;
		}
		auto id = socketMap[(uv_tcp_t*)stream];
		auto data = socketData[id];

		if (!data->isValid)
		{
			break;
		}

		data->frontEnd->OnMessage(buf->base, nread);
	} while (false);

	if (buf->base != NULL) {
		free(buf->base);
	}
}

void IUSocketEngine::OnServerClosed_CB(uv_handle_t * handle)
{
	IUSocketEngine::Instance()->OnServerClosed(handle);
}

void IUSocketEngine::OnServerClosed(uv_handle_t * handle)
{
	std::lock_guard<std::recursive_mutex> lock(g_mutex);

	if (serverMap.count((uv_tcp_t*)handle))
	{
		auto id = serverMap[(uv_tcp_t*)handle];
		auto data = serverData[id];

		serverMap.erase((uv_tcp_t*)handle);
		serverData.erase(id);
	}
}

void IUSocketEngine::OnSocketClosed_CB(uv_handle_t * handle)
{
	IUSocketEngine::Instance()->OnSocketClosed(handle);
}

void IUSocketEngine::OnSocketClosed(uv_handle_t * handle)
{
	std::lock_guard<std::recursive_mutex> lock(g_mutex);

	if (socketMap.count((uv_tcp_t*)handle))
	{
		auto id = socketMap[(uv_tcp_t*)handle];
		auto data = socketData[id];

		socketMap.erase((uv_tcp_t*)handle);
		socketData.erase(id);
	}
}

#ifndef IUSIZE_H
#define IUSIZE_H

#include "InteUtils_global.h"
#include "IUVector.h"

template<typename T,int dim>
class IUSize:public IUVectorBase<T,dim>
{
public:
	IUSize() = default;
	template<typename ...Arg>
	IUSize(Arg...arg):IUVectorBase(arg...){}

	void SetWidth(T v)
	{
		static_assert(dim >= 1,"dimension must >= 1");
		(*this)[0] = v;
	}

	T GetWidth() const
	{
		static_assert(dim >= 1, "dimension must >= 1");
		return (*this)[0];
	}

	void SetHeight(T v)
	{
		static_assert(dim >= 2, "dimension must >= 2");
		(*this)[1] = v;
	}

	T GetHeight() const
	{
		static_assert(dim >= 2, "dimension must >= 2");
		return (*this)[1];
	}

	void SetDepth(T v)
	{
		static_assert(dim >= 3, "dimension must >= 3");
		(*this)[2] = v;
	}

	T GetDepth() const
	{
		static_assert(dim >= 3, "dimension must >= 3");
		return (*this)[2];
	}
};


typedef IUSize<float, 2> IUSize2F;
typedef IUSize<unsigned int, 2> IUSize2I;
typedef IUSize<float, 3> IUSize3F;
typedef IUSize<unsigned int, 3> IUSize3I;
#endif // !IUSIZE_H

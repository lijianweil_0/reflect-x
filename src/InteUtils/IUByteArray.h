#ifndef IUByteArray_H
#define IUByteArray_H

#include <vector>

#include "InteUtils_global.h"

typedef std::vector<unsigned char> IUByteArray;

InteUtils_EXPORT IUByteArray & operator+=(IUByteArray & o1, const IUByteArray & o2);
InteUtils_EXPORT IUByteArray operator+(const IUByteArray & o1, const IUByteArray & o2);


#endif // !IUByteArray_H

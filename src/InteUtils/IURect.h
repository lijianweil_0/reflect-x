#ifndef IURECT_H
#define IURECT_H

#include "IUPoint.h"
#include "IUSize.h"

template<typename T,int dim>
struct IURect
{
public:
	IURect() {
	}

	IURect(const IUPoint<T, dim>& point,const IUSize<T,dim>& size) :point(point),size(size) {

	}

	bool operator ==(const IURect& o) const {
		return point == o.point && size == o.size;
	}

	bool operator != (const IURect& o) const {
		return !(*this == o);
	}
	
	const IUPoint<T, dim>& GetPoint() const {
		return point;
	}

	void SetPoint(const IUPoint<T, dim>& point) {
		this->point = point;
	}

	const IUSize<T, dim>& GetSize() const {
		return size;
	}

	void SetSize(const IUSize<T, dim>& size) {
		this->size = size;
	}

private:
	IUPoint<T, dim> point;//�߿�ԭ��
	IUSize<T, dim> size;//�߿��С
};

typedef IURect<float,2> IURect2F;
typedef IURect<int,2> IURect2I;
typedef IURect<float, 3> IURect3F;
typedef IURect<int, 3> IURect3I;

#endif // !IURECT_H

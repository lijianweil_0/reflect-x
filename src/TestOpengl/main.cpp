#include <GL/glew.h>
#include <GLFW/glfw3.h>
#include "OpenGLTestQt.h"
#include <QtWidgets/QApplication>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    OpenGLTestQt w;
    w.show();
    return a.exec();
}

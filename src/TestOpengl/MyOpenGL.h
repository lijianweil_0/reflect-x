#pragma once

#include <QOpenGLWidget>
#include <QOpenGLTexture>
#include <glm/glm.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include "ui_MyOpenGL.h"

class MyOpenGL : public QOpenGLWidget
{
	Q_OBJECT

public:
	MyOpenGL(QWidget *parent = nullptr);
	~MyOpenGL();

	void paintGL() override;
	void resizeGL(int w, int h) override;
	void initializeGL() override;
	void setUpVertices();
private:
	Ui::MyOpenGLClass ui;
	GLuint vao[1];
	GLuint vbo[2];
	float camera[3];
	float cubeLoc[3];
	GLuint renderingProgram;
	GLuint mvLoc;
	GLuint projLoc;
	float aspect;
	glm::mat4 pMat;
	glm::mat4 vMat;
	glm::mat4 mMat;
	glm::mat4 mvMat;
	GLuint vfProgram;
	QOpenGLTexture* texture;
};

#ifndef UTILS_H
#define UTILS_H
#include <vector>
#include <GL/glew.h>

namespace UTILS
{
	//typedef float Point[3];
	//typedef std::vector<Point> PointList;
	//typedef int Face[3];//������Ƭ
	//typedef std::vector<Face> FaceList;
	//struct MeshData
	//{
	//	PointList points;
	//	FaceList faces;
	//};
	//MeshData ReadObjFile(const char * file);
	std::string ReadFileContent(const char* file);
	GLuint CreateShaderProgram(const char* vShader,const char* fShader);
	std::vector<GLenum> CheckError();
	bool hasError();

}

#endif // !UTILS_H


#pragma once

#include <QtWidgets/QMainWindow>
#include "ui_OpenGLTestQt.h"

class OpenGLTestQt : public QMainWindow
{
    Q_OBJECT

public:
    OpenGLTestQt(QWidget *parent = nullptr);
    ~OpenGLTestQt();

private:
    Ui::OpenGLTestQtClass ui;
};

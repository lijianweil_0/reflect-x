#include <fstream>
#include <iostream>
#include "Utils.h"
#include <QFile>



//UTILS::MeshData UTILS::ReadObjFile(const char * file)
//{
//	
//	std::fstream vfile;
//	vfile.open(file, std::ios::in);
//
//	if (!vfile)
//	{
//		return {};
//	}
//	MeshData ret;
//	char lineBuffer[1024];
//	while (!vfile.eof())
//	{
//		vfile.getline(lineBuffer, sizeof(lineBuffer));
//
//		if (lineBuffer[0] == 'v')
//		{
//			Point point;
//			sscanf(lineBuffer, "v %f %f %f", &point[0], &point[1], &point[2]);
//			ret.points.push_back(point);
//		}
//		else if (lineBuffer[0] == 'f')
//		{
//			Face face;
//		}
//	}
//
//	return MeshData();
//}

std::string UTILS::ReadFileContent(const char* file)
{
	QFile f(file);
	bool r = f.open(QIODevice::ReadOnly);
	if (r)
	{
		return { f.readAll().data() };
	}
	return {};
}

GLuint UTILS::CreateShaderProgram(const char * vShader, const char * fShader)
{
	std::string && vShaderContent = ReadFileContent(vShader);
	const char* vShaderContentPtr = vShaderContent.c_str();
	std::string && fShaderContent = ReadFileContent(fShader);
	const char* fShaderContentPtr = fShaderContent.c_str();
	GLuint vShaderP = glCreateShader(GL_VERTEX_SHADER);
	GLuint fShaderP = glCreateShader(GL_FRAGMENT_SHADER);
	
	glShaderSource(vShaderP, 1, &vShaderContentPtr, NULL);
	
	glShaderSource(fShaderP, 1, &fShaderContentPtr, NULL);
	
	glCompileShader(vShaderP);
	CheckError();
	glCompileShader(fShaderP);
	CheckError();

	GLuint vfProgram = glCreateProgram();
	glAttachShader(vfProgram, vShaderP);
	glAttachShader(vfProgram, fShaderP);

	glLinkProgram(vfProgram);
	CheckError();
	return vfProgram;
}

std::vector<GLenum> UTILS::CheckError()
{
	std::vector<GLenum> ret;
	while (true)
	{
		auto err = glGetError();

		if (err == GL_NO_ERROR)
		{
			break;
		}
		else {
			std::cerr << "GL_ERROR:" <<err << std::endl;
			ret.push_back(err);
		}

	}
	return ret;
}

bool UTILS::hasError()
{
	return !CheckError().empty();
}

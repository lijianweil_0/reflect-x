﻿#include "PluginInstance.h"
#include "TestClass.h"

BeginMetaData(TestClass)

META_DefaultConstructor

META_BEGIN_FUNCTION(Print)
META_END_FUNCTION


EndMetaData

RX_PLUGIN_EXPORT_CLASS(TestClass)

TestClass::TestClass()
{
	staticMetaInfo().ClassName();
}

void TestClass::Print()
{
	std::cout << "plugin" << std::endl;
}

﻿#ifndef  TEST_CLASS_H
#define TEST_CLASS_H

#include <ReflectableObject.h>

class TestClass :public ReflectableObject
{
public:
	REFLECTABLE(TestClass, ReflectableObject)
	TestClass();

	void Print();

};

#endif // ! TEST_CLASS_H

﻿#include "rotest.h"

BeginMetaData(ROTest)

META_DefaultConstructor


META_BEGIN_CONSTRUCTOR(int, double)
META_END_CONSTRUCTOR;

//注册远程属性
RO_BEGIN_META_PROPERTY(Number)
RO_END_META_PROPERTY

//注册远程属性
RO_BEGIN_META_PROPERTY(Name)
META_MEMBER_INFO(DisableSerilize)//添加不可序列化属性
RO_END_META_PROPERTY

META_BEGIN_PROPERTY(ID)
META_ADD_GETTER(GetID)
META_ADD_SETTER(SetID)
META_END_PROPERTY

//注册远程函数
RO_BEGIN_META_FUNCTION(Print)
RO_END_META_FUNCTION

//注册信号
RO_BEGIN_META_SIGNAL(Clicked)
RO_END_META_SIGNAL

META_BEGIN_FUNCTION(PR)
META_END_FUNCTION

META_BEGIN_FUNCTION(Say)
META_END_FUNCTION

EndMetaData

ROTest::ROTest()
{
    Number = 0;
}


int ROTest::GetNumberImp() const{
    return Number;
}

void ROTest::SetNumberImp(int value){
    if(value != Number){
        Number = value;
        NumberChanged(Number);
    }
}

int ROTest::PrintImp(Variant number){
	int v = number.Value<int>();
    std::cout<<"Print"<<number.TypeName()<<v<<std::endl;
    return v + 1;
}

ROTest::ROTest(int i, double j)
{
	std::cout << 123;
}

double ROTest::m_id = 0;
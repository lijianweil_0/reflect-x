﻿#ifndef  CONSOLE_IMAGE_H
#define CONSOLE_IMAGE_H

#include <memory>

class ConsoleImage {
	struct Data {
		Data(int m_width,int m_height,char value);
		~Data();
		char* m_data;
		int m_width;
		int m_height;
	};
	typedef std::unique_ptr<Data> DataPtr;
public:
	ConsoleImage();
	ConsoleImage(int width, int height, char value = ' ');
	~ConsoleImage();

	ConsoleImage(const ConsoleImage& other);
	ConsoleImage(ConsoleImage&& other);
	ConsoleImage& operator = (const ConsoleImage& other);
	ConsoleImage& operator = (ConsoleImage&& other);

	bool operator == (const ConsoleImage& other) const;

	bool isValidate() const;

	int width() const;
	int height() const;

	void set(int width, int height, char value);
	void set(int width, int height, const char* value);

	const char* data() const;

	int dataLen() const;

private:
	int pos(int x, int y) {
		if (x < _data->m_width && x >= 0 && y < _data->m_height && y >= 0)
		{
			return y * _data->m_width + x;
		}

		return -1;
	}

private:
	DataPtr _data;
};

#endif // ! CONSOLE_IMAGE_H

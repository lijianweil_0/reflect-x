﻿#ifndef GAMEINSTANCE_H
#define GAMEINSTANCE_H

#include <actor.h>

class GameInstance:public Actor
{
public:
	REFLECTABLE(GameInstance, Actor);
	GameInstance();
public:
	//Functions
	RO_DECL_FUNCTION_0_R(Login, PlayerId);
	RO_DECL_FUNCTION_1(Logout, PlayerId,id);
	

public:
	//Signals
	RO_DECL_SIGNAL(PlayerAdded,PlayerId);
	RO_DECL_SIGNAL(PlayerLeaved, PlayerId);

private:
	PlayerId AllocPlayerId();

private:
	std::unordered_set<PlayerId> players;
	PlayerId maxPlayer;
};

#endif // !GAMEINSTANCE_H

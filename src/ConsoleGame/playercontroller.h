﻿#ifndef PLAYER_CONTROLLER_H
#define PLAYER_CONTROLLER_H

#include <actor.h>


class PlayerContorller:public Actor
{
public:
	REFLECTABLE(PlayerContorller, Actor);

public:
	RO_DECL_PROPERTY_SIMPLE(PlayerId,PlayerId);

private:
	PlayerId id;
};
#endif // !PLAYER_CONTROLLER_H

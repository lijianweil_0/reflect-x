﻿#include "gameinstance.h"

BeginMetaData(GameInstance)
META_DefaultConstructor

RO_BEGIN_META_FUNCTION(Login)
RO_END_META_FUNCTION

RO_BEGIN_META_FUNCTION(Logout)
RO_END_META_FUNCTION

RO_BEGIN_META_SIGNAL(PlayerAdded)
RO_END_META_SIGNAL

RO_BEGIN_META_SIGNAL(PlayerLeaved)
RO_END_META_SIGNAL

EndMetaData


PlayerId GameInstance::LoginImp() {
	PlayerId id = AllocPlayerId();

	players.insert(id);

	PlayerAdded(id);

	return id;
}

void GameInstance::LogoutImp(PlayerId player) {
	
	players.erase(player);
	PlayerLeaved(player);
}

GameInstance::GameInstance()
{
	maxPlayer = 1;
}

PlayerId GameInstance::AllocPlayerId()
{
	auto ret = maxPlayer;
	maxPlayer++;
	return ret;
}

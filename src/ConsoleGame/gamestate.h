﻿#ifndef GAMESTATE_H
#define GAMESTATE_H

#include <actor.h>

class GameState :public Actor
{
public:
	REFLECTABLE(GameState, Actor);
};
#endif // !GAMESTATE_H

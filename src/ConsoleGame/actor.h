﻿#ifndef ACTOR_H
#define ACTOR_H

#include <chrono>

#include <remoteobject.h>

#include "gamedefine.h"

class ConsoleGameBase;

class Actor :public RemoteObject{
public:
	REFLECTABLE(Actor, RemoteObject);

public:
	//Properties
	RO_DECL_PROPERTY_SIMPLE(std::string, Name);

public:
	ConsoleGameBase* GetGame();

	//启动和关闭帧事件
	void EnableFrameEvent();
	void DisableFrameEvent();

protected:
	virtual void BeginPlay();
	virtual void Frame(std::chrono::system_clock::duration delta);
private:
	ConsoleGameBase* m_Game;
	friend class ConsoleGameBase;
};

#endif
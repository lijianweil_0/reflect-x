﻿#include "consolegamebase.h"
#include "actor.h"

BeginMetaData(Actor)
META_DefaultConstructor

RO_BEGIN_META_PROPERTY(Name)
RO_END_META_PROPERTY

EndMetaData

ConsoleGameBase * Actor::GetGame()
{
	return nullptr;
}

void Actor::EnableFrameEvent()
{
	GetGame()->EnableFrameEvent(this);
}

void Actor::DisableFrameEvent()
{
	GetGame()->DisableFrameEvent(this);
}

void Actor::BeginPlay()
{
}

void Actor::Frame(std::chrono::system_clock::duration delta)
{
}

#include "RXSceneComponent.h"
#include "RX3DCoreAspect.h"
#include "RXSceneShadowNode.h"

RXSceneShadowNode::RXSceneShadowNode(RX3DCoreAspect * aspect):
	m_rotation(1,0,0,0),
	m_scale(1,1,1),
	m_pos(0,0,0)
{
	this->aspect = aspect;
	

}

RXSceneShadowNode::~RXSceneShadowNode()
{
}



void RXSceneShadowNode::InitNode(const RXECSDef::NodeCreationInfo & initInfo)
{
	RXShadowNode::InitNode(initInfo);
	const static int propPos = RXSceneComponent::staticMetaInfo().IndexOfProperty("Position");
	const static int propRotation = RXSceneComponent::staticMetaInfo().IndexOfProperty("Rotation");
	const static int propScale = RXSceneComponent::staticMetaInfo().IndexOfProperty("Scale");

	m_pos = initInfo.init_property.at(propPos).Value<Vector3D>();
	m_rotation = initInfo.init_property.at(propRotation).Value<QuaternionD>();
	m_scale = initInfo.init_property.at(propScale).Value<Vector3D>();
	UpdateToMatrix();
	MarkWorldMatrixDirty();
}

void RXSceneShadowNode::HandlePropertyChange(const RXECSDef::NodePropertyChangeEvent & change)
{
	const static int propPos = RXSceneComponent::staticMetaInfo().IndexOfProperty("Position");
	const static int propRotation = RXSceneComponent::staticMetaInfo().IndexOfProperty("Rotation");
	const static int propScale = RXSceneComponent::staticMetaInfo().IndexOfProperty("Scale");

	bool update = false;

	if (change.propIndex == propPos)
	{
		m_pos = change.value.Value<Vector3D>();
		update = true;
	}
	else if (change.propIndex == propRotation)
	{
		m_rotation = change.value.Value<QuaternionD>();
		update = true;
	}
	else if (change.propIndex == propScale)
	{
		m_scale = change.value.Value<Vector3D>();
		update = true;
	}

	if (update) {
		UpdateToMatrix();
		MarkWorldMatrixDirty();
	}
	RXShadowNode::HandlePropertyChange(change);
}

void RXSceneShadowNode::UpdateToMatrix()
{
	m_matrix = Compose(m_pos, m_scale, m_rotation);
}

void RXSceneShadowNode::MarkWorldMatrixDirty()
{
	aspect->MarkDirty(RX3DCoreAspect::WorldMatrix);
}

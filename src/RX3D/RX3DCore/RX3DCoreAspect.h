#ifndef RX3DCOREASPECT_H
#define RX3DCOREASPECT_H

#include <RXShadowWorld/RXAspect.h>

#include "RX3DCoreTypes.h"
#include "RX3D_global.h"

class RXSceneShadowNode;
class RXBasicAspectService;
class RX3DCoreService;

class RX3D_EXPORT RX3DCoreAspect:public RXAspect
{
public:
	RX3DCoreAspect();
	~RX3DCoreAspect();


	RXECSDef::AspectJobList CreateAspectJob(std::chrono::system_clock::duration delta) override;

private:
	void CalculateWorldMatrix(RXECSDef::NodeId root,const Matrix4D& mat);

private:
	enum DirtyMark
	{
		None,
		WorldMatrix,//世界坐标系乱了
	};
	void MarkDirty(DirtyMark mark);
	void AspectBegin() override;

private:
	class RX3DServiceImp;
	RX3DCoreService* service;
	RXBasicAspectService* baseService;
	ShadowNodeFactoryBase* sceneNodes;
	DirtyMark m_eDirty;
	friend class RXSceneShadowNode;
	friend class RX3DServiceImp;
};


#endif // !RX3DCOREASPECT_H

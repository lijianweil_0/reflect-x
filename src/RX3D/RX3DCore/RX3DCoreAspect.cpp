#include <RXBasicAspect/RXBasicAspectService.h>
#include <RXBasicAspect/RXBasicAspect.h>
#include "RX3DCoreService.h"
#include "RXSceneShadowNode.h"
#include "RX3DCoreAspect.h"
#include "RXSceneComponent.h"
#include "RX3DCoreTypes.h"
#include "RXEntity.h"
#include "RX3DCoreDef.h"

class RX3DNodeFactory :public ShadowNodeFactoryBase
{
public:
	RX3DNodeFactory(RX3DCoreAspect* aspect) {
		this->aspect = aspect;
	}
	RXShadowNode* CreateNode() override {
		return new RXSceneShadowNode(aspect);
	}

private:
	RX3DCoreAspect* aspect;

};


class RX3DCoreAspect::RX3DServiceImp :public RX3DCoreService
{
public:
	RX3DServiceImp(RX3DCoreAspect* aspect) {
		this->aspect = aspect;
		isWorldMatrixChanged = false;
	}

	PositionInfo GetNodePositionInfo(RXECSDef::NodeId id) override {
		PositionInfo info{};

		auto node = static_cast<RXSceneShadowNode*>(this->aspect->sceneNodes->GetNode(id));

		if (!node) {
			return info;
		}

		info.matrix = &node->m_matrix;
		info.pos = &node->m_pos;
		info.rotation = &node->m_rotation;
		info.scale = &node->m_scale;

		return info;
	}
	const Matrix4D* GetWorldPosition(RXECSDef::NodeId id) override{
		auto node = static_cast<RXSceneShadowNode*>(this->aspect->sceneNodes->GetNode(id));

		if (!node) {
			return NULL;
		}

		return &node->m_world_matrix;
	}

	bool IsWorldMatrixChanged() override
	{
		return isWorldMatrixChanged;
	}

private:
	RX3DCoreAspect* aspect;
	bool isWorldMatrixChanged;
	friend class RX3DCoreAspect;
};

RX3DCoreAspect::RX3DCoreAspect()
{
	SetAspectInfo(RXAspectInfo{ Aspects::AspectNames::RX3DCoreAspect,{Aspects::AspectNames::BasicAspect} });
}

RX3DCoreAspect::~RX3DCoreAspect()
{
}

RXECSDef::AspectJobList RX3DCoreAspect::CreateAspectJob(std::chrono::system_clock::duration delta)
{
	
	RXECSDef::AspectJobList jobs;

	RXECSDef::AspectJobPtr worldMartixJob;

	bool worldMatrixDirty = (m_eDirty | WorldMatrix) || baseService->HierarchyChanged();
	static_cast<RX3DServiceImp*>(service)->isWorldMatrixChanged = worldMatrixDirty;
	if (worldMatrixDirty) {
		RXECSDef::AspectJob::CreateJob([=]() {
			Matrix4D m;
			m.setIdentity();
			CalculateWorldMatrix(baseService->GetRootNode(), m);
		}, {});
		jobs.push_back(worldMartixJob);
	}

	m_eDirty = None;
	return jobs;
}

void RX3DCoreAspect::CalculateWorldMatrix(RXECSDef::NodeId root, const Matrix4D& mat)
{
	auto meta = baseService->GetNodeMeta(root);
	if (!meta) {
		return;
	}

	static auto sceneMeta = RXSceneComponent::staticMetaInfo();
	static auto entityMeta = RXEntity::staticMetaInfo();
	Matrix4D current;

	if (entityMeta.IsBaseOf(*meta))//该节点是实体，全局坐标由根节点决定
	{
		auto rootComp = baseService->GetRootComponent(root);//获取该节点的根组件
		RXSceneShadowNode* node = static_cast<RXSceneShadowNode*>(sceneNodes->GetNode(rootComp));

		if (node)
		{
			current = mat * node->m_matrix;//当前节点的全局坐标等于根组件的全局坐标
		}
		else {
			current = mat;
		}
	}
	else if (sceneMeta.IsBaseOf(*meta))
	{
		RXSceneShadowNode* node = static_cast<RXSceneShadowNode*>(sceneNodes->GetNode(root));
		current = mat * node->m_matrix;
		node->m_world_matrix = current;
	}
	else {
		current = mat;
	}

	auto children = baseService->GetNodeChildren(root);

	if (children) {
		for (const auto &i : *children) {
			CalculateWorldMatrix(i, current);
		}
	}
}

void RX3DCoreAspect::MarkDirty(DirtyMark mark)
{
	m_eDirty = DirtyMark(m_eDirty | mark);
}

void RX3DCoreAspect::AspectBegin()
{
	baseService = dynamic_cast<RXBasicAspectService*>(AcquireAspectService(Aspects::AspectService::BasicAspectService));

	service = new RX3DServiceImp(this);
	RegisterAspectService(Aspects::AspectService::RX3DCoreService,service);

	sceneNodes = new RX3DNodeFactory(this);
	RegisterShadowFactory<RXSceneComponent>(sceneNodes);


	RegisterNodeClass<RXSceneComponent>();
}

#include <initializefunction.h>
#include "RX3DCoreTypes.h"

bool operator == (const QuaternionD& o1,const QuaternionD& o2) {
	return o1.x() == o2.x() && o1.y() == o2.y() && o1.z() == o2.z() && o1.w() == o2.w();
}

DataOutputStream& operator << (DataOutputStream& s, const Matrix4D& mat) {

	for (int i = 0; i < 16; i++) {
		s << mat.data()[i];
	}

	return s;
}

DataInputStream& operator >> (DataInputStream& s, Matrix4D& mat) throw (StreamException){
	double data[16];
	for (int i = 0; i < 16; i++) {
		s >> data[i];
	}

	mat = Matrix4D(data);

	return s;
}

DataOutputStream& operator << (DataOutputStream& s, const Vector3D& mat) {

	for (int i = 0; i < 3; i++) {
		s << (double)mat.data()[i];
	}

	return s;
}

DataInputStream& operator >> (DataInputStream& s, Vector3D& mat) throw (StreamException) {

	double data[3];
	for (int i = 0; i < 3; i++) {
		s >> data[i];
	}

	mat = Vector3D(data);

	return s;
}

DataOutputStream& operator << (DataOutputStream& s, const Vector4D& mat) {

	for (int i = 0; i < 4; i++) {
		s << mat.data()[i];
	}

	return s;
}

DataInputStream& operator >> (DataInputStream& s, Vector4D& mat) throw (StreamException) {

	double data[3];
	for (int i = 0; i < 4; i++) {
		s >> data[i];
	}

	mat = Vector4D(data);

	return s;
}

DataOutputStream& operator << (DataOutputStream& s, const QuaternionD& q) {

	s << q.x();
	s << q.y();
	s << q.z();
	s << q.w();

	return s;
}

DataInputStream& operator >> (DataInputStream& s, QuaternionD& q) throw (StreamException) {

	s >> q.x();
	s >> q.y();
	s >> q.z();
	s >> q.w();

	return s;
}

static void RX3DCoreRegisterTypes() {
	
	MetaType::IdFromType<Matrix4D>();
	MetaType::IdFromType<Vector3D>();
	MetaType::IdFromType<Vector4D>();
	MetaType::IdFromType<QuaternionD>();
	MetaType::IdFromType<RX3DShapeValue>();


	MetaType::RegisterStreamOperator<Matrix4D>();
	MetaType::RegisterStreamOperator<Vector3D>();
	MetaType::RegisterStreamOperator<Vector4D>();
	MetaType::RegisterStreamOperator<QuaternionD>();
	MetaType::RegisterStreamOperator<RX3DShapeValue>();
}

INITIALIZE_FUN(RX3DCoreRegisterTypes);

bool operator!=(const QuaternionD & o1, const QuaternionD & o2)
{
	return  !(o1 == o2);
}

RX3D_EXPORT Matrix4D Compose(const Vector3D & pos_, const Vector3D & scale_, const QuaternionD & rotation_)
{
	Matrix4D scale;

	scale << scale_[0], 0, 0, 0,
		0, scale_[1], 0, 0,
		0, 0, scale_[2], 0,
		0, 0, 0, 1;

	Matrix4D rotation;
	rotation.setIdentity();
	Eigen::Matrix3d ro = rotation_.toRotationMatrix();
	for (int i = 0; i < 3; i++) {
		for (int j = 0; j < 3; j++) {
			rotation(i, j) = ro(i, j);
		}
	}

	Matrix4D pos;

	pos << 1, 0, 0, pos_[0],
		0, 1, 0, pos_[1],
		0, 0, 1, pos_[2],
		0, 0, 0, 1;

	return pos * rotation * pos;
}

RX3D_EXPORT void Decompose(const Matrix4D & mat, Vector3D & pos, Vector3D & scale, QuaternionD & rotation)
{
	scale.setIdentity(4, 4);
	scale[0] = sqrt(mat(0, 0) * mat(0, 0) + mat(1, 0) * mat(1, 0) + mat(2, 0) * mat(2, 0));
	scale[1] = sqrt(mat(0, 1) * mat(0, 1) + mat(1, 1) * mat(1, 1) + mat(2, 1) * mat(2, 1));
	scale[2] = sqrt(mat(0, 2) * mat(0, 2) + mat(1, 2) * mat(1, 2) + mat(2, 2) * mat(2, 2));

	Eigen::Matrix3d scaleinv;
	scaleinv << 1 / scale[0], 0, 0,
				0, 1 / scale[1], 0,
				0, 0, 1 / scale[2];

	Eigen::Matrix3d temp;

	for (int i = 0; i < 3; i++) {
		for (int j = 0; j < 3; j++) {
			temp(i, j) = mat(i,j);
		}
	}

	temp = temp * scaleinv;
	rotation = QuaternionD(temp);

	pos = Vector3D(mat(0,3), mat(1, 3), mat(2, 3));

}

RX3D_EXPORT DataInputStream & operator>>(DataInputStream & s, RX3DShapeValue & d)
{
	// TODO: 在此处插入 return 语句
	s >> d.type;
	s >> d.data;
	return s;
}

RX3D_EXPORT DataOutputStream & operator<<(DataOutputStream & s, const RX3DShapeValue & d)
{
	// TODO: 在此处插入 return 语句
	s << d.type;
	s << d.data;
	return s;
}

RX3DShapeValue::RX3DShapeValue()
{
	type = RX3DShapeValueType::ST_None;
}

RX3DShapeValue::~RX3DShapeValue()
{
}

RX3DShapeValue::RX3DShapeValue(const RX3DShapeValue & data)
{
	type = data.type;
	this->data = data.data;
}

bool RX3DShapeValue::operator==(const RX3DShapeValue & o) const
{
	if (o.type != type)
	{
		return false;
	}

	if (data != o.data)
	{
		return false;
	}

	return true;
}

bool RX3DShapeValue::operator!=(const RX3DShapeValue & o) const
{
	return !(*this == o);
}

RX3DShapeValue & RX3DShapeValue::operator=(const RX3DShapeValue & o)
{
	// TODO: 在此处插入 return 语句
	memset(&data, 0, sizeof(data));
	type = o.type;
	data = o.data;
	
	return *this;
}

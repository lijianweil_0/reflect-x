#ifndef RXSCENESHADOWNODE_H
#define RXSCENESHADOWNODE_H

#include <RXShadowWorld/RXShadowNode.h>

#include "RX3DCoreTypes.h"
#include <RX3D_global.h>

class RX3DCoreAspect;

class RXSceneShadowNode:public RXShadowNode
{
public:
	RXSceneShadowNode(RX3DCoreAspect* aspect);
	virtual ~RXSceneShadowNode();
	virtual void InitNode(const RXECSDef::NodeCreationInfo& initInfo);//后端节点初始化

	virtual void HandlePropertyChange(const RXECSDef::NodePropertyChangeEvent& change);//处理前端节点属性变化
private:
	void UpdateToMatrix();
	void MarkWorldMatrixDirty();
private:
	RX3DCoreAspect* aspect;

	QuaternionD m_rotation;
	Vector3D m_scale;
	Vector3D m_pos;

	Matrix4D m_matrix;
	Matrix4D m_world_matrix;
	friend class RX3DCoreAspect;
};


#endif // !RXSCENESHADOWNODE_H

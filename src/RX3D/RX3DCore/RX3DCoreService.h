#ifndef RX3D_CORE_SERVICE_H
#define RX3D_CORE_SERVICE_H

#include <RXShadowWorld/RXAspect.h>
#include "RX3DCoreTypes.h"

#include <RX3D_global.h>

class RX3DShapeValue;

struct PositionInfo
{
	bool validate;
	const Vector3D *pos;
	const Vector3D *scale;
	const QuaternionD *rotation;
	const Matrix4D *matrix;

};

class RX3DCoreService:public RXAspectService
{
public:
	virtual PositionInfo GetNodePositionInfo(RXECSDef::NodeId id) = 0;
	virtual const Matrix4D* GetWorldPosition(RXECSDef::NodeId id) = 0;
	virtual bool IsWorldMatrixChanged() = 0;
};

#endif
#ifndef RXSCENECOMPONENT_H
#define RXSCENECOMPONENT_H

#include <RXComponent.h>
#include "RX3DCoreTypes.h"
#include "RX3D_global.h"

class RX3D_EXPORT RXSceneComponent:public RXComponent
{
	REFLECTABLE(RXSceneComponent, RXComponent)
public:
	RXSceneComponent(RXNodeCreationInfo*);
	~RXSceneComponent();

	virtual void SetRotation(QuaternionD q);
	QuaternionD GetRotation() const;

	virtual void SetPostion(Vector3D pos);
	Vector3D GetPostion() const;

	virtual void SetScale(Vector3D scale);
	Vector3D GetScale() const;

	virtual void SetMatrix(Matrix4D mat);
	Matrix4D GetMatrix() const;

	Matrix4D GetWorldMatrix() const;

private:
	Signal<QuaternionD> RotationChanged;
	Signal<Vector3D> PositionChanged;
	Signal<Vector3D> ScaleChanged;
private:
	void UpdateToMatrix();
	void UpdateFromMatrix();
private:
	QuaternionD m_rotation;
	Vector3D m_pos;
	Vector3D m_scale;

	Matrix4D m_matrix;

};

#endif // !RXSCENECOMPONENT_H

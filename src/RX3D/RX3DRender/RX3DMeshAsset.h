#ifndef RX3DMESHASSET_H
#define RX3DMESHASSET_H

#include "RX3D_global.h"

#include <RXAsset.h>
#include "RX3DRender/RX3DRenderTypes.h"

class RX3D_EXPORT RX3DMeshAsset :public RXAsset
{
public:
	REFLECTABLE(RX3DMeshAsset, RXAsset)
	RX3DMeshAsset(RXNodeCreationInfo*);
	~RX3DMeshAsset();

	DECL_SIMPE_PROPERTY(RXMeshBuffer, Mesh);
};

#endif
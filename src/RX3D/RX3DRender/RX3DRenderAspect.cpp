#include "RX3DCore/RX3DCoreService.h"
#include "RXBasicAspect/RXBasicAspectService.h"

#include "Private/RX3DPainter.h"

#include "RX3DMeshComponent.h"
#include "ShadowNodes/RX3DMeshShadowNode.h"
#include "RX3DCameraComponent.h"
#include "ShadowNodes/RX3DCameraShadowNode.h"
#include "RX3DRendererComponent.h"
#include "ShadowNodes/RX3DRendererShadowNode.h"

#include "RX3DMatrialAsset.h"
#include "ShadowNodes/RX3DMaterialShadowNode.h"
#include "RX3DMeshAsset.h"
#include "ShadowNodes/RX3DMeshAssetShadowNode.h"
#include "RX3DRenderActorAsset.h"
#include "ShadowNodes/RX3DRenderActorShadowNode.h"
#include "RX3DShaderProgramAsset.h"
#include "ShadowNodes/RX3DShaderProgramShadowNode.h"
#include "RX3DTextureAsset.h"
#include "ShadowNodes/RX3DTextureShadowNode.h"


#include "RX3DRenderService.h"
#include "RX3DRenderAspect.h"

#include "RXBasicAspect/RXBasicAspectDef.h"
#include "RX3DCore/RX3DCoreDef.h"
#include "RX3DRenderDef.h"

template<typename T>
class GenericFactory:public ShadowNodeFactoryBase
{
public:
	GenericFactory(RX3DRenderAspect* aspect)
	{
		this->aspect = aspect;
	}

	RXShadowNode* CreateNode() override {
		return new T(aspect);
	}

private:

	RX3DRenderAspect* aspect;
};

typedef GenericFactory<RX3DMeshShadowNode> RX3DMeshFactory;
typedef GenericFactory<RX3DRendererShadowNode> RX3DRendererFactory;
typedef GenericFactory<RX3DCameraShadowNode> RX3DCameraFactory;

typedef GenericFactory<RX3DMaterialShadowNode> RX3DMaterialFactory;
typedef GenericFactory<RX3DMeshAssetShadowNode> RX3DMeshAssetFactory;
typedef GenericFactory<RX3DRenderActorShadowNode> RX3DRenderActorFactory;
typedef GenericFactory<RX3DShaderProgramShadowNode> RX3DShaderProgramFactory;
typedef GenericFactory<RX3DTextureShadowNode> RX3DTextureFactory;

class RX3DRendererServiceImp :public RX3DRenderService
{
public:
	RX3DRendererServiceImp(RX3DRenderAspect* aspect) {
		this->aspect = aspect;
	}

private:
	RX3DRenderAspect* aspect;
};

RX3DRenderAspect::RX3DRenderAspect()
{
	SetAspectInfo(RXAspectInfo{ Aspects::AspectNames::RX3DRenderAspect,{
		Aspects::AspectNames::BasicAspect,
		Aspects::AspectNames::RX3DCoreAspect
		} });
}

RX3DRenderAspect::~RX3DRenderAspect()
{
}

RXECSDef::AspectJobList RX3DRenderAspect::CreateAspectJob(std::chrono::system_clock::duration delta)
{
	RXECSDef::AspectJobList jobs;

	RXECSDef::AspectJobPtr OutputPortUpdateJob;//输出接口更新

	if (m_eDirty | Render)
	{
		OutputPortUpdateJob = RXECSDef::AspectJob::CreateJob([=]() {
			UpdateOutputPort();
		}, {});
		jobs.push_back(OutputPortUpdateJob);
	}

	RXECSDef::AspectJobPtr AssetUpdateJob;

	if (m_eDirty & (
		MeshAsset |
		TextureAsset |
		ShaderProgramAsset |
		MaterialAsset |
		RenderActorAsset
		))
	{//资产更新
		RXECSDef::AspectJobList relys;
		if (OutputPortUpdateJob) relys.push_back(OutputPortUpdateJob);
		AssetUpdateJob = RXECSDef::AspectJob::CreateJob([=]() {
			UpdateAsset();
		}, relys);
		jobs.push_back(AssetUpdateJob);
	}

	RXECSDef::AspectJobPtr ViewPortUpdateJob;

	if (m_eDirty | Render)
	{//视口更新
		RXECSDef::AspectJobList relys;
		if (OutputPortUpdateJob) relys.push_back(OutputPortUpdateJob);
		ViewPortUpdateJob = RXECSDef::AspectJob::CreateJob([=]() {
			UpdateViewPort();
		}, relys);
		jobs.push_back(ViewPortUpdateJob);
	}

	RXECSDef::AspectJobPtr WorldMatrixUpdateJob;//更新全局坐标

	if (core_3d_service->IsWorldMatrixChanged())
	{
		WorldMatrixUpdateJob = RXECSDef::AspectJob::CreateJob([=]() {
			UpdateWorldMatrix();
		}, {});
		jobs.push_back(WorldMatrixUpdateJob);
	}

	RXECSDef::AspectJobPtr MeshUpdateJob;

	if (m_eDirty | MeshComp
		|| core_3d_service->IsWorldMatrixChanged())
	{//网格组件更新
		RXECSDef::AspectJobList relys;
		if (OutputPortUpdateJob) relys.push_back(OutputPortUpdateJob);
		if (AssetUpdateJob) relys.push_back(AssetUpdateJob);
		if (OutputPortUpdateJob) relys.push_back(WorldMatrixUpdateJob);
		MeshUpdateJob = RXECSDef::AspectJob::CreateJob([=]() {
			UpdateMesh();
		}, relys);
		jobs.push_back(MeshUpdateJob);
	}

	RXECSDef::AspectJobPtr CameraUpdateJob;

	if (m_eDirty | Camera
		|| core_3d_service->IsWorldMatrixChanged())
	{//摄像机组件更新
		RXECSDef::AspectJobList relys;
		if (OutputPortUpdateJob) relys.push_back(OutputPortUpdateJob);
		if (OutputPortUpdateJob) relys.push_back(WorldMatrixUpdateJob);
		
		CameraUpdateJob = RXECSDef::AspectJob::CreateJob([=]() {
			UpdateCamera();
		}, relys);
		jobs.push_back(CameraUpdateJob);
	}

	m_eDirty = None;

	return jobs;
}

void RX3DRenderAspect::OnNodeDestoried(RXECSDef::NodeId ret)
{
	for (auto &i : m_Output)
	{
		i.second.painter->NodeRemoved(ret);
	}
}

void RX3DRenderAspect::MarkDirty(DirtyMark mark)
{
	m_eDirty = DirtyMark(m_eDirty | mark);
}

void RX3DRenderAspect::AspectBegin()
{
	//获取服务
	base_Service = dynamic_cast<RXBasicAspectService*>(AcquireAspectService(Aspects::AspectService::BasicAspectService));
	core_3d_service = dynamic_cast<RX3DCoreService*>(AcquireAspectService(Aspects::AspectService::RX3DCoreService));

	//注册服务
	render_service = new RX3DRendererServiceImp(this);
	RegisterAspectService(Aspects::AspectService::RX3DRenderService, render_service);

	//注册节点工厂
	cameraNodes = new RX3DCameraFactory(this);
	RegisterShadowFactory<RX3DCameraComponent>(cameraNodes);

	meshNodes = new RX3DMeshFactory(this);
	RegisterShadowFactory<RX3DMeshComponent>(meshNodes);

	rendererNodes = new RX3DRendererFactory(this);
	RegisterShadowFactory<RX3DRendererComponent>(rendererNodes);

	MaterialAssetNodes = new RX3DMaterialFactory(this);
	RegisterShadowFactory<RX3DMatrialAsset>(MaterialAssetNodes);

	MeshAssetNodes = new RX3DMeshAssetFactory(this);
	RegisterShadowFactory<RX3DMeshAsset>(MeshAssetNodes);

	RenderActorAssetNodes = new RX3DRenderActorFactory(this);
	RegisterShadowFactory<RX3DRenderActorAsset>(RenderActorAssetNodes);

	ShaderProgramAssetNodes = new RX3DShaderProgramFactory(this);
	RegisterShadowFactory<RX3DShaderProgramAsset>(ShaderProgramAssetNodes);

	TextureAssetAssetNodes = new RX3DTextureFactory(this);
	RegisterShadowFactory<RX3DTextureAsset>(TextureAssetAssetNodes);

	//注册节点
	RegisterNodeClass<RX3DCameraComponent>();
	RegisterNodeClass<RX3DMeshComponent>();
	RegisterNodeClass<RX3DRendererComponent>();



}

void RX3DRenderAspect::UpdateWorldMatrix()
{
	auto getMatrix = [=](RXECSDef::NodeId nodeid) {
		auto wm = core_3d_service->GetWorldPosition(nodeid);

		if (wm)
		{
			return *wm;
		}
		Matrix4D mat;
		mat.setIdentity(4,4);
		return mat;
	};

	cameraNodes->Foreach([=](RXShadowNode* n) {
		auto node = static_cast<RX3DCameraShadowNode*>(n);

		node->m_WorldMatrix = getMatrix(n->GetNodeId());
		node->m_dirty = true;
		return true;
	});

	meshNodes->Foreach([=](RXShadowNode* n) {
		auto node = static_cast<RX3DMeshShadowNode*>(n);

		node->m_WorldMatrix = getMatrix(n->GetNodeId());
		node->m_dirty = true;
		return true;
	});
}

void RX3DRenderAspect::UpdateOutputPort()
{
	rendererNodes->Foreach([=](RXShadowNode* n) {
		auto node = static_cast<RX3DRendererShadowNode*>(n);

		if (node->m_outputDirty)
		{
			auto out = node->m_output;

			if (out && !m_Output.count(out))//新接口
			{
				OutputInterfaceData data;
				data.out = out;
				data.painter = new RX3DPainter(this);
				data.painter->Initialize(out);
				m_Output[out] = data;
			}

			node->m_outputDirty = false;
		}
		return true;
	});
}

void RX3DRenderAspect::UpdateAsset()
{
	auto updateToPainter = [=](RXShadowNode* n,bool isDirty) {
		for (auto i : m_Output)
		{
			auto painter = i.second.painter;
			if (isDirty || i.second.asset_inited)
			{
				painter->NodeUpdated(n);
			}
		}
	};

	ShaderProgramAssetNodes->Foreach([=](RXShadowNode* n) {
		auto node = static_cast<RX3DShaderProgramShadowNode*>(n);
		updateToPainter(node, node->m_dirty);
		node->m_dirty = false;
		return true;
	});

	TextureAssetAssetNodes->Foreach([=](RXShadowNode* n) {
		auto node = static_cast<RX3DTextureShadowNode*>(n);
		updateToPainter(node, node->m_dirty);
		node->m_dirty = false;
		return true;
	});

	MaterialAssetNodes->Foreach([=](RXShadowNode* n) {
		auto node = static_cast<RX3DMaterialShadowNode*>(n);
		updateToPainter(node, node->m_dirty);
		node->m_dirty = false;
		return true;
	});

	MeshAssetNodes->Foreach([=](RXShadowNode* n) {
		auto node = static_cast<RX3DMeshAssetShadowNode*>(n);
		updateToPainter(node, node->m_dirty);
		node->m_dirty = false;
		return true;
	});

	RenderActorAssetNodes->Foreach([=](RXShadowNode* n) {
		auto node = static_cast<RX3DRenderActorShadowNode*>(n);
		updateToPainter(node, node->m_dirty);
		node->m_dirty = false;
		return true;
	});

	for (auto i : m_Output)
	{
		i.second.asset_inited = true;
	}
}

void RX3DRenderAspect::UpdateViewPort()
{
	rendererNodes->Foreach([=](RXShadowNode* n) {
		auto node = static_cast<RX3DRendererShadowNode*>(n);

		if (node->m_viewPortDirty)
		{
			for (const auto& i : m_Output)
			{
				i.second.painter->NodeUpdated(node);
			}

			node->m_viewPortDirty = false;
		}
		return true;
	});
}

void RX3DRenderAspect::UpdateMesh()
{
	meshNodes->Foreach([=](RXShadowNode* n) {
		auto node = static_cast<RX3DMeshShadowNode*>(n);

		if (node->m_dirty)
		{
			for (const auto& i : m_Output)
			{
				i.second.painter->NodeUpdated(node);
			}

			node->m_dirty = false;
		}
		return true;
	});
}

void RX3DRenderAspect::UpdateCamera()
{
	cameraNodes->Foreach([=](RXShadowNode* n) {
		auto node = static_cast<RX3DCameraShadowNode*>(n);

		if (node->m_dirty)
		{
			for (const auto& i : m_Output)
			{
				i.second.painter->NodeUpdated(node);
			}

			node->m_dirty = false;
		}
		return true;
	});
}

#ifndef RX3DOUTPUTINTERFACE_H
#define RX3DOUTPUTINTERFACE_H

#include <functional>

class RX3DPainterInterface;

class RX3DGLEvent
{
public:
	RX3DGLEvent(int type) {
		m_type = type;
	};
	virtual ~RX3DGLEvent() {}

private:
	int m_type;
};

class RX3DOutputInterface
{
public:
	virtual void RegisterPainter(RX3DPainterInterface * painter) = 0;//向输出接口增加绘图接口
	virtual void UnRegisterPainter(RX3DPainterInterface * painter) = 0;//向输出接口删除绘图接口
	virtual void Update() = 0;//告诉输出接口更新屏幕

	virtual void MakeCurrent() = 0;//使用当前上下文 (该函数只能在RX3DPainterInterface内使用)
	virtual void DoneCurrent() = 0;//结束当前上下文 (该函数只能在RX3DPainterInterface内使用)

	virtual int GetWidth() = 0;//获取宽度
	virtual int GetHeight() = 0;//获取高度

	virtual void PostEvent(RX3DPainterInterface* painter,RX3DGLEvent* event) = 0;//向绘图器抛出事件
};

class RX3DPainterInterface
{
public:
	virtual void glInit() = 0;//GL初始化，每一个注册到输出接口的绘图器都会初次执行
	virtual void onDispose(RX3DOutputInterface* output) = 0;//绘图接口关闭，释放资源
	virtual void onResize(RX3DOutputInterface* output) = 0;//窗口大小重置
	virtual void paintGL(RX3DOutputInterface* output) = 0;//绘制图像
	virtual void onEvent(RX3DOutputInterface* output,RX3DGLEvent* event) = 0;//处理事件
};

#endif // !RX3DOUTPUTINTERFACE_H

#ifndef RX3DRENDERACTORASSET_H
#define RX3DRENDERACTORASSET_H

#include "RX3D_global.h"

#include "RX3DCore/RX3DCoreTypes.h"

#include <RXAsset.h>

//三维世界中可被渲染的对象，由多个基本资产组成，其自身也可通过树形结构组成复杂渲染对象

class RX3D_EXPORT RX3DRenderActorAsset :public RXAsset
{
public:
	REFLECTABLE(RX3DRenderActorAsset, RXAsset);
	RX3DRenderActorAsset(RXNodeCreationInfo*);
	~RX3DRenderActorAsset();


	DECL_SIMPE_PROPERTY(Matrix4D, Matrix);//局部坐标系偏移，用于多个渲染对象资产组成复杂对象时使用
	
	DECL_SIMPE_PROPERTY(RXECSDef::NodeId,MeshAsset);//网格体资产
	DECL_SIMPE_PROPERTY(RXECSDef::NodeId, MatrialAsset);//材料资产
private:

};

#endif // !RX3DRENDERACTORASSET_H

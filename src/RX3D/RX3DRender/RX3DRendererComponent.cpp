#include "RX3DOutputInterface.h"

#include "RX3DRendererComponent.h"

BeginMetaData(RX3DRendererComponent)

META_BEGIN_CONSTRUCTOR(RXNodeCreationInfo*)
META_END_CONSTRUCTOR

META_SIMPE_PROPERTY_BEGIN(Output)
META_SIMPE_PROPERTY_END

META_SIMPE_PROPERTY_BEGIN(ViewPort)
META_SIMPE_PROPERTY_END

EndMetaData

RX3DRendererComponent::RX3DRendererComponent(RXNodeCreationInfo* info):SuperType(info)
{
}

RX3DRendererComponent::~RX3DRendererComponent()
{
}
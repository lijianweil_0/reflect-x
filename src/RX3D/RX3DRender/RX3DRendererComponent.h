#ifndef RX3DRENDERERCOMPONENT_H
#define RX3DRENDERERCOMPONENT_H

#include "RXComponent.h"
#include "RX3DCore/RX3DCoreTypes.h"
#include "RX3D_global.h"

class RX3DOutputInterface;
DECL_VARIANT_TYPE(RX3DOutputInterface*)
class RX3D_EXPORT RX3DRendererComponent :public RXComponent
{
	REFLECTABLE(RX3DRendererComponent, RXComponent)
public:
	RX3DRendererComponent(RXNodeCreationInfo*);
	~RX3DRendererComponent();

	//为渲染器设置输出接口
	DECL_SIMPE_PROPERTY(RX3DOutputInterface*,Output);

	//设置视口,取值范围在0-1
	DECL_SIMPE_PROPERTY(Rect2F, ViewPort);
private:
};

#endif // !RX3DMESHCOMPONENT_H

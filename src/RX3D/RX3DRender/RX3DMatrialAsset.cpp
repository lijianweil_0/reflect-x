#include "RX3DMatrialAsset.h"

BeginMetaData(RX3DMatrialAsset)

META_BEGIN_CONSTRUCTOR(RXNodeCreationInfo*)
META_END_CONSTRUCTOR

META_SIMPE_PROPERTY_BEGIN(TextureAsset)
META_SIMPE_PROPERTY_END

META_SIMPE_PROPERTY_BEGIN(ShaderProgramAsset)
META_SIMPE_PROPERTY_END

EndMetaData

RX3DMatrialAsset::RX3DMatrialAsset(RXNodeCreationInfo* info) :SuperType(info)
{
}

RX3DMatrialAsset::~RX3DMatrialAsset()
{
}

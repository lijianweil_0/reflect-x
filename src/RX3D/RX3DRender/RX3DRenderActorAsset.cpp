#include "RX3DRenderActorAsset.h"

BeginMetaData(RX3DRenderActorAsset)

META_BEGIN_CONSTRUCTOR(RXNodeCreationInfo*)
META_END_CONSTRUCTOR

META_SIMPE_PROPERTY_BEGIN(Matrix)
META_SIMPE_PROPERTY_END

META_SIMPE_PROPERTY_BEGIN(MeshAsset)
META_SIMPE_PROPERTY_END

META_SIMPE_PROPERTY_BEGIN(MatrialAsset)
META_SIMPE_PROPERTY_END

EndMetaData

RX3DRenderActorAsset::RX3DRenderActorAsset(RXNodeCreationInfo* info) :SuperType(info)
{

}

RX3DRenderActorAsset::~RX3DRenderActorAsset()
{

}

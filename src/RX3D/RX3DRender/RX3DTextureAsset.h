#ifndef RX3DTEXTUREASSET_H
#define RX3DTEXTUREASSET_H

#include "RX3D_global.h"

#include <RXAsset.h>
#include "RX3DRender/RX3DRenderTypes.h"

class RXComponent;

//材质是一个物体的表面信息，它有属性和值组成。但这些属性和值的类型是由着色器程序的类型决定的。当匹配的材质和着色器结合在一起成为材料时，才能正确的为物体提供表面信息

class RX3D_EXPORT RX3DTextureAsset :public RXAsset
{
public:
	REFLECTABLE(RX3DTextureAsset, RXAsset)
	RX3DTextureAsset(RXNodeCreationInfo*);
	~RX3DTextureAsset();

	DECL_SIMPE_PROPERTY(RXTextureBuffer, Texture);
};

#endif
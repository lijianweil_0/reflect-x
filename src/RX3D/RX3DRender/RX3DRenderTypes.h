#ifndef RX3DRENDERTYPES_H
#define RX3DRENDERTYPES_H

#include <shareddata.h>
#include <RX3D_global.h>
#include <metatype.h>
#include <variant.h>

struct RXMeshBufferData;

class RXMeshBuffer;
RX3D_EXPORT DataInputStream& operator >> (DataInputStream& s, RXMeshBuffer& b);
RX3D_EXPORT DataOutputStream& operator <<(DataOutputStream& s, const RXMeshBuffer& b);
class RX3D_EXPORT RXMeshBuffer
{
public:

	static constexpr char VertexBuffer[] = "VertexBuffer";//顶点坐标缓冲
	static constexpr char TextureCoordBuffer[] = "TextureCoordBuffer";//纹理坐标缓冲
	static constexpr char FragmentBuffer[] = "FragmentBuffer";//三角面缓冲

	enum ValueType :char
	{
		VT_None = 0,
		VT_FLOAT,
		VT_INT,
		VT_DOUBLE
	};

	RXMeshBuffer();
	~RXMeshBuffer();
	RXMeshBuffer(const RXMeshBuffer& o);

	bool operator == (const RXMeshBuffer& o) const;
	RXMeshBuffer& operator =(const RXMeshBuffer& o);

	//缓冲名，缓冲的值的类型，缓冲的每个数据的大小，缓冲的数据指针，缓冲的长度
	std::vector<std::string> AllBufferNames() const;
	void SetBuffer(const char* BufferName, ValueType type, int tupleSize, const unsigned char* content, int size);
	void ClearBuffer(const char* BufferName);
	void Clear();
	bool IsNULL() const;//内部是否有数据

	const unsigned char* GetData(const char* BufferName) const;
	int GetDataLength(const char* BufferName) const;
	ValueType GetType(const char* BufferName) const;
	int GetTupleSize(const char* BufferName) const;

private:
	SharedDataPointer<RXMeshBufferData> data;
	friend DataInputStream& operator >> (DataInputStream& s, RXMeshBuffer& b);
	friend DataOutputStream& operator <<(DataOutputStream& s, const RXMeshBuffer& b);
};

DECL_VARIANT_TYPE(RXMeshBuffer);
//纹理
class RXTexture;
RX3D_EXPORT DataInputStream& operator >> (DataInputStream& s, RXTexture& b);
RX3D_EXPORT DataOutputStream& operator <<(DataOutputStream& s, const RXTexture& b);
class RX3D_EXPORT RXTexture
{
public:
	enum TextureWarp:char//纹理环绕方式
	{
		TW_Repeat,//重复
		TW_Mirrored_Repeat,//镜像重复
		TW_Clamp_To_Edge,//约束在0-1之间
		TW_Clamp_ToBorder//超出坐标的部分为指定颜色
	};

	enum Filter :char//纹理过滤方式
	{
		MM_None,//无过滤
		MM_Nearest,
		MM_Linear,
		MM_Nearest_Nearest,
		MM_Linear_Nearest,
		MM_Nearest_Linear,
		MM_Linear_Linear
	};
public:
	RXTexture(Image image = {}, TextureWarp warpT = TW_Repeat, TextureWarp warpS = TW_Repeat, ColorF borderColor = {}, Filter min = {}, Filter mag = {});
	RXTexture(const RXTexture& o);
	~RXTexture();

	bool operator ==(const RXTexture& o) const;
	RXTexture& operator =(const RXTexture& o);

public:
	void SetImage(const Image& image);
	Image GetImage() const;

	void SetWarpT(TextureWarp v);
	TextureWarp GetWarpT() const;

	void SetWarpS(TextureWarp v);
	TextureWarp GetWarpS() const;

	void SetBorderColor(const ColorF &o);
	ColorF GetBorderColor() const;

	void SetMinFilter(Filter v);
	Filter GetMinFilter() const;

	void SetMagFilter(Filter v);
	Filter GetMagFilter() const;

private:
	Filter min;
	Filter mag;
	TextureWarp warpT;
	TextureWarp warpS;
	Image image;
	ColorF borderColor;

	friend DataInputStream& operator >> (DataInputStream& s, RXTexture& b);
	friend DataOutputStream& operator <<(DataOutputStream& s, const RXTexture& b);
};
DECL_VARIANT_TYPE(RXTexture)


//材质数据，是将多个材质和相关参数合并在一起的数据
typedef VariantMap RXTextureBufferData;
class RXTextureBuffer;
RX3D_EXPORT DataInputStream& operator >> (DataInputStream& s, RXTextureBuffer& b);
RX3D_EXPORT DataOutputStream& operator <<(DataOutputStream& s, const RXTextureBuffer& b);
class RX3D_EXPORT RXTextureBuffer
{
public:
	static constexpr char ColorImage[] = "ColorImage";//颜色贴图


	RXTextureBuffer();
	~RXTextureBuffer();
	RXTextureBuffer(const RXTextureBuffer& o);

	bool operator == (const RXTextureBuffer& o) const;
	RXTextureBuffer& operator =(const RXTextureBuffer& o);

	void SetAttribute(const char* Name,Variant value);
	void ClearAttribute(const char* Name);
	void Clear();
	Variant GetAttribute(const char* Name) const;
	std::vector<std::string> GetAttributeNames() const;
private:
	SharedDataPointer<RXTextureBufferData> data;
	friend DataInputStream& operator >> (DataInputStream& s, RXTextureBuffer& b);
	friend DataOutputStream& operator <<(DataOutputStream& s, const RXTextureBuffer& b);
};

DECL_VARIANT_TYPE(RXTextureBuffer);

//着色器数据
class RXShaderBuffer;
RX3D_EXPORT DataInputStream& operator >> (DataInputStream& s, RXShaderBuffer& b);
RX3D_EXPORT DataOutputStream& operator <<(DataOutputStream& s, const RXShaderBuffer& b);
class RX3D_EXPORT RXShaderBuffer
{
public:
	enum ShaderType:char
	{
		None,
		Vertices,
		Fragment
	};

	RXShaderBuffer();
	~RXShaderBuffer();
	RXShaderBuffer(const RXShaderBuffer& o);

	bool operator == (const RXShaderBuffer& o) const;
	RXShaderBuffer& operator =(const RXShaderBuffer& o);

	void SetShader(ShaderType type,const char* content);
	const std::string& GetShader(ShaderType type) const;
	void Clear();

private:
	std::map<ShaderType, std::string> data;
	friend DataInputStream& operator >> (DataInputStream& s, RXShaderBuffer& b);
	friend DataOutputStream& operator <<(DataOutputStream& s, const RXShaderBuffer& b);
};

DECL_VARIANT_TYPE(RXShaderBuffer);

#endif // !RX3DRENDERTYPES_H

#ifndef RX3DPAINTERDATA_H
#define RX3DPAINTERDATA_H

#include <memory>
#include <RX3DCore/RX3DCoreTypes.h>
#include <RXECSDef.h>
#include <RX3DRender/RX3DRenderTypes.h>
#include <RX3DRender/RX3DOutputInterface.h>
#include <RXShadowWorld/RXShadowNode.h>

#include <gl/glew.h>

struct RX3DRenderData;
typedef std::shared_ptr<RX3DRenderData> RX3DRenderDataPtr;

typedef std::unordered_map<RXECSDef::NodeId, RX3DRenderDataPtr> DataContainer;

struct RX3DRenderData
{
public:
	virtual ~RX3DRenderData() {};

	void MarkDestory();//标记为需要删除

	void HandleUpdate();
	void CopyData(RXShadowNode* shadowNode);

	static RX3DRenderDataPtr CreateData(RXShadowNode* shadowNode);

protected:
	virtual	void handleUpdateImp() = 0;
	virtual void CopyDataImp(RXShadowNode* shadowNode) = 0;
public:
	bool isDirty = true;
	bool isDestory = false;
	DataContainer* container = NULL;
	RXECSDef::NodeId id;
};


struct MeshData :public RX3DRenderData
{
	RXECSDef::NodeId m_MeshAsset;
	Matrix4D m_worldMartix;

	~MeshData();
	void handleUpdateImp() override;
	void CopyDataImp(RXShadowNode* shadowNode) override;
};

struct CameraData :public RX3DRenderData
{
	Matrix4D m_worldMartix;


	Matrix4D m_worldMartixInverted;


	~CameraData();
	void handleUpdateImp() override;
	void CopyDataImp(RXShadowNode* shadowNode) override;
};

struct RenderData :public RX3DRenderData
{
	RX3DOutputInterface* m_output;
	Rect2F m_viewPort;//渲染视口位置


	~RenderData();
	void handleUpdateImp() override;
	void CopyDataImp(RXShadowNode* shadowNode) override;
};

struct MaterialData :public RX3DRenderData
{
	RXECSDef::NodeId m_texture;
	RXECSDef::NodeId m_shaderProgram;


	~MaterialData();
	void handleUpdateImp() override;
	void CopyDataImp(RXShadowNode* shadowNode) override;
};

struct MeshAssetData :public RX3DRenderData
{
	RXMeshBuffer m_mesh;

	~MeshAssetData();
	void handleUpdateImp() override;
	void CopyDataImp(RXShadowNode* shadowNode) override;
};

struct RenderActorData :public RX3DRenderData
{
	Matrix4D m_matrix;
	RXECSDef::NodeId m_meshAsset;
	RXECSDef::NodeId m_materialAsset;


	~RenderActorData();
	void handleUpdateImp() override;
	void CopyDataImp(RXShadowNode* shadowNode) override;
};

struct ShaderProgramData :public RX3DRenderData
{
	RXShaderBuffer shaderBuffer;

	GLuint gl_shader_program = 0;
	GLuint gl_fShader = 0;
	GLuint gl_vShader = 0;

	~ShaderProgramData();
	void handleUpdateImp() override;
	void CopyDataImp(RXShadowNode* shadowNode) override;
};

struct TextureData :public RX3DRenderData
{
	RXTextureBuffer texture;
	
	std::map<std::string, int> textureMap;
	std::vector<GLuint> gl_textures;


	~TextureData();
	void handleUpdateImp() override;
	void CopyDataImp(RXShadowNode* shadowNode) override;
};

#endif // !RX3DPAINTERDATA_H

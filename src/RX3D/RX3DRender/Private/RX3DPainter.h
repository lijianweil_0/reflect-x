#ifndef RX3DPAINTER_H
#define RX3DPAINTER_H

#include <memory>
#include <mutex>

#include <RXECSDef.h>
#include <RXShadowWorld/RXShadowNode.h>
#include <RX3DCore/RX3DCoreTypes.h>
#include <RX3DRender/RX3DOutputInterface.h>
#include "RX3DPainterData.h"


class RX3DRenderAspect;
struct RX3DRenderData;

class RX3DPainter:public RX3DPainterInterface
{
public:
	RX3DPainter(RX3DRenderAspect* aspect);
	~RX3DPainter();

	void Initialize(RX3DOutputInterface* output);

	void NodeUpdated(RXShadowNode* node);
	void NodeRemoved(RXECSDef::NodeId node);

private:
	RX3DRenderData* GetOrAcquireData(RXShadowNode* node);

private:
	void Update();
private:
	struct Private;
	std::unique_ptr<Private> _P;

	// ͨ�� RX3DPainterInterface �̳�
	virtual void onDispose(RX3DOutputInterface* output) override;
	virtual void onResize(RX3DOutputInterface* output) override;
	virtual void paintGL(RX3DOutputInterface* output) override;
	virtual void onEvent(RX3DOutputInterface* output,RX3DGLEvent * event) override;
	virtual void glInit() override;
};

#undef DECL_ITEM_POINTER;
#endif // !RX3DPAINTER_H
#include <gl/glew.h>

#include "RX3DRender/RX3DRenderAspect.h"
#include "RX3DCore/RX3DCoreService.h"

#include "RX3DPainter.h"
#include "RX3DPainterData.h"


class InvokeFunctionEvent:public RX3DGLEvent
{
public:
	InvokeFunctionEvent(std::function<void()> fun) :RX3DGLEvent (0){
		this->fun = fun;
	}

	std::function<void()> fun;
};


struct RX3DPainter::Private {

	DataContainer datas;
	std::mutex m_mutex;
	bool m_dirty = false;
	RX3DOutputInterface* output;
	RX3DRenderAspect* aspect;
};

RX3DPainter::RX3DPainter(RX3DRenderAspect * aspect):_P(new Private)
{
	_P->aspect = aspect;
}

RX3DPainter::~RX3DPainter()
{
}

void RX3DPainter::Initialize(RX3DOutputInterface * output)
{
	//��ʼ��GL
	glewInit();

	output->RegisterPainter(this);
	_P->output = output;
}

void RX3DPainter::NodeUpdated(RXShadowNode * node)
{
	if (!node)
	{
		return;
	}
	auto id = node->GetNodeId();
	std::lock_guard<std::mutex> lock(_P->m_mutex);
	auto data = GetOrAcquireData(node);

	if (!data)
	{
		return;
	}

	_P->m_dirty = true;
	data->CopyData(node);

	if (_P->m_dirty)
	{
		_P->output->PostEvent(this, new InvokeFunctionEvent(std::bind(&RX3DPainter::Update,this)));
	}
}

void RX3DPainter::NodeRemoved(RXECSDef::NodeId node)
{
	std::lock_guard<std::mutex> lock(_P->m_mutex);
	if (_P->datas.count(node))
	{
		_P->datas[node]->isDestory = true;
		_P->m_dirty = true;
	}

	_P->output->PostEvent(this, new InvokeFunctionEvent(std::bind(&RX3DPainter::Update, this)));
}

RX3DRenderData* RX3DPainter::GetOrAcquireData(RXShadowNode * node)
{
	auto id = node->GetNodeId();
	if (_P->datas.count(id))
	{
		return _P->datas[id].get();
	}
	else
	{
		auto data = RX3DRenderData::CreateData(node);
		data->container = &_P->datas;
		if (!data)
		{
			return NULL;
		}
		_P->datas[id] = data;
		return data.get();
	}
}

void RX3DPainter::onDispose(RX3DOutputInterface* output)
{

}

void RX3DPainter::onResize(RX3DOutputInterface* output)
{
}

void RX3DPainter::paintGL(RX3DOutputInterface* output)
{
}

void RX3DPainter::onEvent(RX3DOutputInterface* output,RX3DGLEvent * event)
{
	if (dynamic_cast<InvokeFunctionEvent*>(event))
	{
		dynamic_cast<InvokeFunctionEvent*>(event)->fun();
	}
}

void RX3DPainter::glInit()
{
	glewInit();
}

void RX3DPainter::Update()
{
	std::lock_guard<std::mutex> lock(_P->m_mutex);

	if (!_P->m_dirty)
	{
		return;
	}

	for (auto it = _P->datas.begin(); it != _P->datas.end(); )
	{
		if (it->second->isDestory)
		{
			it  = _P->datas.erase(it);
		}
		else if (it->second->isDirty) {
			it->second->HandleUpdate();
			it++;
		}
		else {
			it++;
		}
	}

	_P->m_dirty = false;
}
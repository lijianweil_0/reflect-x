#include "RX3DRender/RX3DRenderAspect.h"
#include "RX3DRender/RX3DMeshComponent.h"
#include "RX3DMeshShadowNode.h"

RX3DMeshShadowNode::RX3DMeshShadowNode(RX3DRenderAspect * aspect)
{
	this->aspect = aspect;
	m_dirty = true;
}

RX3DMeshShadowNode::~RX3DMeshShadowNode()
{
}

void RX3DMeshShadowNode::InitNode(const RXECSDef::NodeCreationInfo & initInfo)
{
	RXShadowNode::InitNode(initInfo);

	static int MeshAsset = RX3DMeshComponent::staticMetaInfo().IndexOfProperty("RenderActorAsset");
	
	m_MeshAsset = initInfo.init_property.at(MeshAsset).Value<RXECSDef::NodeId>();
	MarkMsehCompDirty();
}

void RX3DMeshShadowNode::HandlePropertyChange(const RXECSDef::NodePropertyChangeEvent & change)
{
	RXShadowNode::HandlePropertyChange(change);

	static int MeshAsset = RX3DMeshComponent::staticMetaInfo().IndexOfProperty("RenderActorAsset");

	if (change.propIndex == MeshAsset)
	{
		m_MeshAsset = change.value.Value<RXECSDef::NodeId>();
		MarkMsehCompDirty();
	}

}

void RX3DMeshShadowNode::MarkMsehCompDirty()
{
	m_dirty = true;
	aspect->MarkDirty(RX3DRenderAspect::DirtyMark::MeshComp);
}


#include "RX3DRender/RX3DRenderAspect.h"
#include "RX3DRender/RX3DTextureAsset.h"
#include "RX3DTextureShadowNode.h"

RX3DTextureShadowNode::RX3DTextureShadowNode(RX3DRenderAspect * aspect)
{
	this->aspect = aspect;
	m_dirty = true;
}

RX3DTextureShadowNode::~RX3DTextureShadowNode()
{
}

void RX3DTextureShadowNode::InitNode(const RXECSDef::NodeCreationInfo & initInfo)
{
	RXShadowNode::InitNode(initInfo);

	static int TextureIndex = RX3DTextureAsset::staticMetaInfo().IndexOfProperty("Texture");

	texture = initInfo.init_property.at(TextureIndex).Value<RXTextureBuffer>();
	MarkTextureDirty();
}

void RX3DTextureShadowNode::HandlePropertyChange(const RXECSDef::NodePropertyChangeEvent & change)
{
	static int TextureIndex = RX3DTextureAsset::staticMetaInfo().IndexOfProperty("Texture");

	if (change.propIndex == TextureIndex)
	{
		texture = change.value.Value<RXTextureBuffer>();
		MarkTextureDirty();
	}
}

void RX3DTextureShadowNode::MarkTextureDirty()
{
	m_dirty = true;
	aspect->MarkDirty(RX3DRenderAspect::DirtyMark::TextureAsset);
}

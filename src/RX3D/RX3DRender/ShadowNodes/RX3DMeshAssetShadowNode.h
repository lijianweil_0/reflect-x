#ifndef RX3DMESHASSETSHADOWNODE_H
#define RX3DMESHASSETSHADOWNODE_H

#include <RXShadowWorld/RXShadowNode.h>
#include <RX3D_global.h>
class RX3DRenderAspect;

class RX3DMeshAssetShadowNode :public RXShadowNode
{
public:
	RX3DMeshAssetShadowNode(RX3DRenderAspect* aspect);
	virtual ~RX3DMeshAssetShadowNode();
	virtual void InitNode(const RXECSDef::NodeCreationInfo& initInfo);//后端节点初始化

	virtual void HandlePropertyChange(const RXECSDef::NodePropertyChangeEvent& change);//处理前端节点属性变化

	void MarkDirty();


	RX3DRenderAspect* aspect;
	bool m_dirty;
	RXMeshBuffer m_mesh;
	
	friend class RX3DRenderAspect;
};

#endif // !RX3DMESHASSETSHADOWNODE_H

#include "RX3DRender/RX3DRenderAspect.h"
#include "RX3DRender/RX3DMeshAsset.h"
#include "RX3DMeshAssetShadowNode.h"

RX3DMeshAssetShadowNode::RX3DMeshAssetShadowNode(RX3DRenderAspect * aspect)
{
	this->aspect = aspect;
	m_dirty = true;
}

RX3DMeshAssetShadowNode::~RX3DMeshAssetShadowNode()
{
}

void RX3DMeshAssetShadowNode::InitNode(const RXECSDef::NodeCreationInfo & initInfo)
{
	RXShadowNode::InitNode(initInfo);

	static int Mesh = RX3DMeshAsset::staticMetaInfo().IndexOfProperty("Mesh");

	m_mesh = initInfo.init_property.at(Mesh).Value<RXMeshBuffer>();
	MarkDirty();
}

void RX3DMeshAssetShadowNode::HandlePropertyChange(const RXECSDef::NodePropertyChangeEvent & change)
{
	RXShadowNode::HandlePropertyChange(change);

	static int Mesh = RX3DMeshAsset::staticMetaInfo().IndexOfProperty("Mesh");

	if (change.propIndex == Mesh)
	{
		m_mesh = change.value.Value<RXMeshBuffer>();
		MarkDirty();
	}
}

void RX3DMeshAssetShadowNode::MarkDirty()
{
	m_dirty = true;
	aspect->MarkDirty(RX3DRenderAspect::DirtyMark::MeshAsset);
}

#include "RX3DRender/RX3DRenderAspect.h"
#include "RX3DRender/RX3DCameraComponent.h"
#include "RX3DCameraShadowNode.h"

RX3DCameraShadowNode::RX3DCameraShadowNode(RX3DRenderAspect * aspect)
{
	this->aspect = aspect;
	m_dirty = true;
}

RX3DCameraShadowNode::~RX3DCameraShadowNode()
{
}

void RX3DCameraShadowNode::InitNode(const RXECSDef::NodeCreationInfo & initInfo)
{
	RXShadowNode::InitNode(initInfo);
	MarkDirty();
}

void RX3DCameraShadowNode::HandlePropertyChange(const RXECSDef::NodePropertyChangeEvent & change)
{
	RXShadowNode::HandlePropertyChange(change);
}

void RX3DCameraShadowNode::MarkDirty()
{
	m_dirty = true;
	aspect->MarkDirty(RX3DRenderAspect::DirtyMark::Camera);
}

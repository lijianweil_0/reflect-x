#ifndef RX3DMATERIALASSET_H
#define RX3DMATERIALASSET_H

#include <RXShadowWorld/RXShadowNode.h>
#include <RX3D_global.h>
#include "RX3DRender/RX3DRenderTypes.h"
class RX3DRenderAspect;

class RX3DMaterialShadowNode :public RXShadowNode
{
public:
	RX3DMaterialShadowNode(RX3DRenderAspect* aspect);
	virtual ~RX3DMaterialShadowNode();
	virtual void InitNode(const RXECSDef::NodeCreationInfo& initInfo);//后端节点初始化

	virtual void HandlePropertyChange(const RXECSDef::NodePropertyChangeEvent& change);//处理前端节点属性变化

	void MarkDirty();


	RX3DRenderAspect* aspect;
	bool m_dirty;
	RXECSDef::NodeId m_texture;
	RXECSDef::NodeId m_shaderProgram;
	friend class RX3DRenderAspect;
};


#endif // !RXSCENESHADOWNODE_H

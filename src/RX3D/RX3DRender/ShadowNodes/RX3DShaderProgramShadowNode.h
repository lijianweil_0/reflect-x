#ifndef RX3DSHADERPROGRAMSHADOWNODE_H
#define RX3DSHADERPROGRAMSHADOWNODE_H

#include <RXShadowWorld/RXShadowNode.h>
#include <RX3D_global.h>

#include "RX3DRender/Private/RX3DPainterData.h"

class RX3DRenderAspect;

class RX3DShaderProgramShadowNode :public RXShadowNode
{
public:
	RX3DShaderProgramShadowNode(RX3DRenderAspect* aspect);
	virtual ~RX3DShaderProgramShadowNode();
	virtual void InitNode(const RXECSDef::NodeCreationInfo& initInfo);//后端节点初始化

	virtual void HandlePropertyChange(const RXECSDef::NodePropertyChangeEvent& change);//处理前端节点属性变化

	void MarkDirty();


	RX3DRenderAspect* aspect;
	bool m_dirty;

	RXShaderBuffer m_shader;
	friend class RX3DRenderAspect;
};


#endif // !RX3DSHADERPROGRAMSHADOWNODE_H

#include "RX3DRender/RX3DRenderAspect.h"
#include "RX3DRender/RX3DShaderProgramAsset.h"
#include "RX3DShaderProgramShadowNode.h"

RX3DShaderProgramShadowNode::RX3DShaderProgramShadowNode(RX3DRenderAspect * aspect)
{
	this->aspect = aspect;
	m_dirty = true;
}

RX3DShaderProgramShadowNode::~RX3DShaderProgramShadowNode()
{
}

void RX3DShaderProgramShadowNode::InitNode(const RXECSDef::NodeCreationInfo & initInfo)
{
	RXShadowNode::InitNode(initInfo);

	static int Shader = RX3DShaderProgramAsset::staticMetaInfo().IndexOfProperty("Shader");

	m_shader = initInfo.init_property.at(Shader).Value<RXShaderBuffer>();
	MarkDirty();
}

void RX3DShaderProgramShadowNode::HandlePropertyChange(const RXECSDef::NodePropertyChangeEvent & change)
{
	RXShadowNode::HandlePropertyChange(change);

	static int Shader = RX3DShaderProgramAsset::staticMetaInfo().IndexOfProperty("Shader");

	if (change.propIndex == Shader)
	{
		m_shader = change.value.Value<RXShaderBuffer>();
		MarkDirty();
	}
}

void RX3DShaderProgramShadowNode::MarkDirty()
{
	m_dirty = true;
	aspect->MarkDirty(RX3DRenderAspect::DirtyMark::ShaderProgramAsset);
}

#ifndef RX3DTEXTURESHADOWNODE_H
#define RX3DTEXTURESHADOWNODE_H

#include <RXShadowWorld/RXShadowNode.h>
#include <RX3D_global.h>
#include "RX3DRender/RX3DRenderTypes.h"
#include "RX3DRender/Private/RX3DPainterData.h"

class RX3DRenderAspect;

class RX3DTextureShadowNode :public RXShadowNode
{
public:
	RX3DTextureShadowNode(RX3DRenderAspect* aspect);
	virtual ~RX3DTextureShadowNode();
	virtual void InitNode(const RXECSDef::NodeCreationInfo& initInfo);//后端节点初始化

	virtual void HandlePropertyChange(const RXECSDef::NodePropertyChangeEvent& change);//处理前端节点属性变化

	void MarkTextureDirty();


	RX3DRenderAspect* aspect;
	RXTextureBuffer texture;
	bool m_dirty;
	friend class RX3DRenderAspect;
};


#endif // !RXSCENESHADOWNODE_H

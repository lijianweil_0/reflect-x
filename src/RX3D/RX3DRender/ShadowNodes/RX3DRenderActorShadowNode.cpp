#include "RX3DRender/RX3DRenderAspect.h"
#include "RX3DRender/RX3DRenderActorAsset.h"
#include "RX3DRenderActorShadowNode.h"

RX3DRenderActorShadowNode::RX3DRenderActorShadowNode(RX3DRenderAspect * aspect)
{
	this->aspect = aspect;
	m_dirty = true;
}

RX3DRenderActorShadowNode::~RX3DRenderActorShadowNode()
{
}

void RX3DRenderActorShadowNode::InitNode(const RXECSDef::NodeCreationInfo & initInfo)
{
	RXShadowNode::InitNode(initInfo);

	static int MatrixIndex = RX3DRenderActorAsset::staticMetaInfo().IndexOfProperty("Matrix");
	static int MeshIndex = RX3DRenderActorAsset::staticMetaInfo().IndexOfProperty("MeshAsset");
	static int MatrialIndex = RX3DRenderActorAsset::staticMetaInfo().IndexOfProperty("MatrialAsset");


	m_matrix = initInfo.init_property.at(MatrixIndex).Value<Matrix4D>();
	m_meshAsset = initInfo.init_property.at(MeshIndex).Value<RXECSDef::NodeId>();
	m_materialAsset = initInfo.init_property.at(MatrialIndex).Value<RXECSDef::NodeId>();
	MarkDirty();
}

void RX3DRenderActorShadowNode::HandlePropertyChange(const RXECSDef::NodePropertyChangeEvent & change)
{
	RXShadowNode::HandlePropertyChange(change);

	static int MatrixIndex = RX3DRenderActorAsset::staticMetaInfo().IndexOfProperty("Matrix");
	static int MeshIndex = RX3DRenderActorAsset::staticMetaInfo().IndexOfProperty("MeshAsset");
	static int MatrialIndex = RX3DRenderActorAsset::staticMetaInfo().IndexOfProperty("MatrialAsset");

	if (change.propIndex == MatrixIndex)
	{
		m_matrix = change.value.Value<Matrix4D>();
		MarkDirty();
	}
	else if (change.propIndex == MeshIndex)
	{
		m_meshAsset = change.value.Value<RXECSDef::NodeId>();
		MarkDirty();
	}
	else if (change.propIndex == MatrialIndex)
	{
		m_materialAsset = change.value.Value<RXECSDef::NodeId>();
		MarkDirty();
	}
}

void RX3DRenderActorShadowNode::MarkDirty()
{
	m_dirty = true;
	aspect->MarkDirty(RX3DRenderAspect::DirtyMark::RenderActorAsset);
}

#ifndef RXSCENESHADOWNODE_H
#define RXSCENESHADOWNODE_H

#include <RXShadowWorld/RXShadowNode.h>

#include <RX3D_global.h>

class RX3DRenderAspect;

class RX3DCameraShadowNode :public RXShadowNode
{
public:
	RX3DCameraShadowNode(RX3DRenderAspect* aspect);
	virtual ~RX3DCameraShadowNode();
	virtual void InitNode(const RXECSDef::NodeCreationInfo& initInfo);//后端节点初始化

	virtual void HandlePropertyChange(const RXECSDef::NodePropertyChangeEvent& change);//处理前端节点属性变化
	void MarkDirty();


	RX3DRenderAspect* aspect;
	Matrix4D m_WorldMatrix;
	bool m_dirty;
	friend class RX3DRenderAspect;
};


#endif // !RXSCENESHADOWNODE_H

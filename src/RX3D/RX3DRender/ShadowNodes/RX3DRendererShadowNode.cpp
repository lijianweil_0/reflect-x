#include "RX3DRender/RX3DRendererComponent.h"
#include "RX3DRender/RX3DRenderAspect.h"

#include "RX3DRendererShadowNode.h"

RX3DRendererShadowNode::RX3DRendererShadowNode(RX3DRenderAspect * aspect)
{
	this->aspect = aspect;
	m_outputDirty = true;
	m_viewPortDirty = true;
}

RX3DRendererShadowNode::~RX3DRendererShadowNode()
{
}

void RX3DRendererShadowNode::InitNode(const RXECSDef::NodeCreationInfo & initInfo)
{
	RXShadowNode::InitNode(initInfo);

	static int ViewPort = RX3DRendererComponent::staticMetaInfo().IndexOfProperty("ViewPort");
	static int Output = RX3DRendererComponent::staticMetaInfo().IndexOfProperty("Output");
	
	m_viewPort = initInfo.init_property.at(ViewPort).Value<Rect2F>();
	m_output = initInfo.init_property.at(Output).Value<RX3DOutputInterface*>();

	MarkOutputDirty();
	MarkViewPortDirty();
}

void RX3DRendererShadowNode::HandlePropertyChange(const RXECSDef::NodePropertyChangeEvent & change)
{
	RXShadowNode::HandlePropertyChange(change);

	static int ViewPort = RX3DRendererComponent::staticMetaInfo().IndexOfProperty("ViewPort");
	static int Output = RX3DRendererComponent::staticMetaInfo().IndexOfProperty("Output");

	if (change.propIndex == ViewPort)
	{
		m_viewPort = change.value.Value<Rect2F>();
		MarkOutputDirty();
	}
	else if (change.propIndex == Output)
	{
		m_output = change.value.Value<RX3DOutputInterface*>();
		MarkViewPortDirty();
	}
}

void RX3DRendererShadowNode::MarkOutputDirty()
{
	m_outputDirty = true;
	aspect->MarkDirty(RX3DRenderAspect::DirtyMark::Render);
}

void RX3DRendererShadowNode::MarkViewPortDirty()
{
	m_viewPortDirty = true;
	aspect->MarkDirty(RX3DRenderAspect::DirtyMark::Render);
}

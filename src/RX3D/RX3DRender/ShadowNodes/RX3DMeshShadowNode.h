#ifndef RX3DMESHSHADOWNODE_H
#define RX3DMESHSHADOWNODE_H

#include <RXShadowWorld/RXShadowNode.h>
#include <RX3D_global.h>
#include "RX3DRender/Private/RX3DPainterData.h"
class RX3DRenderAspect;

class RX3DMeshShadowNode :public RXShadowNode
{
public:
	RX3DMeshShadowNode(RX3DRenderAspect* aspect);
	virtual ~RX3DMeshShadowNode();
	virtual void InitNode(const RXECSDef::NodeCreationInfo& initInfo);//后端节点初始化

	virtual void HandlePropertyChange(const RXECSDef::NodePropertyChangeEvent& change);//处理前端节点属性变化

	void MarkMsehCompDirty();


	RX3DRenderAspect* aspect;
	bool m_dirty;
	RXECSDef::NodeId m_MeshAsset;
	Matrix4D m_WorldMatrix;
	
	friend class RX3DRenderAspect;
};


#endif // !RXSCENESHADOWNODE_H

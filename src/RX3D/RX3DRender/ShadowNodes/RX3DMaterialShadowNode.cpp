#include "RX3DRender/RX3DRenderAspect.h"
#include "RX3DRender/RX3DMeshComponent.h"
#include "RX3DMaterialShadowNode.h"

RX3DMaterialShadowNode::RX3DMaterialShadowNode(RX3DRenderAspect * aspect)
{
	this->aspect = aspect;
	m_dirty = true;
}

RX3DMaterialShadowNode::~RX3DMaterialShadowNode()
{
}

void RX3DMaterialShadowNode::InitNode(const RXECSDef::NodeCreationInfo & initInfo)
{
	RXShadowNode::InitNode(initInfo);

	static int Texture = RX3DMeshComponent::staticMetaInfo().IndexOfProperty("TextureAsset");
	static int ShaderProgram = RX3DMeshComponent::staticMetaInfo().IndexOfProperty("ShaderProgramAsset");


	m_texture = initInfo.init_property.at(Texture).Value<RXECSDef::NodeId>();
	m_shaderProgram = initInfo.init_property.at(ShaderProgram).Value<RXECSDef::NodeId>();
	MarkDirty();
}

void RX3DMaterialShadowNode::HandlePropertyChange(const RXECSDef::NodePropertyChangeEvent & change)
{
	RXShadowNode::HandlePropertyChange(change);

	static int Texture = RX3DMeshComponent::staticMetaInfo().IndexOfProperty("TextureAsset");
	static int ShaderProgram = RX3DMeshComponent::staticMetaInfo().IndexOfProperty("ShaderProgramAsset");

	if (change.propIndex == Texture)
	{
		m_texture = change.value.Value<RXECSDef::NodeId>();
		MarkDirty();
	}
	else if (change.propIndex == ShaderProgram)
	{
		m_shaderProgram = change.value.Value<RXECSDef::NodeId>();
		MarkDirty();
	}
}

void RX3DMaterialShadowNode::MarkDirty()
{
	m_dirty = true;
	aspect->MarkDirty(RX3DRenderAspect::DirtyMark::MaterialAsset);
}

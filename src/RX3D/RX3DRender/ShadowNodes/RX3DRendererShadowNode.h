#ifndef RX3DRENDERERSHADOWNODE_H
#define RX3DRENDERERSHADOWNODE_H

#include <RXShadowWorld/RXShadowNode.h>

#include <RX3DRender/RX3DOutputInterface.h>
#include "RX3DCore/RX3DCoreTypes.h"
#include "RX3DRender/Private/RX3DPainterData.h"
#include <RX3D_global.h>

class RX3DRenderAspect;

class RX3DRendererShadowNode :public RXShadowNode
{
public:
	RX3DRendererShadowNode(RX3DRenderAspect* aspect);
	virtual ~RX3DRendererShadowNode();
	virtual void InitNode(const RXECSDef::NodeCreationInfo& initInfo);//后端节点初始化

	virtual void HandlePropertyChange(const RXECSDef::NodePropertyChangeEvent& change);//处理前端节点属性变化

	void MarkOutputDirty();
	void MarkViewPortDirty();

	RX3DRenderAspect* aspect;
	RX3DOutputInterface* m_output;
	Rect2F m_viewPort;//渲染视口位置
	bool m_outputDirty;
	bool m_viewPortDirty;

	friend class RX3DRenderAspect;
};


#endif // !RX3DRENDERERSHADOWNODE_H

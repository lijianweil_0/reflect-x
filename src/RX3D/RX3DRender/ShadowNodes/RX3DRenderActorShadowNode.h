#ifndef RX3DRENDERACTORSHADOWNODE_H
#define RX3DRENDERACTORSHADOWNODE_H

#include "RX3DCore/RX3DCoreTypes.h"
#include <RXShadowWorld/RXShadowNode.h>
#include <RX3D_global.h>

class RX3DRenderAspect;

class RX3DRenderActorShadowNode :public RXShadowNode
{
public:
	RX3DRenderActorShadowNode(RX3DRenderAspect* aspect);
	virtual ~RX3DRenderActorShadowNode();
	virtual void InitNode(const RXECSDef::NodeCreationInfo& initInfo);//后端节点初始化

	virtual void HandlePropertyChange(const RXECSDef::NodePropertyChangeEvent& change);//处理前端节点属性变化

	void MarkDirty();


	RX3DRenderAspect* aspect;
	bool m_dirty;

	Matrix4D m_matrix;
	RXECSDef::NodeId m_meshAsset;
	RXECSDef::NodeId m_materialAsset;
	friend class RX3DRenderAspect;
};


#endif // !RXSCENESHADOWNODE_H

#ifndef RX3DCAMERACOMPONENT_H
#define RX3DCAMERACOMPONENT_H

#include "RX3DCore/RXSceneComponent.h"
#include "RX3D_global.h"

class RX3D_EXPORT RX3DCameraComponent:public RXSceneComponent
{
	REFLECTABLE(RX3DCameraComponent, RXSceneComponent)
public:
	RX3DCameraComponent(RXNodeCreationInfo*);
	~RX3DCameraComponent();
private:


};

#endif // !RX3DCAMERACOMPONENT_H

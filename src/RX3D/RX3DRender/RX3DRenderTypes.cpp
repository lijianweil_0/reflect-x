#include <initializefunction.h>
#include "RX3DRenderTypes.h"

static void RX3DRenderRegisterTypes() {
	MetaType::IdFromType<RXMeshBuffer>();
	MetaType::IdFromType<RXTexture>();
	MetaType::IdFromType<RXTextureBuffer>();
	MetaType::IdFromType<RXShaderBuffer>();

	MetaType::RegisterStreamOperator<RXMeshBuffer>();
	MetaType::RegisterStreamOperator<RXTexture>();
	MetaType::RegisterStreamOperator<RXTextureBuffer>();
	MetaType::RegisterStreamOperator<RXShaderBuffer>();
}

INITIALIZE_FUN(RX3DRenderRegisterTypes);

struct SingleBuffer
{
	RXMeshBuffer::ValueType type;
	int tupleSize;
	ByteArray content;

	bool operator ==(const SingleBuffer& o) const {
		return type == o.type && tupleSize == o.tupleSize && content == o.content;
	}
};

DataInputStream& operator >> (DataInputStream& s, SingleBuffer& b) {
	s >> b.type;
	s >> b.tupleSize;
	s >> b.content;
	return s;
}

DataOutputStream& operator <<(DataOutputStream& s, const SingleBuffer& b) {
	s << b.type;
	s << b.tupleSize;
	s << b.content;
	return s;
}

struct RXMeshBufferData
{
	std::map<std::string, SingleBuffer> Buffers;

	bool operator ==(const RXMeshBufferData& o) const {
		return Buffers == o.Buffers;
	}
};

RXMeshBuffer::RXMeshBuffer()
{
}

RXMeshBuffer::~RXMeshBuffer()
{
}

RXMeshBuffer::RXMeshBuffer(const RXMeshBuffer & o) :data(o.data)
{
}

bool RXMeshBuffer::operator==(const RXMeshBuffer & o) const
{
	if (data == o.data)
	{
		return true;
	}

	if (data.IsEmpty() || o.data.IsEmpty())
	{
		return false;
	}

	return *data == *o.data;
}

RXMeshBuffer & RXMeshBuffer::operator=(const RXMeshBuffer & o)
{
	// TODO: 在此处插入 return 语句
	data = o.data;
	return *this;
}

std::vector<std::string> RXMeshBuffer::AllBufferNames() const
{
	if (data.IsEmpty())
	{
		return {};
	}

	std::vector<std::string> ret;
	for (const auto& i : data->Buffers)
	{
		ret.push_back(i.first);
	}

	return ret;
}

void RXMeshBuffer::SetBuffer(const char * BufferName, ValueType type, int tupleSize, const unsigned char * content, int size)
{
	data->Buffers[BufferName] = SingleBuffer();
	auto& buffer = data->Buffers[BufferName];
	buffer.content = { content ,content + size };
	buffer.tupleSize = tupleSize;
	buffer.type = type;
}

void RXMeshBuffer::ClearBuffer(const char * BufferName)
{
	data->Buffers.erase(BufferName);
}

void RXMeshBuffer::Clear()
{
	data->Buffers.clear();
}

bool RXMeshBuffer::IsNULL() const
{
	return data.IsEmpty();
}

const unsigned char * RXMeshBuffer::GetData(const char * BufferName) const
{
	if (data.IsEmpty())
	{
		return NULL;
	}

	if (!data->Buffers.count(BufferName))
	{
		return NULL;
	}

	return data->Buffers[BufferName].content.data();
}

int RXMeshBuffer::GetDataLength(const char * BufferName) const
{
	if (data.IsEmpty())
	{
		return NULL;
	}

	if (!data->Buffers.count(BufferName))
	{
		return NULL;
	}

	return data->Buffers[BufferName].content.size();
}

RXMeshBuffer::ValueType RXMeshBuffer::GetType(const char * BufferName) const
{
	if (data.IsEmpty())
	{
		return VT_None;
	}

	if (!data->Buffers.count(BufferName))
	{
		return VT_None;
	}

	return data->Buffers[BufferName].type;
}

int RXMeshBuffer::GetTupleSize(const char * BufferName) const
{
	if (data.IsEmpty())
	{
		return 0;
	}

	if (!data->Buffers.count(BufferName))
	{
		return 0;
	}

	return data->Buffers[BufferName].tupleSize;
}

DataInputStream& operator >> (DataInputStream& s, RXMeshBuffer& b) {
	bool validate;
	s >> validate;
	if (validate)
	{
		b.data = SharedDataPointer<RXMeshBufferData>(new RXMeshBufferData());
		s >> b.data->Buffers;
	}
	else {
		b = RXMeshBuffer();
	}
	return s;
}

DataOutputStream& operator <<(DataOutputStream& s, const RXMeshBuffer& b) {
	if (b.data.IsEmpty())
	{
		s << false;
	}
	else {
		s << true;
		s << b.data->Buffers;
	}
	return s;
}

RX3D_EXPORT DataInputStream & operator>>(DataInputStream & s, RXTexture & b)
{
	s >> b.borderColor;
	s >> b.min;
	s >> b.mag;
	s >> b.warpT;
	s >> b.image;

	return s;
}

RX3D_EXPORT DataOutputStream & operator<<(DataOutputStream & s, const RXTexture & b)
{
	s << b.borderColor;
	s << b.min;
	s << b.mag;
	s << b.warpT;
	s << b.image;

	return s;
}

RX3D_EXPORT DataInputStream & operator>>(DataInputStream & s, RXTextureBuffer & b)
{
	// TODO: 在此处插入 return 语句

	bool isValid;
	s >> isValid;
	if (isValid)
	{
		b.data = SharedDataPointer<RXTextureBufferData>(new RXTextureBufferData());
		s >> *(b.data);
	}
	else {
		b = RXTextureBuffer();
	}

	return s;
}

RX3D_EXPORT DataOutputStream & operator<<(DataOutputStream & s, const RXTextureBuffer & b)
{
	// TODO: 在此处插入 return 语句
	if (b.data.IsEmpty())
	{
		s << false;
	}
	else {
		s << true;
		s << *b.data;
	}
	return s;
}

RX3D_EXPORT DataInputStream & operator>>(DataInputStream & s, RXShaderBuffer & b)
{
	s >> b.data;
	return s;
}

RX3D_EXPORT DataOutputStream & operator<<(DataOutputStream & s, const RXShaderBuffer & b)
{
	s << b.data;
	return s;
}



RXTextureBuffer::RXTextureBuffer()
{
}

RXTextureBuffer::~RXTextureBuffer()
{
}

RXTextureBuffer::RXTextureBuffer(const RXTextureBuffer & o):data(o.data)
{
}

bool RXTextureBuffer::operator==(const RXTextureBuffer & o) const
{
	if (data == o.data)
	{
		return true;
	}

	return *data == *o.data;
}

RXTextureBuffer & RXTextureBuffer::operator=(const RXTextureBuffer & o)
{
	data = o.data;
	return *this;
}

void RXTextureBuffer::SetAttribute(const char * Name, Variant value)
{
	data->operator[](Name) = value;
}

void RXTextureBuffer::ClearAttribute(const char * Name)
{
	data->erase(Name);
}

void RXTextureBuffer::Clear()
{
	data->clear();
}

Variant RXTextureBuffer::GetAttribute(const char* Name) const
{
	return data->at(Name);
}

std::vector<std::string> RXTextureBuffer::GetAttributeNames() const
{
	std::vector<std::string> ret;

	for (auto i : *data)
	{
		ret.push_back(i.first);
	}
	return ret;
}

RXShaderBuffer::RXShaderBuffer()
{


}

RXShaderBuffer::~RXShaderBuffer()
{
}

RXShaderBuffer::RXShaderBuffer(const RXShaderBuffer & o)
{
}

bool RXShaderBuffer::operator==(const RXShaderBuffer & o) const
{
	return data == o.data;
}

RXShaderBuffer & RXShaderBuffer::operator=(const RXShaderBuffer & o)
{
	this->data = o.data;

	return *this;
}

void RXShaderBuffer::SetShader(ShaderType type, const char * content)
{
	data[type] = content;
}

const std::string & RXShaderBuffer::GetShader(ShaderType type) const
{
	return data.at(type);
}

void RXShaderBuffer::Clear()
{
	data.clear();
}

RXTexture::RXTexture(Image image, TextureWarp warpT, TextureWarp warpS , ColorF borderColor, Filter min, Filter mag):
	image(image),
	warpT(warpT),
	warpS(warpS),
	borderColor(borderColor),
	min(min),
	mag(mag)
{
}

RXTexture::RXTexture(const RXTexture & o) = default;

RXTexture::~RXTexture()
{
}

bool RXTexture::operator==(const RXTexture & o) const
{
	return warpT == o.warpT && borderColor == o.borderColor && min == o.min && mag == o.mag && image == o.image;
}

RXTexture & RXTexture::operator=(const RXTexture & o) = default;

void RXTexture::SetImage(const Image & image)
{
	this->image = image;
}

Image RXTexture::GetImage() const
{
	return image;
}

void RXTexture::SetWarpT(TextureWarp v)
{
	this->warpT = v;
}

RXTexture::TextureWarp RXTexture::GetWarpT() const
{
	return warpT;
}

void RXTexture::SetWarpS(TextureWarp v)
{
	this->warpS = v;
}

RXTexture::TextureWarp RXTexture::GetWarpS() const
{
	return warpS;
}

void RXTexture::SetBorderColor(const ColorF & o)
{
	this->borderColor = o;
}

ColorF RXTexture::GetBorderColor() const
{
	return borderColor;
}

void RXTexture::SetMinFilter(Filter v)
{
	this->min = v;
}

RXTexture::Filter RXTexture::GetMinFilter() const
{
	return min;
}

void RXTexture::SetMagFilter(Filter v)
{
	this->mag = v;
}

RXTexture::Filter RXTexture::GetMagFilter() const
{
	return mag;
}

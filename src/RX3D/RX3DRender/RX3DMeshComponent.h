#ifndef RX3DMESHCOMPONENT_H
#define RX3DMESHCOMPONENT_H

#include "RX3DCore/RXSceneComponent.h"
#include "RX3D_global.h"

class RX3D_EXPORT RX3DMeshComponent :public RXSceneComponent
{
	REFLECTABLE(RX3DMeshComponent, RXSceneComponent)
public:
	RX3DMeshComponent(RXNodeCreationInfo*);
	~RX3DMeshComponent();

	//指定的渲染目标资产
	DECL_SIMPE_PROPERTY(RXECSDef::NodeId,RenderActorAsset)
private:
};

#endif // !RX3DMESHCOMPONENT_H

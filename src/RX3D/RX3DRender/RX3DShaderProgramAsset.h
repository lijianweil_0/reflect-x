#ifndef RX3DSHADERPROGRAMASSET_H
#define RX3DSHADERPROGRAMASSET_H
#include "RX3D_global.h"

#include <RXAsset.h>
#include "RX3DRender/RX3DRenderTypes.h"

class RX3DShaderAsset;

//着色器程序资产由多个着色器组成
class RX3D_EXPORT RX3DShaderProgramAsset :public RXAsset
{
public:
	REFLECTABLE(RX3DShaderProgramAsset, RXAsset)
	RX3DShaderProgramAsset(RXNodeCreationInfo*);
	~RX3DShaderProgramAsset();

	
	DECL_SIMPE_PROPERTY(RXShaderBuffer,Shader);
private:

};


#endif // !RXENTITY_H

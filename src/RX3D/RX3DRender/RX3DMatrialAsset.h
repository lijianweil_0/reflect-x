#ifndef RX3DMATRIALASSET_H
#define RX3DMATRIALASSET_H

#include "RX3D_global.h"

#include <RXAsset.h>
#include "RX3DRender/RX3DRenderTypes.h"

class RX3D_EXPORT RX3DMatrialAsset :public RXAsset
{
public:
	REFLECTABLE(RX3DMatrialAsset, RXAsset)
	RX3DMatrialAsset(RXNodeCreationInfo*);
	~RX3DMatrialAsset();

	//����
	DECL_SIMPE_PROPERTY(RXECSDef::NodeId,TextureAsset);
	//��ɫ������
	DECL_SIMPE_PROPERTY(RXECSDef::NodeId, ShaderProgramAsset);
};

#endif // !RX3DMESHCOMPONENT_H

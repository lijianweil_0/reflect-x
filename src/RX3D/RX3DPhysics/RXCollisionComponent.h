#ifndef RXCOLLISIONCOMPONENT_H
#define RXCOLLISIONCOMPONENT_H

#include "RX3D_global.h"
#include "RX3DCore/RXSceneComponent.h"

/*
RXCollisionComponent继承了RX3DShapeComponent类，它代表着能够在世界场景中进行物理模拟的物体，它只负责物理模拟，不负责图像显示
*/
class RX3D_EXPORT RXCollisionComponent:public RXSceneComponent
{
	REFLECTABLE(RXCollisionComponent, RXSceneComponent)
	ABSTRACT_CLASS
public:
	RXCollisionComponent(RXNodeCreationInfo*);
	~RXCollisionComponent();



private:


};



#endif // !RXCOLLISIONCOMPONENT_H

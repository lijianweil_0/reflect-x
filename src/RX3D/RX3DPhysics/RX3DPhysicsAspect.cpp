#include <RXBasicAspect/RXBasicAspectDef.h>
#include <RXBasicAspect/RXBasicAspectService.h>

#include "RX3DCore/RX3DCoreAspect.h"
#include "RX3DCore/RX3DCoreDef.h"

#include "RX3DPhysicsDef.h"

#include "RXRigidBodyComponent.h"
#include "RX3DRigidShadowNode.h"
#include "RXSensorBodyComponent.h"
#include "RX3DSensorShadowNode.h"
#include "RX3DPhysicsService.h"

#include "RX3DPhysicsAspect.h"

class RX3DRigidyNodeFactory :public ShadowNodeFactoryBase
{
public:
	RX3DRigidyNodeFactory(RX3DPhysicsAspect* aspect) {
		this->aspect = aspect;
	}
	RXShadowNode* CreateNode() override {
		return new RX3DRigidShadowNode(aspect);
	}

private:
	RX3DPhysicsAspect* aspect;

};

class RX3DSensorNodeFactory :public ShadowNodeFactoryBase
{
public:
	RX3DSensorNodeFactory(RX3DPhysicsAspect* aspect) {
		this->aspect = aspect;
	}
	RXShadowNode* CreateNode() override {
		return new RX3DSensorShadowNode(aspect);
	}

private:
	RX3DPhysicsAspect* aspect;

};

class RX3DPhysicsServiceImp :public RX3DPhysicsService
{
public:
	RX3DPhysicsServiceImp(RX3DPhysicsAspect* aspect) {
		this->aspect = aspect;
	}


private:
	RX3DPhysicsAspect* aspect;

};

RX3DPhysicsAspect::RX3DPhysicsAspect()
{
	SetAspectInfo(RXAspectInfo{ Aspects::AspectNames::RX3DPhysicsAspect,{
		Aspects::AspectService::RX3DCoreService,
		Aspects::AspectService::BasicAspectService
		} });
}

RX3DPhysicsAspect::~RX3DPhysicsAspect()
{
}

RXECSDef::AspectJobList RX3DPhysicsAspect::CreateAspectJob(std::chrono::system_clock::duration delta)
{
	RXECSDef::AspectJobList jobs;




	return jobs;
}

void RX3DPhysicsAspect::MarkDirty(DirtyMark mark)
{
	m_eDirty = DirtyMark(m_eDirty | mark);
}

void RX3DPhysicsAspect::AspectBegin()
{
	base_Service = dynamic_cast<RXBasicAspectService*>(AcquireAspectService(Aspects::AspectService::BasicAspectService));
	core_3d_service = dynamic_cast<RX3DCoreService*>(AcquireAspectService(Aspects::AspectService::RX3DCoreService));

	physics_service = new RX3DPhysicsServiceImp(this);
	RegisterAspectService(Aspects::AspectService::RX3DPhysicsService, physics_service);

	rigidyNodes = new RX3DRigidyNodeFactory(this);
	RegisterShadowFactory<RXRigidBodyComponent>(rigidyNodes);

	sensorNodes = new RX3DSensorNodeFactory(this);
	RegisterShadowFactory<RXSensorBodyComponent>(sensorNodes);

	RegisterNodeClass<RXRigidBodyComponent>();
	RegisterNodeClass<RXSensorBodyComponent>();
}

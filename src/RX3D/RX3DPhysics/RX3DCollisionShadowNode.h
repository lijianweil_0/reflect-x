#ifndef RX3DCOLLISIONSHADOWNODE_H
#define RX3DCOLLISIONSHADOWNODE_H

#include <RXShadowWorld/RXShadowNode.h>
#include <RX3D_global.h>

class RX3DPhysicsAspect;

class RX3DCollisionShadowNode :public RXShadowNode
{
public:
	RX3DCollisionShadowNode(RX3DPhysicsAspect* aspect);
	virtual ~RX3DCollisionShadowNode();
	virtual void InitNode(const RXECSDef::NodeCreationInfo& initInfo);//后端节点初始化

	virtual void HandlePropertyChange(const RXECSDef::NodePropertyChangeEvent& change);//处理前端节点属性变化
private:

protected:
	RX3DPhysicsAspect* aspect;

	friend class RX3DPhysicsAspect;
};

#endif // !RX3DCOLLISIONSHADOWNODE_H

#include "RX3DCollisionShadowNode.h"

RX3DCollisionShadowNode::RX3DCollisionShadowNode(RX3DPhysicsAspect * aspect)
{
	this->aspect = aspect;
}

RX3DCollisionShadowNode::~RX3DCollisionShadowNode()
{
}

void RX3DCollisionShadowNode::InitNode(const RXECSDef::NodeCreationInfo & initInfo)
{
	RXShadowNode::InitNode(initInfo);
}

void RX3DCollisionShadowNode::HandlePropertyChange(const RXECSDef::NodePropertyChangeEvent & change)
{
	RXShadowNode::HandlePropertyChange(change);
}

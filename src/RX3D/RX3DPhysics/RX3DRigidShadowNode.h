#ifndef RX3DRIGIDSHADOWNODE_H
#define RX3DRIGIDSHADOWNODE_H

#include "RX3DCollisionShadowNode.h"

class RX3DPhysicsAspect;

class RX3DRigidShadowNode:public RX3DCollisionShadowNode
{
public:
	RX3DRigidShadowNode(RX3DPhysicsAspect* aspect);
	virtual ~RX3DRigidShadowNode();
	virtual void InitNode(const RXECSDef::NodeCreationInfo& initInfo);//后端节点初始化

	virtual void HandlePropertyChange(const RXECSDef::NodePropertyChangeEvent& change);//处理前端节点属性变化
private:


	friend class RX3DPhysicsAspect;
};

#endif // !RX3DRIGIDSHADOWNODE_H

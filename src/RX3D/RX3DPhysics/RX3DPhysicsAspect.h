#ifndef RX3DPHYSICSASPECT_H
#define RX3DPHYSICSASPECT_H

#include <RXShadowWorld/RXAspect.h>

#include <RXBasicAspect/RXBasicAspectService.h>
#include <RX3DCore/RX3DCoreService.h>

#include "RX3DPhysicsService.h"
#include "RX3D_global.h"

class RX3D_EXPORT RX3DPhysicsAspect :public RXAspect
{
public:
	RX3DPhysicsAspect();
	~RX3DPhysicsAspect();


	RXECSDef::AspectJobList CreateAspectJob(std::chrono::system_clock::duration delta) override;

private:
	enum DirtyMark
	{
		None,
	};
	void MarkDirty(DirtyMark mark);
	void AspectBegin() override;

private:
	class RX3DServiceImp;
	RX3DCoreService* core_3d_service;
	RXBasicAspectService* base_Service;

	RX3DPhysicsService* physics_service;
	ShadowNodeFactoryBase* sensorNodes;
	ShadowNodeFactoryBase* rigidyNodes;
	DirtyMark m_eDirty;
	friend class RXSceneShadowNode;
	friend class RX3DServiceImp;
};


#endif // !RX3DPHYSICSASPECT_H

#include "RX3DRigidShadowNode.h"

RX3DRigidShadowNode::RX3DRigidShadowNode(RX3DPhysicsAspect * aspect):RX3DCollisionShadowNode(aspect)
{
}

RX3DRigidShadowNode::~RX3DRigidShadowNode()
{
}

void RX3DRigidShadowNode::InitNode(const RXECSDef::NodeCreationInfo & initInfo)
{
	RX3DCollisionShadowNode::InitNode(initInfo);
}

void RX3DRigidShadowNode::HandlePropertyChange(const RXECSDef::NodePropertyChangeEvent & change)
{
	RX3DCollisionShadowNode::HandlePropertyChange(change);
}

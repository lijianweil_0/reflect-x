#include "RX3DSensorShadowNode.h"

RX3DSensorShadowNode::RX3DSensorShadowNode(RX3DPhysicsAspect * aspect):RX3DCollisionShadowNode(aspect)
{
}

RX3DSensorShadowNode::~RX3DSensorShadowNode()
{
}

void RX3DSensorShadowNode::InitNode(const RXECSDef::NodeCreationInfo & initInfo)
{
	RX3DCollisionShadowNode::InitNode(initInfo);



}

void RX3DSensorShadowNode::HandlePropertyChange(const RXECSDef::NodePropertyChangeEvent & change)
{
	RX3DCollisionShadowNode::HandlePropertyChange(change);



}

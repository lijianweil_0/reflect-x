#ifndef RXSENSORBODYCOMPONENT_H
#define RXSENSORBODYCOMPONENT_H

#include "RXCollisionComponent.h"

/*
RXSensorBodyComponent 用于在物理世界中检测其是否和其它刚体碰撞，其本身不会受到碰撞或重力影响
*/

class RX3D_EXPORT RXSensorBodyComponent:public RXCollisionComponent
{
	REFLECTABLE(RXSensorBodyComponent, RXCollisionComponent);
	IMPLEMENT_CLASS;
public:
	RXSensorBodyComponent(RXNodeCreationInfo*);
	~RXSensorBodyComponent();




private:


};

#endif // !RXSENSORBODYCOMPONENT_H

#ifndef RXRIGIDBODYCOMPONENT_H
#define RXRIGIDBODYCOMPONENT_H


#include "ReflectX_global.h"
#include "RXCollisionComponent.h"

//刚体，用于模拟在物理世界中的刚体运动。
//该组件和其后端节点属于双向同步状态，但为了防止循环同步，它重写了关于位置操作的函数。

class RX3D_EXPORT RXRigidBodyComponent:public RXCollisionComponent
{
	REFLECTABLE(RXRigidBodyComponent, RXCollisionComponent);
	IMPLEMENT_CLASS;
public:
	RXRigidBodyComponent(RXNodeCreationInfo* );
	~RXRigidBodyComponent();

private:


};

#endif // !RXRIGIDBODYCOMPONENT_H
